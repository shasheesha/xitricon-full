<?php
require "./api/functions.php";
getVisitorIP();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./assets/css/bootstrap.css">
    <link rel="stylesheet" href="./assets/css/main.css">
    <link rel="icon" type="image/x-icon" href="assets/favicon.png">
    <title>Xitricon | SharePoint</title>
</head>

<body>
    <section class="hero-banner">
        <div class="overlay"></div>
        <video
        loop 
        muted 
        autoplay 
        preload="auto">
        <source src="./assets/videos/sharepoint_trial2.mp4" type="video/mp4">
        your browser does not support the video tag.
    </video> 
        <nav class="nav-bar">
            <div class="container">
            <div class="row">
                <div class="secs col-6">
                    <img id="main-logo" class="main-logo" src="./assets/logo1.svg" alt="" srcset="">
                </div>
                <div class="secs secs-2 col-6">
                    <div class="logo-area">
                        <img src="./assets/sharepoint.png" alt="" srcset="">
                    </div>
                </div>
            </div>
            </div>
        </nav>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h4 class="sub-title-two">Unleash SharePoint's Potential with Xitricon:</h4>
                    <h1 class="hero-title">Your Digital Workplace<br>at a Glance</h3>
                    <h2 class="min-title">• Contact Us •</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <form action="" id="form-1" class="needs-validation" novalidate>
                        <div class="input-fields">
                            <div class="form-floating">
                                <input type="text" class="form-control" id="name" placeholder="ex: Jhone" required>
                                <label for="name">Name</label>
                                <!-- <div class="valid-feedback">Looks good!</div> -->
                                <div class="invalid-feedback">Please provide your name.</div>
                            </div>
                            <div class="form-floating">
                                <input type="text" class="form-control" id="company"
                                    placeholder="ex: Xitricon (PVT) LTD" required>
                                <label for="company">Company</label>
                                <!-- <div class="valid-feedback">Looks good!</div> -->
                                <div class="invalid-feedback">Please provide your company.</div>
                            </div>
                            <div class="form-floating">
                                <select class="form-select form-control" id="iam" aria-label="i-am-select" required>
                                    <option selected value="NOT MENTIONED">I am</option>
                                    <option value="Planning a project">Actively planning a project</option>
                                    <option value="just researching">Just researching</option>
                                </select>
                                <!-- <label for="floatingSelect">I am</label> -->

                                <!-- <div class="valid-feedback">Looks good!</div> -->
                                <div class="invalid-feedback">Please provide your designation.</div>
                            </div>
                            <div class="form-floating">
                                <input type="number" class="form-control" id="contact" placeholder="ex: 012 345 6789"
                                    required>
                                <label for="contact">Contact Number</label>
                                <!-- <div class="valid-feedback">Looks good!</div> -->
                                <div class="invalid-feedback">Please provide your contact Number.</div>
                            </div>
                            <div class="form-floating">
                                <input type="email" class="form-control" id="email" placeholder="ex: name@example.com"
                                    required>
                                <label for="email">Email</label>
                                <!-- <div class="valid-feedback">Looks good!</div> -->
                                <div class="invalid-feedback">Please provide your e-mail address.</div>
                            </div>
                        </div>
                        <p class="orange-text">Please provide the required information, and we will get back to you as soon as possible.</p>
                        <button class="btn btn-primary cus-primary" onclick="contactReq()" type="button">send</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <section class="discover-sec">
        <div class="container">
            <div class="row">
                <div class="col-12 side-sec">
                    <h2 class="secondary-title">Why does an organization need<br>Microsoft SharePoint?</h2>
                    <p class="primary-para">Microsoft SharePoint is a platform that 
                    enables organizations to collaborate, store, and manage data from any device. SharePoint is a 
                    cloud computing solution that can provide your company with a competitive advantage by reducing 
                    costs, increasing scalability and agility, and enabling mobility. Additionally, SharePoint can help 
                    solve various everyday challenges your organization may encounter.</p>
                </div>
                <div class="col-12 point-cards-area">
                    <div class="row">
                        <div class="col-md-6 point-card">
                            <img class="point-icon" src="./assets/icons/communicate.svg" alt="" srcset="">
                            <div class="text-board">
                                <h4 class="card-title">Communicate</h4>
                                <p class="card-para">SharePoint facilitates effortless organization-wide communication.</p>
                            </div>
                        </div>
                        
                        <div class="col-md-6 point-card">
                            <img class="point-icon" src="./assets/icons/collaborate.svg" alt="" srcset="">
                            <div class="text-board">
                                <h4 class="card-title">Collaborate</h4>
                                <p class="card-para">Streamline group management and collaboration tools.</p>
                            </div>
                        </div>
                        
                        <div class="col-md-6 point-card">
                            <img class="point-icon" src="./assets/icons/co-innovate.svg" alt="" srcset="">
                            <div class="text-board">
                                <h4 class="card-title">Co-innovate</h4>
                                <p class="card-para">Gather and nurture ideas in an enjoyable manner.</p>
                            </div>
                        </div>
                        
                        <div class="col-md-6 point-card">
                            <img class="point-icon" src="./assets/icons/engage.svg" alt="" srcset="">
                            <div class="text-board">
                                <h4 class="card-title">Engage</h4>
                                <p class="card-para">SharePoint enables engaging interactions with employees throughout the organization.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="features">
        <div class="container">
            <div class="row">
                <h2 class="sec-title secondary-title light-blue text-center">Core Features of SharePoint</h2>
                <div class="row" style="margin: 0;">
                    <div class="col-lg-4 feature-card">
                        <img class="feature-icon" src="./assets/icons/news-boarder.svg" alt="" srcset="">
                        <div class="text-board">
                            <h4 class="feature-title">News Publishing</h4>
                            <p class="feature-para">Target news by groups. Engage people with comments and likes. Add images with drag & drop function.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 feature-card">
                        <img class="feature-icon" src="./assets/icons/templates-boarder.svg" alt="" srcset="">
                        <div class="text-board">
                            <h4 class="feature-title">Page Templates</h4>
                            <p class="feature-para">Intuitive page templates have everything necessary in place so you can focus on the content.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 feature-card">
                        <img class="feature-icon" src="./assets/icons/event-boarder.svg" alt="" srcset="">
                        <div class="text-board">
                            <h4 class="feature-title">Event Hub</h4>
                            <p class="feature-para">Keep  track of upcoming internal events, conferences andrecurring webinars.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 feature-card">
                        <img class="feature-icon" src="./assets/icons/polls-boarder.svg" alt="" srcset="">
                        <div class="text-board">
                            <h4 class="feature-title">Polls</h4>
                            <p class="feature-para">Publish and analyze quick polls on the Intranet. Get insight from your valued employees.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 feature-card">
                        <img class="feature-icon" src="./assets/icons/messages-boarder.svg" alt="" srcset="">
                        <div class="text-board">
                            <h4 class="feature-title">Important Messages</h4>
                            <p class="feature-para">Make important announcements using the news alert pop up at your intranet.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 feature-card">
                        <img class="feature-icon" src="./assets/icons/search-boarder.svg" alt="" srcset="">
                        <div class="text-board">
                            <h4 class="feature-title">Search</h4>
                            <p class="feature-para">We utilize SharePoint’s powerful search experience. Locating relevant information is easy and fast.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 feature-card">
                        <img class="feature-icon" src="./assets/icons/open-boarder.svg" alt="" srcset="">
                        <div class="text-board">
                            <h4 class="feature-title">Open Positions</h4>
                            <p class="feature-para">Introduce the best people to the right positions by using our in-house recruiting tools.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 feature-card">
                        <img class="feature-icon" src="./assets/icons/links-boarder.svg" alt="" srcset="">
                        <div class="text-board">
                            <h4 class="feature-title">Quick Links</h4>
                            <p class="feature-para">Navigate quickly to promoted content via the Intranet front page shortcuts.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 feature-card">
                        <img class="feature-icon" src="./assets/icons/faq-boarder.svg" alt="" srcset="">
                        <div class="text-board">
                            <h4 class="feature-title">FAQ</h4>
                            <p class="feature-para">FAQ includes a dynamic, user friendly UI and question categories. Create dedicated FAQs needed.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>




    <section class="brochure">
        <div class="container">
            <div class="row">
                <div class="col-md-7 side-sec text-content">
                    <h2 class="secondary-title">All The Features Your Digital Workplace Needs</h2>
                    <p class="primary-para">Our SharePoint services provide organizations with a powerful tool to enhance collaboration, streamline processes, and efficiently manage documents.</p>
                    <a style="cursor: pointer;" class="hyper-para" target="_blank" onclick="DownloadFile('Sharepoint Brochure A4 V2.pdf')"><img class="icon" src="./assets/icons/arrow.svg" alt="">View
                            Brochure</a>
                </div>
                <div class="col-md-5 img-content">
                    <img style="cursor: pointer;" class="brochure-img" onclick='window.open("http://enrol-technology.000webhostapp.com/sync/xitricon-landing-two/assets/pdf/Sharepoint Brochure A4 V2.pdf", "_blank")' src="./assets/Rectangle-1.png" alt="" srcset="">
                </div>
            </div>
        </div>
    </section>
    
    <section class="dark-sec">
        <div class="container">
            <div class="row s-sec">
                <div class="col-12">
                    <h2 class="secondary-title third-color text-center">Why Choose Xitricon as your Sharepoint<br>Implementation Partner?</h2>
                    <p class="primary-para white-para text-center">Xitricon is a leading consulting, technology, and digital transformation services 
                        company that provides a comprehensive suite of solutions to assist organizations in successfully navigating their 
                        digital transformation journey. With extensive industry expertise, a global presence, and advanced solutions, we 
                        empower enterprises to maintain a competitive edge. Through SharePoint, our services offer organizations a powerful 
                        tool for improving collaboration, streamlining processes, and efficiently managing documents.</p>
                </div>
            </div>
            <div class="row s-sec">
                <div class="col-12">
                    <h2 class="secondary-title third-color text-center">What We Offer</h2>
                    <div class="row">
                        <div class="col-lg-6 boarder-card justify-content-start">
                            <div class="list-card">
                                <h4 class="third-color">Implementation Services:</h4>
                                    <h5 class="number-points white-para">1. Power Apps :</h5>
                                        <p class="primary-para list-para white-para">Enabling connectivity through a single platform in real-time via web or mobile. Available in 20+ languages.</p>
                                    <h5 class="number-points white-para">2. Power Automation</h5>
                                    <ul class="point-ul ul-para-2  white-para">
                                        <li>Eliminate Human Errors</li>
                                        <li>Focused Strategic Opportunities</li>
                                        <li>Predict Outcomes to Improve Business Performance</li>
                                        <li>Boost Productivity and Accuracy</li>
                                        <li>Streamline Business Processes</li>
                                        <li>Intelligent Automated Processes</li>
                                    </ul>
                            </div>
                        </div>
                        <div class="col-lg-6 boarder-card justify-content-end">
                            <div class="list-card">
                                <h4 class="third-color">Support Services</h4>
                                <ul class="point-ul ul-para white-para">
                                    <li>SharePoint Portal Creation & Maintenance</li>
                                    <li>Search Configuration</li>
                                    <li>SharePoint Security</li>
                                    <li>Organizational Branding</li>
                                    <li>SharePoint Access</li>
                                    <li>Regular System Updates</li>
                                    <li>Content Management</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row s-sec blog-sec">
                <div class="col-12">
                    <h2 class="secondary-title third-color text-center">Harness the Power of SharePoint Online and<br>
Power Platforms to Create Powerful Business Solutions</h2>
<h4 class="sub-title-two">Read the Blog from our Experts</h4>
                    <div class="blog-slider">
                        <img onclick='location.replace("./blogs/Harness-the-Power-of-SharePoint.php")' src="./assets/blog-thumb.png" alt="" srcset="">
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
include "./includes/footer.php";

// Modal
include "./includes/subscribe_modal.php";
// toaster
include "./includes/submit_success_toast.php";
include "./includes/submit_unsuccess_toast.php";

?>  

</body>
<script src="https://code.jquery.com/jquery-3.7.0.js" integrity="sha256-JlqSTELeR4TLqP0OG9dxM7yDPqX1ox/HfgiSLBj8+kM=" crossorigin="anonymous"></script>
<script src="./assets/js/bootstrap.js"></script>
<script src="./assets/js/main.js"></script>
<script>
    $(window).on('load', function() {
        $('#subscribe-modal').modal('show');
    });
    
    function addNewSubscriber() {
    let email = document.getElementById('newsletter_email');
    const xhr = new XMLHttpRequest();
    xhr.open('POST', 'api/main-api.php', true);
    xhr.setRequestHeader("application-auth", "xitricon-auth");

    const formData = new FormData();
    formData.append('email', email.value);
    formData.append('action', "add_subscriber");

    xhr.onload = function () {
        if (xhr.readyState === xhr.DONE && xhr.status === 200) {
            if (xhr.responseText) {
                var responseObj = JSON.parse(xhr.responseText);
                if (responseObj) {
                    if (responseObj.status === "SUCCESS") {
                        // location.reload();
                        $('#subscribe-modal').modal('toggle');
                    } else {
                    alert(responseObj.msg);

                    }
                }
            }
        }
    };
    xhr.send(formData);
}



function contactReq() {
    let name = document.getElementById("name");
    let company = document.getElementById("company");
    let iam = document.getElementById("iam");
    let contact = document.getElementById("contact");
    let email = document.getElementById("email");

    const xhr = new XMLHttpRequest();
    xhr.open('POST', 'api/main-api.php', true);
    xhr.setRequestHeader("application-auth", "xitricon-auth");

    const formData = new FormData();
    formData.append('name', name.value);
    formData.append('company', company.value);
    formData.append('iam', iam.value);
    formData.append('contact', contact.value);
    formData.append('email', email.value);
    formData.append('action', "add_contactreq");

    xhr.onload = function () {
        if (xhr.readyState === xhr.DONE && xhr.status === 200) {
            if (xhr.responseText) {
                var responseObj = JSON.parse(xhr.responseText);
                if (responseObj) {
                    if (responseObj.status === "SUCCESS") {
                        
                        var element1 = document.getElementById("submit-success");

                         // Create toast instance
                        var myToast1 = new bootstrap.Toast(element1);
                        myToast1.show();
                        
                        $('#form-1')[0].reset();

                    } else {
                        var element2 = document.getElementById("submit-unsuccess");

                         // Create toast instance
                        var myToast2 = new bootstrap.Toast(element2);
                        myToast3.show();
                    alert(responseObj.msg);
                    }
                }
            }
        }
    };
    xhr.send(formData);
}

        function DownloadFile(fileName) {
            //Set the File URL.
            var url = "assets/pdf/" + fileName;
 
            //Create XMLHTTP Request.
            var req = new XMLHttpRequest();
            req.open("GET", url, true);
            req.responseType = "blob";
            req.onload = function () {
                //Convert the Byte Data to BLOB object.
                var blob = new Blob([req.response], { type: "application/octetstream" });
 
                //Check the Browser type and download the File.
                var isIE = false || !!document.documentMode;
                if (isIE) {
                    window.navigator.msSaveBlob(blob, fileName);
                } else {
                    var url = window.URL || window.webkitURL;
                    link = url.createObjectURL(blob);
                    var a = document.createElement("a");
                    a.setAttribute("download", fileName);
                    a.setAttribute("href", link);
                    document.body.appendChild(a);
                    a.click();
                    document.body.removeChild(a);
                }
            };
            req.send();
        };
</script>
</html>