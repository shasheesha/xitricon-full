<?php
session_start();


if(isset($_SESSION["attempt"])){
    header('Location:dashboard.php');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../assets/css/bootstrap.css">
    <link rel="stylesheet" href="../assets/css/admin-main.css">
    <link rel="icon" type="image/x-icon" href="..assets/favicon.png">
    <title>Xitricon | Admin</title>
</head>

<body>
    <form class="signin-form needs-validation" action="" method="" novalidate>
        <div class="form-floating">
            <h2>Xitricon</h2>
            <h4>Landing page Admin Panel</h4>
        </div>
        <div class="form-floating">
            <input type="text" class="form-control" id="username" required>
            <label for="username">Username</label>
            <!-- <div class="valid-feedback">Looks good!</div> -->
            <div class="invalid-feedback">Please provide username.</div>
        </div>
        <div class="form-floating">
            <input type="password" class="form-control" id="password" required>
            <label for="password">password</label>
            <!-- <div class="valid-feedback">Looks good!</div> -->
            <div class="invalid-feedback">Please provide password.</div>
        </div>
        <button class="btn btn-primary cus-btn-signin" onclick="signIn()" type="button">Sign in</button>
    </form>
</body>
<script src="../assets/js/bootstrap.js"></script>
<script>
    function signIn() {

        var username = document.getElementById("username").value;
        var password = document.getElementById("password").value;

        var xhttp = new XMLHttpRequest();
        xhttp.open("POST", "../api/main-api.php", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.setRequestHeader("application-auth", "xitricon-auth");

        xhttp.onreadystatechange = function () {
            if (xhttp.readyState === XMLHttpRequest.DONE && xhttp.status === 200) {
                if (xhttp.responseText) {
                    var responseObj = JSON.parse(xhttp.responseText);
                    if (responseObj) {
                        if (responseObj.status === "SUCCESS") {
                            location.reload();
                        } else {
                            alert(responseObj.msg);
                        }
                    }
                }
            }
        }
        xhttp.send("action=signin&username=" + username + "&password=" + password);
    }

    // Example starter JavaScript for disabling form submissions if there are invalid fields
(function () {
  'use strict'

  // Fetch all the forms we want to apply custom Bootstrap validation styles to
  var forms = document.querySelectorAll('.needs-validation')

  // Loop over them and prevent submission
  Array.prototype.slice.call(forms)
    .forEach(function (form) {
      form.addEventListener('submit', function (event) {
        if (!form.checkValidity()) {
          event.preventDefault()
          event.stopPropagation()
        }

        form.classList.add('was-validated')
      }, false)
    })
})()
</script>

</html>