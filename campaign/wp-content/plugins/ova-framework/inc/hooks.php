<?php

class ovaframework_hooks {

	public function __construct() {
		
		// Customize Menu Structures
		add_filter( 'wp_nav_menu_args', array( $this, 'ova_prefix_modify_nav_menu_args' ) );
		
		// Share Social in Single Post
		add_filter( 'ova_share_social', array( $this, 'howdy_content_social' ), 2, 10 );

		// Allow add font class to title of widget
		add_filter( 'widget_title', array( $this, 'ova_html_widget_title' ) );
		

		add_filter( 'widget_text', 'do_shortcode' );

    }

    /* Filter Walker for all menu */
	public function ova_prefix_modify_nav_menu_args( $args ) {
		return array_merge( $args, array(
		        'fallback_cb' => 'Ova_Walker_Menu::fallback',
		        'walker' 	=> new Ova_Walker_Menu()
		    ) );
	}

	public function howdy_content_social( $link, $title ) {
 		$html = '<ul class="share-social-icons clearfix">
			
			<li><a class="share-ico ico-facebook" target="_blank" href="http://www.facebook.com/sharer.php?u='.$link.'"><i class="fa fa-facebook"></i></a></li>
			
			<li><a class="share-ico ico-twitter" target="_blank" href="https://twitter.com/share?url='.$link.'&amp;text='.urlencode($title).'&amp;hashtags=simplesharebuttons"><i class="fab fa-twitter"></i></a></li>

			<li><a class="share-ico ico-pinterest" target="_blank" href="http://www.pinterest.com/pin/create/button/?url='.$link.'"><i class="fa fa-pinterest-p" ></i></a></li>

			<li><a class="share-ico ico-mail" href="mailto:?body='.$link.'"><i class="fa fa-envelope-o"></i></a></li>                                  
			
			
		</ul>';
		return $html;
 	}


	// Filter class in widget title
	public function ova_html_widget_title( $title ) {
		$title = str_replace( '{{', '<i class="', $title );
		$title = str_replace( '}}', '"></i>', $title );
		return $title;
	}

}

new ovaframework_hooks();

