<?php
namespace ova_framework\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Core\Schemes\Typography;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Border;
use Elementor\Utils;
use Elementor\Repeater;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class ova_banner_5 extends Widget_Base {

	public function get_name() {
		return 'ova_banner_5';
	}

	public function get_title() {
		return __( 'Banner 5', 'ova-framework' );
	}

	public function get_icon() {
		return 'eicon-banner';
	}

	public function get_categories() {
		return [ 'ovatheme' ];
	}

	protected function register_controls() {

		$this->start_controls_section(
			'section_content',
			[
				'label' => __( 'Content', 'ova-framework' ),
			]
		);

		$this->add_control(
			'title',
			[
				'label' => __( 'Title', 'ova-framework' ),
				'type' 	=> Controls_Manager::TEXT,
				'default' => __('Welcome To Howdy','ova-framework')
			]
		);

		$this->add_control(
			'desc',
			[
				'label' => __( 'Description', 'ova-framework' ),
				'type' 	=> Controls_Manager::TEXTAREA,
				'default' => __('Curabitur quam etsum lacus etsumis nulatis etsumised netsum ets vitae varius loremis sed lorem nets feugiat netis ligula etsumis vitae.','ova-framework')
			]
		);

		$this->add_control(
			'image',
			[
				'label' => __( 'Background Image', 'ova-framework' ),
				'type' 	=> Controls_Manager::MEDIA,
			]
		);

		$repeater = new \Elementor\Repeater();

		$repeater->add_control(
			'text_button',
			[
				'label' => __( 'Text Button ', 'ova-framework' ),
				'type' 	=> \Elementor\Controls_Manager::TEXT,
			]
		);

		$repeater->add_control(
			'link',
			[
				'label' => __( 'Link', 'ova-framework' ),
				'type' 	=> \Elementor\Controls_Manager::URL,
				
			]
		);

		$this->add_control(
			'tabs',
			[
				'label' 	=> __( 'Items', 'ova-framework' ),
				'type' 		=> Controls_Manager::REPEATER,
				'fields' 	=> $repeater->get_controls(),
				'title_field' => '{{{ text_button }}}',
				'default' => [
					[
						'text_button' => __('Explore Benefits', 'ova-framework'),
					],
					[
						'text_button' => __('Discover More', 'ova-framework'),
					]
				]
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_content_style',
			[
				'label' => __( 'Style', 'ova-framework' ),
			]
		);

			$this->add_control(
				'bgcolor_1',
				[
					'label' => __( 'Background Gradient color 1', 'ova-framework' ),
					'type' 	=> Controls_Manager::COLOR,
					'default' => '#4576c3',
				]
			);
			$this->add_control(
				'bgcolor_2',
				[
					'label' => __( 'Background Gradient color 2', 'ova-framework' ),
					'type' 	=> Controls_Manager::COLOR,
					'default' => '#5547bf',
				]
			);

			$this->add_control(
				'bgcolor_3',
				[
					'label' => __( 'Background Gradient color 3', 'ova-framework' ),
					'type' 	=> Controls_Manager::COLOR,
					'default' => '#9045c0',
				]
			);
			$this->add_control(
				'bgcolor_4',
				[
					'label' => __( 'Background Gradient color 4', 'ova-framework' ),
					'type' 	=> Controls_Manager::COLOR,
					'default' => '#be47b2',
				]
			);

			$this->add_responsive_control(
				'padding_barnner',
				[
					'label' 	 => __( 'Padding', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-banner-5 .banner-5' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);
			
		$this->end_controls_section();

		// section style content
		$this->start_controls_section(
			'section_content_row_style',
			[
				'label' => __( 'Content', 'ova-framework' ),
				'tab' 	=> Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_responsive_control(
				'padding_content',
				[
					'label' 	 => __( 'Padding', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-banner-5 .banner-5 .col-md-12' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->add_responsive_control(
				'margin_content',
				[
					'label' 	 => __( 'Margin', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-banner-5 .banner-5 .col-md-12' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->add_responsive_control(
	            'align_content',
	            [
	                'label' => __( 'Alignment', 'ova_framework' ),
	                'type' 	=> Controls_Manager::CHOOSE,
	                'options' => [
	                    'left' => [
	                        'title' => __( 'Left', 'ova_framework' ),
	                        'icon' 	=> 'eicon-text-align-left',
	                    ],
	                    'center' => [
	                        'title' => __( 'Center', 'ova_framework' ),
	                        'icon' 	=> 'eicon-text-align-center',
	                    ],
	                    'right' => [
	                        'title' => __( 'Right', 'ova_framework' ),
	                        'icon' 	=> 'eicon-text-align-right',
	                    ],
	                    'justify' => [
	                        'title' => __( 'Justified', 'ova_framework' ),
	                        'icon' => 'eicon-text-align-justify',
                    	],
	                ],
	                'selectors' => [
	                    '{{WRAPPER}} .ova-banner-5 .banner-5 .col-md-12' 			=> 'text-align: {{VALUE}}',
	                    '{{WRAPPER}} .ova-banner-5 .banner-5 .col-md-12 .content' 	=> 'text-align: {{VALUE}}',
	                ],
	            ]
	        );

		$this->end_controls_section();
		// end section style content

		// section style title
		$this->start_controls_section(
			'section_title_style',
			[
				'label' => __( 'Title', 'ova-framework' ),
				'tab' 	=> Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'title_typography',
					'selector' => '{{WRAPPER}} .ova-banner-5 .banner-5 .col-md-12 .content h1',
				]
			);

			$this->add_control(
				'color_title',
				[
					'label' => __( 'Color', 'ova-framework' ),
					'type' 	=> Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .ova-banner-5 .banner-5 .col-md-12 .content h1' => 'color: {{VALUE}};',
					],
				]
			);

			$this->add_responsive_control(
				'padding_title',
				[
					'label' 	 => __( 'Padding', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-banner-5 .banner-5 .col-md-12 .content h1' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->add_responsive_control(
				'margin_title',
				[
					'label' 	 => __( 'Margin', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-banner-5 .banner-5 .col-md-12 .content h1' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

		$this->end_controls_section();
		// end section style title

		// section style description
		$this->start_controls_section(
			'section_description_style',
			[
				'label' => __( 'Description', 'ova-framework' ),
				'tab' 	=> Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'description_typography',
					'selector' => '{{WRAPPER}} .ova-banner-5 .banner-5 .col-md-12 .content p',
				]
			);

			$this->add_control(
				'color_description',
				[
					'label' => __( 'Color', 'ova-framework' ),
					'type' 	=> Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .ova-banner-5 .banner-5 .col-md-12 .content p' => 'color: {{VALUE}};',
					],
				]
			);

			$this->add_responsive_control(
				'padding_description',
				[
					'label' 	 => __( 'Padding', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-banner-5 .banner-5 .col-md-12 .content p' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->add_responsive_control(
				'margin_description',
				[
					'label' 	 => __( 'Margin', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-banner-5 .banner-5 .col-md-12 .content p' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

		$this->end_controls_section();
		// end section style description

		// section style items
		$this->start_controls_section(
			'section_items_style',
			[
				'label' => __( 'Items', 'ova-framework' ),
				'tab' 	=> Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'items_typography',
					'selector' => '{{WRAPPER}} .ova-banner-5 .banner-5 .col-md-12 .wp-button a',
				]
			);

			$this->start_controls_tabs( 'tabs_items_style' );

		        $this->start_controls_tab(
		            'tab_items_normal',
		            [
		                'label' => __( 'Normal', 'ova_framework' ),
		            ]
		        );

		        	$this->add_control(
						'bg_items',
						[
							'label' => __( 'Background', 'ova-framework' ),
							'type' 	=> Controls_Manager::COLOR,
							'selectors' => [
								'{{WRAPPER}} .ova-banner-5 .banner-5 .col-md-12 .wp-button a' => 'background-color: {{VALUE}};',
							],
						]
					);

					$this->add_control(
						'border_items',
						[
							'label' => __( 'Border Color', 'ova-framework' ),
							'type' 	=> Controls_Manager::COLOR,
							'selectors' => [
								'{{WRAPPER}} .ova-banner-5 .banner-5 .col-md-12 .wp-button a' => 'border-color: {{VALUE}};',
							],
						]
					);

        			$this->add_control(
						'color_items',
						[
							'label' => __( 'Color', 'ova-framework' ),
							'type' 	=> Controls_Manager::COLOR,
							'selectors' => [
								'{{WRAPPER}} .ova-banner-5 .banner-5 .col-md-12 .wp-button a' => 'color: {{VALUE}};',
							],
						]
					);

		        $this->end_controls_tab();


		        $this->start_controls_tab(
		            'tab_items_hover',
		            [
		                'label' => __( 'Hover', 'ova_framework' ),
		            ]
		        );

		        	$this->add_control(
						'bg_items_hover',
						[
							'label' => __( 'Background', 'ova-framework' ),
							'type' 	=> Controls_Manager::COLOR,
							'selectors' => [
								'{{WRAPPER}} .ova-banner-5 .banner-5 .col-md-12 .wp-button a:hover' => 'background-color: {{VALUE}};',
							],
						]
					);

					$this->add_control(
						'border_items_hover',
						[
							'label' => __( 'Border Color', 'ova-framework' ),
							'type' 	=> Controls_Manager::COLOR,
							'selectors' => [
								'{{WRAPPER}} .ova-banner-5 .banner-5 .col-md-12 .wp-button a:hover' => 'border-color: {{VALUE}};',
							],
						]
					);

        			$this->add_control(
						'color_items_hover',
						[
							'label' => __( 'Color', 'ova-framework' ),
							'type' 	=> Controls_Manager::COLOR,
							'selectors' => [
								'{{WRAPPER}} .ova-banner-5 .banner-5 .col-md-12 .wp-button a:hover' => 'color: {{VALUE}};',
							],
						]
					);

		        $this->end_controls_tab();

		    $this->end_controls_tabs();

		    $this->add_group_control(
	            Group_Control_Border::get_type(), [
	                'name' 		=> 'button_border',
	                'selector' 	=> '{{WRAPPER}} .ova-banner-5 .banner-5 .col-md-12 .wp-button a',
	                'separator' => 'before',
	            ]
	        );

	        $this->add_control(
	            'button_border_radius',
	            [
	                'label' => __( 'Border Radius', 'ova_framework' ),
	                'type' 	=> Controls_Manager::DIMENSIONS,
	                'size_units' => [ 'px', '%' ],
	                'selectors'  => [
	                    '{{WRAPPER}} .ova-banner-5 .banner-5 .col-md-12 .wp-button a' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
	                ],
	            ]
	        );

		    $this->add_responsive_control(
				'padding_items',
				[
					'label' 	 => __( 'Padding', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-banner-5 .banner-5 .col-md-12 .wp-button a' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->add_responsive_control(
				'margin_items',
				[
					'label' 	 => __( 'Margin', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-banner-5 .banner-5 .col-md-12 .wp-button a' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

		$this->end_controls_section();
		// end section style items

		// section style image
		$this->start_controls_section(
			'section_img_style',
			[
				'label' => __( 'Image', 'ova-framework' ),
				'tab' 	=> Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_responsive_control(
	            'width_img',
	            [
	                'label' => __( 'Width', 'ova_framework' ),
	                'type' => Controls_Manager::SLIDER,
	                'size_units' => [ '%', 'px' ],
	                'range' => [
	                    'px' => [
	                        'max' => 1000,
	                    ],
	                ],
	                'tablet_default' => [
	                    'unit' => '%',
	                ],
	                'mobile_default' => [
	                    'unit' => '%',
	                ],
	                'selectors' => [
	                    '{{WRAPPER}} .ova-banner-5 .banner-5 .col-md-12 .image img' => 'width: {{SIZE}}{{UNIT}};',
	                ],
	            ]
	        );

			$this->add_responsive_control(
				'padding_img',
				[
					'label' 	 => __( 'Padding', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-banner-5 .banner-5 .col-md-12 .image img' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->add_responsive_control(
				'margin_img',
				[
					'label' 	 => __( 'Margin', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-banner-5 .banner-5 .col-md-12 .image img' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

		$this->end_controls_section();
		// end section style content

	}

	protected function render() {

		$settings = $this->get_settings();

		?>
		<div class="ova-banner-5">
			<div class="banner-5" >
				<div class="banner-5-overlay" style="
					    background: linear-gradient(-45deg, <?php echo $settings['bgcolor_1'] ?>, <?php echo $settings['bgcolor_2'] ?>, <?php echo $settings['bgcolor_3'] ?>, <?php echo $settings['bgcolor_4'] ?>);
					    background-size: 400% 400%;
				"></div>
				<div class="container">
			        <div class="row">
			            <div class="col-md-12">
			            	<div class="content">
								<h1><?php echo esc_html($settings['title']) ?></h1>
								<p><?php echo esc_html($settings['desc']) ?></p>
							</div>
							<div class="wp-button">
								<?php if (!empty($settings['tabs'])) : foreach ($settings['tabs'] as $item) : ?>
									<?php 
									$is_external = $item['link']['is_external'] == 'on' ? '_blank' : '_self';
									$nofollow = $item['link']['nofollow'] == true ? 'rel="nofollow"' : '';
									 ?>
									<a href="<?php echo esc_attr($item['link']['url']) ?>" target="<?php echo $is_external; ?>" <?php echo $nofollow; ?> ><?php echo esc_attr($item['text_button']) ?></a>
								<?php endforeach; endif ?>
							</div> 
							<div class="image">
								<img alt="<?php echo esc_attr($settings['title']) ?>" src="<?php echo esc_attr($settings['image']['url']) ?>">
							</div>
			            </div>
			        </div>
			    </div>
					
			</div>
		</div>
		
		
		<?php
	}
}