<?php
namespace ova_framework\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Core\Schemes\Typography;
use Elementor\Group_Control_Typography;
use Elementor\Core\Schemes\Color;
use Elementor\Group_Control_Border;
use Elementor\Repeater;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


class ova_price_tab extends Widget_Base {

	public function get_name() {
		return 'ova_price_tab';
	}

	public function get_title() {
		return __( 'Price Table', 'ova-framework' );
	}

	public function get_icon() {
		return 'fa fa-table';
	}

	public function get_categories() {
		return [ 'ovatheme' ];
	}

	public function get_script_depends() {
		return [ 'script-elementor' ];
	}

	protected function register_controls() {
		$this->start_controls_section(
			'section_content',
			[
				'label' => __( 'Price Table', 'ova-framework' ),
			]
		);
		$this->add_control(
			'version',
			[
				'label' => __( 'Version', 'ova-framework' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'options' => [
					'white' => __('White', 'ova-framework'),
					'grey' => __('Grey', 'ova-framework'),
					'blue' => __('Blue', 'ova-framework'),
				]
			]
		);

		$this->add_control(
			'type',
			[
				'label' => __( 'Type', 'ova-framework' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __('Individual', 'ova-framework'),
			]
		);


		$this->add_control(
			'price',
			[
				'label' => __( 'Price', 'ova-framework' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __('£50', 'ova-framework'),
			]
		);

		$this->add_control(
			'per_time',
			[
				'label' => __( 'Per Time', 'ova-framework' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __('Per Month', 'ova-framework'),
			]
		);


		$this->add_control(
			'text_button',
			[
				'label' => __( 'Text Button', 'ova-framework' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __('GET STARTED', 'ova-framework'),
			]
		);

		$this->add_control(
			'link',
			[
				'label' => __( 'Link', 'ova-framework' ),
				'type' => \Elementor\Controls_Manager::URL,
				'placeholder' => __( 'https://your-link.com', 'ova-framework' ),
				'default' => [
					'url' => '#',
					'is_external' => false,
					'nofollow' => false,
				],
			]
		);

		$repeater = new \Elementor\Repeater();

		

		$repeater->add_control(
			'desc',
			[
				'label' => __( 'Description ', 'ova-framework' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( '24/7 Support', 'ova-framework' ),
			]
		);


		$this->add_control(
			'tabs',
			[
				'label' => __( 'Items', 'ova-framework' ),
				'type' => Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'title_field' => '{{{ desc }}}',
			]
		);


		$this->end_controls_section();

		$this->start_controls_section(
			'section_style',
			[
				'label' => __( 'Style', 'ova-framework' ),
			]
		);

			$this->add_control(
				'bg_color_1',
				[
					'label' => __( 'Background Pricing Gradient Color 1', 'ova-framework' ),
					'type' => Controls_Manager::COLOR,
				]
			);

			

			$this->add_control(
				'bg_color_2',

				[
					'label' => __( 'Background Pricing Gradient Color 2', 'ova-framework' ),
					'type' => Controls_Manager::COLOR,
					'separator' => 'after',
				]
				
			);


			$this->add_control(
				'type_color',
				[
					'label' => __( 'Type Color', 'ova-framework' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} ul.pricing-list li.price-title' => 'color : {{VALUE}} ;'
					],
					
				]
			);
			$this->add_control(
				'value_color',
				[
					'label' => __( 'Value Color', 'ova-framework' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .price-box-white ul.pricing-list li.price-value' => 'color : {{VALUE}} ;'
					],
					
				]
			);
			$this->add_control(
				'time_color',
				[
					'label' => __( 'Time Color', 'ova-framework' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} ul.pricing-list li.price-subtitle' => 'color : {{VALUE}} ;'
					],
					
				]
			);
			$this->add_control(
				'desc_color',
				[
					'label' => __( 'Description Color', 'ova-framework' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} ul.pricing-list li.price-text' => 'color : {{VALUE}} ;'
					],
					
				]
			);
			
			

			$this->add_control(
				'button_color_1',
				[
					'label' => __( 'Button Background Gradient Color 1', 'ova-framework' ),
					'type' => Controls_Manager::COLOR,
				]
			);

			

			$this->add_control(
				'button_color_2',
				[
					'label' => __( 'Button Background Gradient Color 2', 'ova-framework' ),
					'type' => Controls_Manager::COLOR,
				]
			);

			
		

		$this->end_controls_section();

		// end tab section_content

	}

	protected function render() {
		$settings = $this->get_settings_for_display();
		$version = $settings['version'];
		$class_version = "price-box-white";
		$class_white_text = "";
		switch ($version) {
			case "white" : {
				$class_version = 'price-box-white';
				break;
			}
			case "grey" : {
				$class_version = 'price-box-grey';
				break;
			}
			case "blue" : {
				$class_version = 'price-box-blue';
				$class_white_text = 'white-text';
				break;
			}
		}

		$is_external = $settings['link']['is_external'] == 'on' ? '_blank' : '_self';
		$nofollow = $settings['link']['nofollow'] == true ? 'rel="nofollow"' : '';
		?>

		<div class="ova-price-tab">
			<div class="price-box-color <?php echo esc_attr($class_version) ?>" style="
				    background: <?php echo $settings['bg_color_1']; ?>;
				    background: -webkit-linear-gradient(left top, <?php echo $settings['bg_color_1']; ?>, <?php echo $settings['bg_color_2']; ?>);
				    background: -o-linear-gradient(bottom right, <?php echo $settings['bg_color_1']; ?>, <?php echo $settings['bg_color_2']; ?>6);
				    background: -moz-linear-gradient(bottom right, <?php echo $settings['bg_color_1']; ?>, <?php echo $settings['bg_color_2']; ?>);
				    background: linear-gradient(to bottom right, <?php echo $settings['bg_color_1']; ?>, <?php echo $settings['bg_color_2']; ?>);

			">

				<ul class="pricing-list">

					<li class="price-title <?php echo esc_attr($class_white_text) ?>"><?php echo esc_html($settings['type']) ?></li>

					<li class="price-value <?php echo esc_attr($class_white_text) ?>"><?php echo esc_html($settings['price']) ?></li>

					<li class="price-subtitle <?php echo esc_attr($class_white_text) ?>"><?php echo esc_html($settings['per_time']) ?></li>

					<li class="price-tag">
						<a href="<?php echo $settings['link']['url']; ?>" target="<?php echo $is_external; ?>" <?php echo $nofollow; ?> style="
							background: <?php echo $settings['button_color_1']; ?>;
						    background: -webkit-linear-gradient(135deg, <?php echo $settings['button_color_1']; ?> 0%, <?php echo $settings['button_color_2']; ?> 100%);
						    background: -o-linear-gradient(bottom right, <?php echo $settings['button_color_1']; ?>, <?php echo $settings['button_color_2']; ?>);
						    background: -moz-linear-gradient(bottom right, <?php echo $settings['button_color_1']; ?>, <?php echo $settings['button_color_2']; ?>);
						    background: linear-gradient(135deg, <?php echo $settings['button_color_1']; ?> 0%, <?php echo $settings['button_color_2']; ?> 100%);
						">
							<?php echo esc_html($settings['text_button']) ?>
						</a>
					</li>

					<?php if (!empty($settings['tabs'])) : foreach ($settings['tabs'] as $item) : ?>
						<li class="price-text <?php echo esc_attr($class_white_text) ?>"><?php echo esc_attr($item['desc']) ?></li>
					<?php endforeach; endif ?>
				</ul>

			</div>
		</div>

		<?php
	}
}
