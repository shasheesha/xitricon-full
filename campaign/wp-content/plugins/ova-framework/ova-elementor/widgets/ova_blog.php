<?php
namespace ova_framework\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Core\Schemes\Typography;
use Elementor\Group_Control_Typography;
use Elementor\Core\Schemes\Color;
use Elementor\Group_Control_Border;
use Elementor\Repeater;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


class ova_blog extends Widget_Base {

	public function get_name() {
		return 'ova_blog';
	}

	public function get_title() {
		return __( 'Blog', 'ova-framework' );
	}

	public function get_icon() {
		return 'eicon-posts-ticker';
	}

	public function get_categories() {
		return [ 'ovatheme' ];
	}

	public function get_script_depends() {
		return [ 'script-elementor' ];
	}

	protected function register_controls() {

		$args = array(
			'orderby' => 'name',
			'order' => 'ASC'
		);

		$categories=get_categories($args);
		$cate_array = array();
		$arrayCateAll = array( 'all' => 'All categories ' );
		if ($categories) {
			foreach ( $categories as $cate ) {
				$cate_array[$cate->cat_name] = $cate->slug;
			}
		} else {
			$cate_array["No content Category found"] = 0;
		}



		//SECTION CONTENT
		$this->start_controls_section(
			'section_content',
			[
				'label' => __( 'Content', 'ova-framework' ),
			]
		);

		$this->add_control(
			'category',
			[
				'label' => __( 'Category', 'ova-framework' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'all',
				'options' => array_merge($arrayCateAll,$cate_array),
			]
		);

		$this->add_control(
			'total_count',
			[
				'label' => __( 'Total Post', 'ova-framework' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 3,
			]
		);

		$this->add_control(
			'number_title',
			[
				'label' => __( 'Number Word Title', 'ova-framework' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 8,
			]
		);

		$this->add_control(
			'number_content',
			[
				'label' => __( 'Number Word Content', 'ova-framework' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 23,
			]
		);

		$this->add_control(
			'order_by',
			[
				'label' => __('Order By', 'ova-framework'),
				'type' => Controls_Manager::SELECT,
				'default' => 'desc',
				'options' => [
					'asc' => __('ASC', 'ova-framework'),
					'desc' => __('DESC', 'ova-framework'),
				]
			]
		);

		$this->add_control(
			'show_author',
			[
				'label' => __( 'Show Author', 'ova-framework' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => __( 'Show', 'ova-framework' ),
				'label_off' => __( 'Hide', 'ova-framework' ),
				'return_value' => 'yes',
				'default' => 'yes',
			]
		);

		$this->add_control(
			'show_category',
			[
				'label' => __( 'Show Category', 'ova-framework' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => __( 'Show', 'ova-framework' ),
				'label_off' => __( 'Hide', 'ova-framework' ),
				'return_value' => 'yes',
				'default' => 'yes',
			]
		);

		$this->add_control(
			'show_title',
			[
				'label' => __( 'Show Title', 'ova-framework' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => __( 'Show', 'ova-framework' ),
				'label_off' => __( 'Hide', 'ova-framework' ),
				'return_value' => 'yes',
				'default' => 'yes',
			]
		);

		$this->add_control(
			'show_content',
			[
				'label' => __( 'Show Content', 'ova-framework' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => __( 'Show', 'ova-framework' ),
				'label_off' => __( 'Hide', 'ova-framework' ),
				'return_value' => 'yes',
				'default' => 'yes',
			]
		);

		$this->add_control(
			'title_color',
			[
				'label' => __( 'Title Color', 'ova-framework' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} h3.blog-title a' => 'color : {{VALUE}} ;'
				],
				'default' => '#454545'
				
			]
		);
		$this->add_control(
			'title_color_hover',
			[
				'label' => __( 'Title Color Hover', 'ova-framework' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} h3.blog-title a:hover' => 'color : {{VALUE}} ;'
				],
				'default' => '#87ac34',
				'separator' => 'after',
				
			]
		);

		$this->add_control(
			'link_color',
			[
				'label' => __( 'Author Color Hover', 'ova-framework' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .ova-blog .blog-item .blog-item-inner  a.blog-icons' => 'color : {{VALUE}}!important;'
				],
				'default' => '#858585'
				
			]
		);
		$this->add_control(
			'link_color_hover',
			[
				'label' => __( 'Author Color Hover', 'ova-framework' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .ova-blog .blog-item .blog-item-inner  a.blog-icons:hover' => 'color : {{VALUE}}!important;'
				],
				'default' => '#228798',
				'separator' => 'after',
				
			]
		);

		$this->add_control(
			'cat_color',
			[
				'label' => __( 'Category Color Hover', 'ova-framework' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .ova-blog .blog-item .blog-item-inner  .category a' => 'color : {{VALUE}}!important;'
				],
				'default' => '#858585'
				
			]
		);
		$this->add_control(
			'cat_color_hover',
			[
				'label' => __( 'Category Color Hover', 'ova-framework' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .ova-blog .blog-item .blog-item-inner  .category a:hover' => 'color : {{VALUE}}!important;'
				],
				'default' => '#228798',
				'separator' => 'after',
				
			]
		);

		$this->end_controls_section();
	}

	protected function render() {
		$settings = $this->get_settings_for_display();

		$category = $settings['category'];
		$total_count = $settings['total_count'];
		$order = $settings['order_by'];

		$number_title = $settings['number_title'] ? $settings['number_title'] : 8;
		$number_content = $settings['number_content'] ? $settings['number_content'] : 23;

		$args = [];
		if ($category == 'all') {
			$args=[
				'post_type' => 'post',
				'posts_per_page' => $total_count,
				'order' => $order,
			];
		} else {
			$args=[
				'post_type' => 'post', 
				'category_name'=>$category,
				'posts_per_page' => $total_count,
				'order' => $order,
			];
		}

		$blog = new \WP_Query($args);

		?>
		<div class="ova-blog">
			<?php
			if($blog->have_posts()) : while($blog->have_posts()) : $blog->the_post();
				$thumbnail_url = wp_get_attachment_image_url(get_post_thumbnail_id() , 'full' );
				?>
				<div class="blog-item">

					<!--begin popup image -->
					<div class="popup-wrapper">
						<div class="popup-gallery">
							<?php if (!empty($thumbnail_url)): ?>
							<a href="<?php echo esc_url(the_permalink()) ?>">
								<img src="<?php echo esc_attr($thumbnail_url) ?>" class="width-100" alt="<?php echo esc_html(howdy_custom_text(get_the_title(), $number_title)); ?>">
								<span class="eye-wrapper2"><i class="fa fa-link eye-icon"></i></span>
							</a>
							<?php endif ?>
						</div>
					</div>
					<!--end popup image -->

					<!--begin blog-item_inner -->
					<div class="blog-item-inner">
						<?php if($settings['show_title'] === 'yes') : ?>
						<h3 class="blog-title"><a href="<?php echo esc_url(the_permalink()) ?>"><?php echo esc_html(howdy_custom_text(get_the_title(), $number_title)); ?></a></h3>
						<?php endif ?>
						<?php if ($settings['show_author']) : ?>
						<a href="<?php echo get_author_posts_url(get_the_ID()) ?>" class="blog-icons"><i class="fa fa-user"></i> <?php echo get_the_author() ?></a>
						<?php endif ?>
						<?php if ($settings['show_category']) : ?>
						<span class="category">
							<i class="fa fa-tags"></i>
							<?php the_category('&sbquo;&nbsp;'); ?>
						</span>
						<?php endif ?>
						<?php if ($settings['show_content']) : ?>
						<p><?php echo esc_html(howdy_custom_text(get_the_excerpt(), $number_content)); ?></p>
						<?php endif ?>

					</div>
					<!--end blog-item-inner -->

				</div>

				<?php
			endwhile; endif; wp_reset_postdata();
			?>

			
		</div>
		<?php
	}
}
