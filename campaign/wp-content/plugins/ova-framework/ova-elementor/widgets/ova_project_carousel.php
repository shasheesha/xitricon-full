<?php
namespace ova_framework\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Core\Schemes\Typography;
use Elementor\Group_Control_Typography;
use Elementor\Core\Schemes\Color;
use Elementor\Group_Control_Border;
use Elementor\Repeater;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Elementor Hello World
 *
 * Elementor widget for hello world.
 *
 * @since 1.0.0
 */
class ova_project_carousel extends Widget_Base {

	public function get_name() {
		return 'ova_project_carousel';
	}

	public function get_title() {
		return __( 'Project Carousel', 'ova-framework' );
	}

	public function get_icon() {
		return 'fa fa-product-hunt';
	}

	public function get_categories() {
		return [ 'ovatheme' ];
	}


	public function get_script_depends() {
		wp_enqueue_style( 'magnific-popup', OVA_PLUGIN_URI.'assets/libs/magnific-popup/style-magnific-popup.css' );
		wp_enqueue_script( 'manific-popup', OVA_PLUGIN_URI.'assets/libs/magnific-popup/jquery.magnific-popup.min.js', array('jquery'), false, true );
		wp_enqueue_style( 'owl-carousel', OVA_PLUGIN_URI.'assets/libs/owl-carousel/assets/owl.carousel.min.css' );
		wp_enqueue_script( 'owl-carousel', OVA_PLUGIN_URI.'assets/libs/owl-carousel/owl.carousel.min.js', array('jquery'), false, true );

		return [ 'script-elementor' ];
	}

	protected function register_controls() {

		/*************************************************************************************************
		SECTION VERSION
		*************************************************************************************************/

		$this->start_controls_section(
			'section_content',
			[
				'label' => __( 'Content', 'ova-framework' ),
			]
		);

		$repeater = new \Elementor\Repeater();

		$repeater->add_control(
			'image',
			[
				'label' => __( 'Image ', 'ova-framework' ),
				'type' 	=> \Elementor\Controls_Manager::MEDIA,
			]
		);

		$repeater->add_control(
			'icon',
			[
				'label' 	=> __( 'Icon ', 'ova-framework' ),
				'type' 		=> \Elementor\Controls_Manager::TEXT,
				'default' 	=> 'fa fa-search-plus eye-icon',
			]
		);

		$repeater->add_control(
			'title',
			[
				'label' 	=> __( 'Title ', 'ova-framework' ),
				'type' 		=> \Elementor\Controls_Manager::TEXT,
				'default' 	=> __( 'Safe Payment Method', 'ova-framework' ),
			]
		);

		$repeater->add_control(
			'info',
			[
				'label' 	=> __( 'Info ', 'ova-framework' ),
				'type' 		=> \Elementor\Controls_Manager::TEXT,
				'default' 	=> __( 'PPC | eCommerce', 'ova-framework' ),
			]
		);

		$repeater->add_control(
			'link',
			[
				'label' => __( 'Link', 'ova-framework' ),
				'type' 	=> \Elementor\Controls_Manager::URL,
				'placeholder' => __( 'https://your-link.com', 'ova-framework' ),
				'show_external' => false,
				'default' => [
					'url' => '#',
					'is_external' => false,
					'nofollow' => false,
				],
			]
		);

		$this->add_control(
			'tabs',
			[
				'label' 	=> __( 'Items', 'ova-framework' ),
				'type' 		=> Controls_Manager::REPEATER,
				'fields' 	=> $repeater->get_controls(),
				'title_field' => '{{{ title }}}',
			]
		);

		$this->end_controls_section();

		############################ END SECTION VERSIONTION TESTIMONIAL 1  ##############################


		/*****************************************************************
		START SECTION ADDITIONAL VERSIONT
		******************************************************************/

		$this->start_controls_section(
			'section_additional_options',
			[
				'label' => __( 'Additional Options', 'ova-framework' ),
			]
		);

		$this->add_control(
			'margin_items',
			[
				'label' 	=> __( 'Margin Right Items', 'ova-framework' ),
				'type' 		=> Controls_Manager::NUMBER,
				'default' 	=> 30,
			]

		);

		$this->add_control(
			'slides_to_scroll',
			[
				'label' 		=> __( 'Slides to Scroll', 'ova-framework' ),
				'type' 			=> Controls_Manager::NUMBER,
				'description' 	=> __( 'Set how many slides are scrolled per swipe.', 'ova-framework' ),
				'default' 		=> '1',
			]
		);

		$this->add_control(
			'pause_on_hover',
			[
				'label' => __( 'Pause on Hover', 'ova-framework' ),
				'type' 	=> Controls_Manager::SWITCHER,
				'default' => 'yes',
				'options' => [
					'yes' 	=> __( 'Yes', 'ova-framework' ),
					'no' 	=> __( 'No', 'ova-framework' ),
				],
				'frontend_available' => true,
			]
		);


		$this->add_control(
			'infinite',
			[
				'label' 	=> __( 'Infinite Loop', 'ova-framework' ),
				'type' 		=> Controls_Manager::SWITCHER,
				'default' 	=> 'yes',
				'options' 	=> [
					'yes' 	=> __( 'Yes', 'ova-framework' ),
					'no' 	=> __( 'No', 'ova-framework' ),
				],
				'frontend_available' => true,
			]
		);

		$this->add_control(
			'autoplay',
			[
				'label' 	=> __( 'Autoplay', 'ova-framework' ),
				'type' 		=> Controls_Manager::SWITCHER,
				'default' 	=> 'yes',
				'options' 	=> [
					'yes' 	=> __( 'Yes', 'ova-framework' ),
					'no' 	=> __( 'No', 'ova-framework' ),
				],
				'frontend_available' => true,
			]
		);

		$this->add_control(
			'autoplay_speed',
			[
				'label' 	=> __( 'Autoplay Speed', 'ova-framework' ),
				'type' 		=> Controls_Manager::NUMBER,
				'default' 	=> 3000,
				'step' 		=> 500,
				'condition' => [
					'autoplay' => 'yes',
				],
				'frontend_available' => true,
				'condition' => [
					'autoplay' => 'yes',
				],
			]
		);

		$this->add_control(
			'smartspeed',
			[
				'label'   => __( 'Smart Speed', 'ova-framework' ),
				'type'    => Controls_Manager::NUMBER,
				'default' => 500,

			]
		);


		$this->add_control(
			'color_dot',
			[
				'label' => __( 'Color Dot', 'ova-framework' ),
				'type' 	=> Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .ova-project-carousel .owl-carousel .owl-dots .owl-dot' => 'border-color : {{VALUE}}; ',
				],
			]
		);

		$this->add_control(
			'color_dot_active',
			[
				'label' => __( 'Color Dot Active', 'ova-framework' ),
				'type' 	=> Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .ova-project-carousel .owl-carousel .owl-dots .owl-dot.active' => 'background-color : {{VALUE}} ; border-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'color_title_hover',
			[
				'label' => __( 'Title Hover Color', 'ova-framework' ),
				'type' 	=> Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .portfolio-box h3 a:hover' => 'color : {{VALUE}};',
				],
				'default'	=> '#3498db'
			]
		);


		$this->end_controls_section();
		#########################    END SECTION ADDITIONAL  VERSION 1  #########################

		// section style image
		$this->start_controls_section(
			'section_image_style',
			[
				'label' => __( 'Image', 'ova-framework' ),
				'tab' 	=> Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_responsive_control(
	            'image_width',
	            [
	                'label' => __( 'Width', 'ova_framework' ),
	                'type' => Controls_Manager::SLIDER,
	                'size_units' => [ '%', 'px' ],
	                'range' => [
	                    'px' => [
	                        'max' => 1000,
	                    ],
	                ],
	                'tablet_default' => [
	                    'unit' => '%',
	                ],
	                'mobile_default' => [
	                    'unit' => '%',
	                ],
	                'selectors' => [
	                    '{{WRAPPER}} .ova-project-carousel .wp-ova-project .list-project .item .portfolio-item-wrapper .popup-wrapper .popup-gallery' => 'width: {{SIZE}}{{UNIT}};',
	                ],
	            ]
	        );

			$this->add_responsive_control(
				'padding_image',
				[
					'label' 	 => __( 'Padding', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-project-carousel .wp-ova-project .list-project .item .portfolio-item-wrapper .popup-wrapper' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->add_responsive_control(
				'margin_image',
				[
					'label' 	 => __( 'Margin', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-project-carousel .wp-ova-project .list-project .item .portfolio-item-wrapper .popup-wrapper' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

		$this->end_controls_section();
		// end section style image

		// section style title
		$this->start_controls_section(
			'section_title_style',
			[
				'label' => __( 'Title', 'ova-framework' ),
				'tab' 	=> Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'title_typography',
					'selector' => '{{WRAPPER}} .ova-project-carousel .wp-ova-project .list-project .item .portfolio-item-wrapper .portfolio-box h3 a',
				]
			);

			$this->add_control(
				'color_title',
				[
					'label' => __( 'Color', 'ova-framework' ),
					'type' 	=> Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .ova-project-carousel .wp-ova-project .list-project .item .portfolio-item-wrapper .portfolio-box h3 a' => 'color : {{VALUE}};',
					],
				]
			);

			$this->add_responsive_control(
				'padding_title',
				[
					'label' 	 => __( 'Padding', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-project-carousel .wp-ova-project .list-project .item .portfolio-item-wrapper .portfolio-box h3' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->add_responsive_control(
				'margin_title',
				[
					'label' 	 => __( 'Margin', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-project-carousel .wp-ova-project .list-project .item .portfolio-item-wrapper .portfolio-box h3' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;',
					],
				]
			);

		$this->end_controls_section();
		// end section style title

		// section style info
		$this->start_controls_section(
			'section_info_style',
			[
				'label' => __( 'Info', 'ova-framework' ),
				'tab' 	=> Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'info_typography',
					'selector' => '{{WRAPPER}} .ova-project-carousel .wp-ova-project .list-project .item .portfolio-item-wrapper .portfolio-box .portfolio-info',
				]
			);

			$this->add_control(
				'color_info',
				[
					'label' => __( 'Color', 'ova-framework' ),
					'type' 	=> Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .ova-project-carousel .wp-ova-project .list-project .item .portfolio-item-wrapper .portfolio-box .portfolio-info' => 'color : {{VALUE}};',
					],
				]
			);

			$this->add_responsive_control(
				'padding_info',
				[
					'label' 	 => __( 'Padding', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-project-carousel .wp-ova-project .list-project .item .portfolio-item-wrapper .portfolio-box .portfolio-info' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->add_responsive_control(
				'margin_info',
				[
					'label' 	 => __( 'Margin', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-project-carousel .wp-ova-project .list-project .item .portfolio-item-wrapper .portfolio-box .portfolio-info' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

		$this->end_controls_section();
		// end section style title

	}

	/**
	 * Render the widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

			$tabs = $settings['tabs'];

			$data_options['slideBy'] 			= $settings['slides_to_scroll'];
			$data_options['margin'] 			= $settings['margin_items'];
			$data_options['autoplayHoverPause'] = $settings['pause_on_hover'] === 'yes' ? true : false;
			$data_options['loop'] 			 	= $settings['infinite'] === 'yes' ? true : false;
			$data_options['autoplay'] 			= $settings['autoplay'] === 'yes' ? true : false;
			$data_options['autoplayTimeout']	= $data_options['autoplay'] ? $settings['autoplay_speed'] : 3000;
			$data_options['smartSpeed']			= $settings['smartspeed'];


		?>

		<div class="ova-project-carousel">
			<div class="wp-ova-project">
				<div class="list-project slide-project owl-carousel" data-options="<?php echo esc_attr(json_encode($data_options)) ?>">
					<?php if (!empty($tabs)) : foreach ($tabs as $item) : ?>
						<div class="item">
							<div class=" portfolio-item-wrapper">
								<!--begin popup image -->
								<div class="popup-wrapper">
									<div class="popup-gallery">
										<a href="<?php echo esc_attr($item['image']['url']) ?>" class="popup1">
											<img src="<?php echo esc_attr($item['image']['url']) ?>" class="width-100" alt="<?php echo esc_attr($item['title']) ?>">
											<span class="eye-wrapper"><i class="<?php echo esc_attr($item['icon']) ?>"></i></span>
										</a>
									</div>
								</div>
								<!--end popup image -->
								<!--begin portfolio-box -->
								<div class="portfolio-box">
									<h3><a href="<?php echo esc_attr($item['link']['url']) ?>"><?php echo esc_attr($item['title']) ?></a></h3>

									<p class="portfolio-info"><?php echo esc_attr($item['info']) ?></p>
								</div>
								<!--end portfolio-box -->
							</div>
							<!--end portfolio-item-wrapper -->
						</div>
					<?php endforeach; endif; ?>
				</div>
			</div>
		</div>
		<!-- end .ova-project-carousel -->

		<?php
	}
}

