<?php
namespace ova_framework\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Core\Schemes\Typography;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Border;
use Elementor\Utils;
use Elementor\Repeater;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class ova_banner_4 extends Widget_Base {

	public function get_name() {
		return 'ova_banner_4';
	}

	public function get_title() {
		return __( 'Banner 4', 'ova-framework' );
	}

	public function get_icon() {
		return 'eicon-banner';
	}

	public function get_categories() {
		return [ 'ovatheme' ];
	}

	public function get_script_depends() {
		return [ 'script-elementor' ];
	}

	protected function register_controls() {

		

		$this->start_controls_section(
			'section_content',
			[
				'label' => __( 'Content', 'ova-framework' ),
			]
		);

			$this->add_control(
				'link_video',
				[
					'label' => __( 'Link Video Youtube', 'ova-framework' ),
					'type' 	=> \Elementor\Controls_Manager::URL,
					'show_external' => false,
					'default' => [
						'url' => '#',
						'is_external' => false,
						'nofollow' => false,
					],
				]
			);

			$this->add_control(
				'title',
				[
					'label' => __( 'Title', 'ova-framework' ),
					'type' 	=> Controls_Manager::TEXT,
					'label_block' => true,
					'default' => __('Ready To Promote Your Business With Howdy?','ova-framework')
				]
			);

			$this->add_control(
				'desc',
				[
					'label' => __( 'Description', 'ova-framework' ),
					'type' 	=> Controls_Manager::TEXTAREA,
					'default' => __('Curabitur quam etsum lacus etsumis nulatis etsumised vitae nisledita varius loremis sed feugiat ligula aliqua etsumis vitae dictimus etis netsum ipsum netsum.', 'ova-framework'),
				]
			);


			$this->add_control(
				'text_button',
				[
					'label' => __( 'Text Button ', 'ova-framework' ),
					'type' 	=> \Elementor\Controls_Manager::TEXT,
					'default' => __('Explore Benefits', 'ova-framework'),
				]
			);

			$this->add_control(
				'link',
				[
					'label' => __( 'Link', 'ova-framework' ),
					'type' 	=> \Elementor\Controls_Manager::URL,
					
				]
			);

			$this->add_control(
				'bgcolor_1',
				[
					'label' => __( 'Background Gradient color 1', 'ova-framework' ),
					'type' 	=> Controls_Manager::COLOR,
					'default' => '#7566e3',
				]
			);

			$this->add_control(
				'bgcolor_2',
				[
					'label' => __( 'Background Gradient color 2', 'ova-framework' ),
					'type' 	=> Controls_Manager::COLOR,
					'default' => '#ce4d86',
				]
			);

			$this->add_responsive_control(
				'padding_barnner',
				[
					'label' 	 => __( 'Padding', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-banner-4 .home-section' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

		$this->end_controls_section();

		// section style content
		$this->start_controls_section(
			'section_content_row_style',
			[
				'label' => __( 'Content', 'ova-framework' ),
				'tab' 	=> Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_responsive_control(
				'padding_content',
				[
					'label' 	 => __( 'Padding', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-banner-4 .home-section .container .row .padding-top-50' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;',
					],
				]
			);

			$this->add_responsive_control(
				'margin_content',
				[
					'label' 	 => __( 'Margin', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-banner-4 .home-section .container .row .padding-top-50' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;',
					],
				]
			);

			$this->add_responsive_control(
	            'align_content',
	            [
	                'label' => __( 'Alignment', 'ova_framework' ),
	                'type' 	=> Controls_Manager::CHOOSE,
	                'options' => [
	                    'left' => [
	                        'title' => __( 'Left', 'ova_framework' ),
	                        'icon' 	=> 'eicon-text-align-left',
	                    ],
	                    'center' => [
	                        'title' => __( 'Center', 'ova_framework' ),
	                        'icon' 	=> 'eicon-text-align-center',
	                    ],
	                    'right' => [
	                        'title' => __( 'Right', 'ova_framework' ),
	                        'icon' 	=> 'eicon-text-align-right',
	                    ],
	                    'justify' => [
	                        'title' => __( 'Justified', 'ova_framework' ),
	                        'icon' => 'eicon-text-align-justify',
                    	],
	                ],
	                'selectors' => [
	                    '{{WRAPPER}} .ova-banner-4 .home-section .container .row .padding-top-50' => 'text-align: {{VALUE}}',
	                ],
	            ]
	        );

		$this->end_controls_section();
		// end section style content

		// section style title
		$this->start_controls_section(
			'section_title_style',
			[
				'label' => __( 'Title', 'ova-framework' ),
				'tab' 	=> Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'title_typography',
					'selector' => '{{WRAPPER}} .ova-banner-4 .home-section .container .row .padding-top-50 h1',
				]
			);

			$this->add_control(
				'color_title',
				[
					'label' => __( 'Color', 'ova-framework' ),
					'type' 	=> Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .ova-banner-4 .home-section .container .row .padding-top-50 h1' => 'color : {{VALUE}};',
					],
				]
			);

			$this->add_responsive_control(
				'padding_title',
				[
					'label' 	 => __( 'Padding', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-banner-4 .home-section .container .row .padding-top-50 h1' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->add_responsive_control(
				'margin_title',
				[
					'label' 	 => __( 'Margin', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-banner-4 .home-section .container .row .padding-top-50 h1' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

		$this->end_controls_section();
		// end section style title

		// section style description
		$this->start_controls_section(
			'section_description_style',
			[
				'label' => __( 'Description', 'ova-framework' ),
				'tab' 	=> Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'description_typography',
					'selector' => '{{WRAPPER}} .ova-banner-4 .home-section .container .row .padding-top-50 p',
				]
			);

			$this->add_control(
				'color_description',
				[
					'label' => __( 'Color', 'ova-framework' ),
					'type' 	=> Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .ova-banner-4 .home-section .container .row .padding-top-50 p' => 'color : {{VALUE}};',
					],
				]
			);

			$this->add_responsive_control(
				'padding_description',
				[
					'label' 	 => __( 'Padding', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-banner-4 .home-section .container .row .padding-top-50 p' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->add_responsive_control(
				'margin_description',
				[
					'label' 	 => __( 'Margin', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-banner-4 .home-section .container .row .padding-top-50 p' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

		$this->end_controls_section();
		// end section style description

		// section style button
		$this->start_controls_section(
			'section_button_style',
			[
				'label' => __( 'Button', 'ova-framework' ),
				'tab' 	=> Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'button_typography',
					'selector' => '{{WRAPPER}} .ova-banner-4 .home-section .container .row .padding-top-50 a',
				]
			);

			$this->start_controls_tabs( 'tabs_button_style' );

		        $this->start_controls_tab(
		            'tab_button_normal',
		            [
		                'label' => __( 'Normal', 'ova_framework' ),
		            ]
		        );

		        	$this->add_control(
						'bg_button',
						[
							'label' => __( 'Background', 'ova-framework' ),
							'type' 	=> Controls_Manager::COLOR,
							'selectors' => [
								'{{WRAPPER}} .ova-banner-4 .home-section .container .row .padding-top-50 a' => 'background-color: {{VALUE}};',
							],
						]
					);

					$this->add_control(
						'border_button',
						[
							'label' => __( 'Border Color', 'ova-framework' ),
							'type' 	=> Controls_Manager::COLOR,
							'selectors' => [
								'{{WRAPPER}} .ova-banner-4 .home-section .container .row .padding-top-50 a' => 'border-color: {{VALUE}};',
							],
						]
					);

        			$this->add_control(
						'color_button',
						[
							'label' => __( 'Color', 'ova-framework' ),
							'type' 	=> Controls_Manager::COLOR,
							'selectors' => [
								'{{WRAPPER}} .ova-banner-4 .home-section .container .row .padding-top-50 a' => 'color: {{VALUE}};',
							],
						]
					);

		        $this->end_controls_tab();


		        $this->start_controls_tab(
		            'tab_button_hover',
		            [
		                'label' => __( 'Hover', 'ova_framework' ),
		            ]
		        );

		        	$this->add_control(
						'bg_button_hover',
						[
							'label' => __( 'Background', 'ova-framework' ),
							'type' 	=> Controls_Manager::COLOR,
							'selectors' => [
								'{{WRAPPER}} .ova-banner-4 .home-section .container .row .padding-top-50 a:hover' => 'background-color: {{VALUE}};',
							],
						]
					);

					$this->add_control(
						'border_button_hover',
						[
							'label' => __( 'Border Color', 'ova-framework' ),
							'type' 	=> Controls_Manager::COLOR,
							'selectors' => [
								'{{WRAPPER}} .ova-banner-4 .home-section .container .row .padding-top-50 a:hover' => 'border-color: {{VALUE}};',
							],
						]
					);

        			$this->add_control(
						'color_button_hover',
						[
							'label' => __( 'Color', 'ova-framework' ),
							'type' 	=> Controls_Manager::COLOR,
							'selectors' => [
								'{{WRAPPER}} .ova-banner-4 .home-section .container .row .padding-top-50 a:hover' => 'color: {{VALUE}};',
							],
						]
					);

		        $this->end_controls_tab();

		    $this->end_controls_tabs();

		    $this->add_group_control(
	            Group_Control_Border::get_type(), [
	                'name' 		=> 'button_border',
	                'selector' 	=> '{{WRAPPER}} .ova-banner-4 .home-section .container .row .padding-top-50 a',
	                'separator' => 'before',
	            ]
	        );

	        $this->add_control(
	            'button_border_radius',
	            [
	                'label' => __( 'Border Radius', 'ova_framework' ),
	                'type' 	=> Controls_Manager::DIMENSIONS,
	                'size_units' => [ 'px', '%' ],
	                'selectors'  => [
	                    '{{WRAPPER}} .ova-banner-4 .home-section .container .row .padding-top-50 a' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
	                ],
	            ]
	        );

			$this->add_responsive_control(
				'padding_button',
				[
					'label' 	 => __( 'Padding', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-banner-4 .home-section .container .row .padding-top-50 a' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->add_responsive_control(
				'margin_button',
				[
					'label' 	 => __( 'Margin', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-banner-4 .home-section .container .row .padding-top-50 a' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

		$this->end_controls_section();
		// end section style buttom

		// section style video
		$this->start_controls_section(
			'section_video_style',
			[
				'label' => __( 'Video', 'ova-framework' ),
				'tab' 	=> Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_responsive_control(
				'padding_video',
				[
					'label' 	 => __( 'Padding', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-banner-4 .home-section .container .row .video-youtube' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->add_responsive_control(
				'margin_video',
				[
					'label' 	 => __( 'Margin', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-banner-4 .home-section .container .row .video-youtube' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->add_group_control(
	            Group_Control_Border::get_type(), [
	                'name' 		=> 'video_border',
	                'selector' 	=> '{{WRAPPER}} .ova-banner-4 .home-section .container .row .video-youtube iframe',
	                'separator' => 'before',
	            ]
	        );

	        $this->add_control(
	            'button_video_radius',
	            [
	                'label' => __( 'Border Radius', 'ova_framework' ),
	                'type' 	=> Controls_Manager::DIMENSIONS,
	                'size_units' => [ 'px', '%' ],
	                'selectors'  => [
	                    '{{WRAPPER}} .ova-banner-4 .home-section .container .row .video-youtube iframe' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
	                ],
	            ]
	        );

		$this->end_controls_section();
		// end section style video

	}

	protected function render() {
		$settings = $this->get_settings();
		$is_external = $settings['link']['is_external'] == 'on' ? '_blank' : '_self';
		$nofollow = $settings['link']['nofollow'] == true ? 'rel="nofollow"' : '';

		?>
		<div class="ova-banner-4">
			<div class="home-section" id="home_wrapper" style="
				    background: <?php echo $settings['bgcolor_1']; ?>;
				    background: -webkit-linear-gradient(left top, <?php echo $settings['bgcolor_1']; ?>, <?php echo $settings['bgcolor_2']; ?>);
				    background: -o-linear-gradient(bottom right, <?php echo $settings['bgcolor_1']; ?>, <?php echo $settings['bgcolor_2']; ?>);
				    background: -moz-linear-gradient(bottom right, <?php echo $settings['bgcolor_1']; ?>, <?php echo $settings['bgcolor_2']; ?>);
				    background: linear-gradient(to bottom right, <?php echo $settings['bgcolor_1']; ?>, <?php echo $settings['bgcolor_2']; ?>);
				    background-size: cover;
			">
				<!--begin container -->
				<div class="container">
					<!--begin row -->
					<div class="row">


						<!--begin col-md-6-->
			            <div class="col-md-6 padding-top-50">

			          		<h1><?php echo esc_html($settings['title']) ?></h1>

			          		<p><?php echo esc_html($settings['desc']) ?></p>

							<a href="<?php echo esc_attr($settings['link']['url']) ?>" class="btn-white" target="<?php echo $is_external; ?>" <?php echo $nofollow; ?> ><?php echo esc_html($settings['text_button']) ?></a>

			            </div>
			            <!--end col-md-6-->
			       
						<!--begin col-md-6-->
			            <div class="col-md-6">
			            	<div class="video-youtube" id="video-l" video-url="<?php echo esc_attr($settings['link_video']['url']) ?>">
			            		
			            	</div> 	
			            </div>
			            <!--end col-md-6-->


					</div>
					<!--end row -->
				</div>
				<!--end container -->
			</div>
			<!--end home section -->
		</div>
		
		
		<?php
	}
}