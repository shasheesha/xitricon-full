<?php
namespace ova_framework\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Core\Schemes\Typography;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Utils;
use Elementor\Repeater;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class ova_banner_2 extends Widget_Base {

	public function get_name() {
		return 'ova_banner_2';
	}

	public function get_title() {
		return __( 'Banner 2', 'ova-framework' );
	}

	public function get_icon() {
		return 'eicon-banner';
	}

	public function get_categories() {
		return [ 'ovatheme' ];
	}

	protected function register_controls() {

		$this->start_controls_section(
			'section_content',
			[
				'label' => __( 'Content', 'ova-framework' ),
			]
		);

		$this->add_control(
			'title',
			[
				'label' 	=> __( 'Title', 'ova-framework' ),
				'type' 		=> Controls_Manager::TEXT,
				'default' 	=> __('Do Not Wait - Launch Your Startup Today!','ova-framework')
			]
		);

		$this->add_control(
			'desc',
			[
				'label' 	=> __( 'Description', 'ova-framework' ),
				'type' 		=> Controls_Manager::TEXTAREA,
				'default' 	=> __('Curabitur quam etsum lacus etsumis nulatis etsumised netsum ets vitae
					varius loremis sed lorem nets feugiat netis ligula etsumis vitae.','ova-framework')
			]
		);

		$this->add_control(
			'shortcode',
			[
				'label' => __( 'Short Code Form', 'ova-framework' ),
				'type' 	=> Controls_Manager::TEXTAREA,
				'label_block' => true,
			]
		);

		$this->add_control(
			'img_bg',
			[
				'label' => __( 'Back Ground Image', 'ova-framework' ),
				'type' 	=> Controls_Manager::MEDIA,
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_content_style',
			[
				'label' => __( 'Style', 'ova-framework' ),
			]
		);

			$this->add_control(
				'bgcolor_1',
				[
					'label' 	=> __( 'Background Gradient color 1', 'ova-framework' ),
					'type' 		=> Controls_Manager::COLOR,
					'default' 	=> '#4576c3',
				]
			);
			$this->add_control(
				'bgcolor_2',
				[
					'label' 	=> __( 'Background Gradient color 2', 'ova-framework' ),
					'type' 		=> Controls_Manager::COLOR,
					'default' 	=> '#5547bf',
				]
			);

			$this->add_control(
				'bgcolor_3',
				[
					'label' 	=> __( 'Background Gradient color 3', 'ova-framework' ),
					'type' 		=> Controls_Manager::COLOR,
					'default' 	=> '#9045c0',
				]
			);
			$this->add_control(
				'bgcolor_4',
				[
					'label' 	=> __( 'Background Gradient color 4', 'ova-framework' ),
					'type' 		=> Controls_Manager::COLOR,
					'default' 	=> '#be47b2',
				]
			);

			$this->add_responsive_control(
				'padding_barnner',
				[
					'label' 	 => __( 'Padding', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-banner-2 .banner-2' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

		$this->end_controls_section();

		// section style content
		$this->start_controls_section(
			'section_content_row_style',
			[
				'label' => __( 'Content', 'ova-framework' ),
				'tab' 	=> Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_responsive_control(
				'padding_content',
				[
					'label' 	 => __( 'Padding', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-banner-2 .banner-2 .content' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->add_responsive_control(
				'margin_content',
				[
					'label' 	 => __( 'Margin', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-banner-2 .banner-2 .content' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->add_responsive_control(
	            'align_content',
	            [
	                'label' => __( 'Alignment', 'ova_framework' ),
	                'type' 	=> Controls_Manager::CHOOSE,
	                'options' => [
	                    'left' => [
	                        'title' => __( 'Left', 'ova_framework' ),
	                        'icon' 	=> 'eicon-text-align-left',
	                    ],
	                    'center' => [
	                        'title' => __( 'Center', 'ova_framework' ),
	                        'icon' 	=> 'eicon-text-align-center',
	                    ],
	                    'right' => [
	                        'title' => __( 'Right', 'ova_framework' ),
	                        'icon' 	=> 'eicon-text-align-right',
	                    ],
	                    'justify' => [
	                        'title' => __( 'Justified', 'ova_framework' ),
	                        'icon' => 'eicon-text-align-justify',
                    	],
	                ],
	                'selectors' => [
	                    '{{WRAPPER}} .ova-banner-2 .banner-2 .content' => 'text-align: {{VALUE}}',
	                ],
	            ]
	        );

		$this->end_controls_section();
		// end section style content

		// section style title
		$this->start_controls_section(
			'section_title_style',
			[
				'label' => __( 'Title', 'ova-framework' ),
				'tab' 	=> Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'title_typography',
					'selector' => '{{WRAPPER}} .ova-banner-2 .banner-2 .content h1',
				]
			);

			$this->add_control(
				'color_title',
				[
					'label' => __( 'Color', 'ova-framework' ),
					'type' 	=> Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .ova-banner-2 .banner-2 .content h1' => 'color : {{VALUE}};',
					],
				]
			);

			$this->add_responsive_control(
				'padding_title',
				[
					'label' 	 => __( 'Padding', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-banner-2 .banner-2 .content h1' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->add_responsive_control(
				'margin_title',
				[
					'label' 	 => __( 'Margin', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-banner-2 .banner-2 .content h1' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

		$this->end_controls_section();
		// end section style title

		// section style description
		$this->start_controls_section(
			'section_description_style',
			[
				'label' => __( 'Description', 'ova-framework' ),
				'tab' 	=> Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'description_typography',
					'selector' => '{{WRAPPER}} .ova-banner-2 .banner-2 .content p',
				]
			);

			$this->add_control(
				'color_description',
				[
					'label' => __( 'Color', 'ova-framework' ),
					'type' 	=> Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .ova-banner-2 .banner-2 .content p' => 'color : {{VALUE}};',
					],
				]
			);

			$this->add_responsive_control(
				'padding_description',
				[
					'label' 	 => __( 'Padding', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-banner-2 .banner-2 .content p' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->add_responsive_control(
				'margin_description',
				[
					'label' 	 => __( 'Margin', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-banner-2 .banner-2 .content p' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

		$this->end_controls_section();
		// end section style description

	}

	protected function render() {
		$settings = $this->get_settings();

		$background_image = $settings['img_bg']['url'];

		?>
		<div class="ova-banner-2">
			<div class="banner-2" style="background-image:url(<?php echo esc_attr($background_image) ?>)">
				<div class="banner-2-overlay" style="
					    background: linear-gradient(-45deg, <?php echo $settings['bgcolor_1'] ?>, <?php echo $settings['bgcolor_2'] ?>, <?php echo $settings['bgcolor_3'] ?>, <?php echo $settings['bgcolor_4'] ?>);
					    background-size: 400% 400%;
				"></div>
					<div class="content">
						<h1><?php echo esc_html($settings['title']) ?></h1>
						<p><?php echo esc_html($settings['desc']) ?></p>
					</div>
					<div class="form-shortcode">
						<?php echo do_shortcode($settings['shortcode']) ?>
					</div> 	
			</div>
			

			
		</div>
		
		
		<?php
	}
}