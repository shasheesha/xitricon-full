<?php
namespace ova_framework\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Core\Schemes\Typography;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Utils;
use Elementor\Repeater;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class ova_banner_1 extends Widget_Base {

	public function get_name() {
		return 'ova_banner_1';
	}

	public function get_title() {
		return __( 'Banner 1', 'ova-framework' );
	}

	public function get_icon() {
		return 'eicon-banner';
	}

	public function get_categories() {
		return [ 'ovatheme' ];
	}

	public function get_script_depends() {
		return [ 'script-elementor' ];
	}

	protected function register_controls() {

		$animation_array =array(
			'bounce'  => 'bounce',
			'flash'  => 'flash',
			'pulse'  => 'pulse',
			'rubberBand'  => 'rubberBand',
			'shake'  => 'shake',
			'swing'  => 'swing',
			'tada'  => 'tada',
			'wobble'  => 'wobble',
			'jello'  => 'jello',
			'bounceIn'  => 'bounceIn',
			'bounceInDown'  => 'bounceInDown',
			'bounceInLeft'  => 'bounceInLeft',
			'bounceInRight'  => 'bounceInRight',
			'bounceInUp'  => 'bounceInUp',
			'bounceOut'  => 'bounceOut',
			'bounceOutDown'  => 'bounceOutDown',
			'bounceOutLeft'  => 'bounceOutLeft',
			'bounceOutRight'  => 'bounceOutRight',
			'bounceOutUp'  => 'bounceOutUp',
			'fadeIn'  => 'fadeIn',
			'fadeInDown'  => 'fadeInDown',
			'fadeInDownBig'  => 'fadeInDownBig',
			'fadeInLeft'  => 'fadeInLeft',
			'fadeInLeftBig'  => 'fadeInLeftBig',
			'fadeInRight'  => 'fadeInRight',
			'fadeInRightBig'  => 'fadeInRightBig',
			'fadeInUp'  => 'fadeInUp',
			'fadeInUpBig'  => 'fadeInUpBig',
			'fadeOut'  => 'fadeOut',
			'fadeOutDown'  => 'fadeOutDown',
			'fadeOutDownBig'  => 'fadeOutDownBig',
			'fadeOutLeft'  => 'fadeOutLeft',
			'fadeOutLeftBig'  => 'fadeOutLeftBig',
			'fadeOutRight'  => 'fadeOutRight',
			'fadeOutRightBig'  => 'fadeOutRightBig',
			'fadeOutUp'  => 'fadeOutUp',
			'fadeOutUpBig'  => 'fadeOutUpBig',
			'flip'  => 'flip',
			'flipInX'  => 'flipInX',
			'flipInY'  => 'flipInY',
			'flipOutX'  => 'flipOutX',
			'flipOutY'  => 'flipOutY',
			'lightSpeedIn'  => 'lightSpeedIn',
			'lightSpeedOut'  => 'lightSpeedOut',
			'rotateIn'  => 'rotateIn',
			'rotateInDownLeft'  => 'rotateInDownLeft',
			'rotateInDownRight'  => 'rotateInDownRight',
			'rotateInUpLeft'  => 'rotateInUpLeft',
			'rotateInUpRight'  => 'rotateInUpRight',
			'rotateOut'  => 'rotateOut',
			'rotateOutDownLeft'  => 'rotateOutDownLeft',
			'rotateOutDownRight'  => 'rotateOutDownRight',
			'rotateOutUpLeft'  => 'rotateOutUpLeft',
			'rotateOutUpRight'  => 'rotateOutUpRight',
			'slideInUp'  => 'slideInUp',
			'slideInDown'  => 'slideInDown',
			'slideInLeft'  => 'slideInLeft',
			'slideInRight'  => 'slideInRight',
			'slideOutUp'  => 'slideOutUp',
			'slideOutDown'  => 'slideOutDown',
			'slideOutLeft'  => 'slideOutLeft',
			'slideOutRight'  => 'slideOutRight',
			'zoomIn'  => 'zoomIn',
			'zoomInDown'  => 'zoomInDown',
			'zoomInLeft'  => 'zoomInLeft',
			'zoomInRight'  => 'zoomInRight',
			'zoomInUp'  => 'zoomInUp',
			'zoomOut'  => 'zoomOut',
			'zoomOutDown'  => 'zoomOutDown',
			'zoomOutLeft'  => 'zoomOutLeft',
			'zoomOutRight'  => 'zoomOutRight',
			'zoomOutUp'  => 'zoomOutUp',
			'hinge'  => 'hinge',
			'jackInTheBox'  => 'jackInTheBox',
			'rollIn'  => 'rollIn',
			'rollOut'  => 'rollOut'
		);

		$this->start_controls_section(
			'section_content',
			[
				'label' => __( 'Content', 'ova-framework' ),
			]
		);

		$this->add_control(
			'title_form',
			[
				'label' => __( 'Title Form', 'ova-framework' ),
				'type' 	=> Controls_Manager::TEXT,
				'label_block' => true,
				'default' 	  => __('Get Your 30 Days Free Trial','ova-framework')
			]
		);

		$this->add_control(
			'shortcode',
			[
				'label' => __( 'Short Code Form', 'ova-framework' ),
				'type' 	=> Controls_Manager::TEXTAREA,
				'label_block' => true,
			]
		);

		$this->add_control(
			'title',
			[
				'label' => __( 'Title', 'ova-framework' ),
				'type' 	=> Controls_Manager::TEXT,
				'label_block' => true,
				'default' => __('Ready To Promote Your New Business With Howdy?','ova-framework')
			]
		);

		$this->add_control(
			'desc',
			[
				'label' => __( 'Description', 'ova-framework' ),
				'type' 	=> Controls_Manager::TEXTAREA,
				'default' => __('Curabitur quam etsum lacus etsumis nulatis etsumised vitae nisledita varius loremis seditum netsum aliquam.', 'ova-framework'),
			]
		);

		$repeater = new \Elementor\Repeater();

		$repeater->add_control(
			'text',
			[
				'label' => __( 'Text ', 'ova-framework' ),
				'type' 	=> \Elementor\Controls_Manager::TEXT,
				'default' => __( 'This is text', 'ova-framework' ),
			]
		);

		$repeater->add_control(
			'icon',
			[
				'label' 	=> __( 'Icon ', 'ova-framework' ),
				'type' 		=> \Elementor\Controls_Manager::TEXT,
				'default' 	=> 'fa fa-check',
			]
		);

		$this->add_control(
			'tabs',
			[
				'label' 		=> __( 'Items', 'ova-framework' ),
				'type' 			=> Controls_Manager::REPEATER,
				'fields' 		=> $repeater->get_controls(),
				'title_field' 	=> '{{{ text }}}',
				'default' 		=> [
					[
						'text' => __( 'Increase brand awareness and sales.', 'ova-framework' ),
					],
					[
						'text' => __( 'Rank higher on Google search engine.', 'ova-framework' ),
					],
					[
						'text' => __( 'Double your actual website traffic.', 'ova-framework' ),
					],
					[
						'text' => __( 'Outrank your competitors.', 'ova-framework' ),
					],
				],
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_content_style',
			[
				'label' => __( 'Style', 'ova-framework' ),
			]
		);

			$this->add_control(
				'bgcolor_1',
				[
					'label' => __( 'Background Gradient color 1', 'ova-framework' ),
					'type' 	=> Controls_Manager::COLOR,
					'default' => '#7566e3',
				]
			);
			$this->add_control(
				'bgcolor_2',
				[
					'label' => __( 'Background Gradient color 2', 'ova-framework' ),
					'type' 	=> Controls_Manager::COLOR,
					'default' => '#ce4d86',
				]
			);

			$this->add_control(
				'bg_check_item',
				[
					'label' => __( 'Background Check Item', 'ova-framework' ),
					'type' 	=> Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} ul.home-benefits li i' => 'background-color: {{VALUE}};',
					],
					
					'default' 	=> '#fccd05',
				]
			);

			$this->add_control(
				'bg_button_contact',
				[
					'label' => __( 'Background Button Form', 'ova-framework' ),
					'type' 	=> Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .ova-banner-1 .wp-form-register p input[type=submit]' => 'background-color: {{VALUE}}; border-color: {{VALUE}};'
					],
					'default' 	=> '#fccd05',
				]
			);

			$this->add_control(
				'bg_button_hover_contact',
				[
					'label' => __( 'Background Button Form', 'ova-framework' ),
					'type' 	=> Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .ova-banner-1 .wp-form-register p input[type=submit]:hover' => 'background-color: {{VALUE}}; border-color: {{VALUE}};'
					],
					'default' 	=> '#343434',
				]
			);

			$this->add_responsive_control(
				'padding_barnner',
				[
					'label' 	 => __( 'Padding', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-banner-1 .home-section' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_content_animate',
			[
				'label' => __( 'Animate', 'ova-framework' ),
			]
		);


		$this->add_control(
			'heading_animate_title',
			[
				'label' => __( 'Animate Title', 'ova-framework' ),
				'type' 	=> \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);


		$this->add_control(
			'show_animation_title',
			[
				'label' => __( 'Show Animate', 'ova-framework' ),
				'type' 	=> Controls_Manager::SWITCHER,
				'default' => '',
			]
		);

		$this->add_control(
			'animate_style_title',
			[
				'label' 	=> __( 'Type Animate', 'ova-framework' ),
				'type' 		=> Controls_Manager::SELECT,
				'options' 	=> $animation_array,
				'default' 	=> 'fadeInUp',
				'condition' => [
					'show_animation_title' => 'yes',
				],
			]
		);

		$this->add_control(
			'animation_dur_title',
			[
				'label' 	=> __( 'Time Animation', 'ova-framework' ),
				'type' 		=> \Elementor\Controls_Manager::NUMBER,
				'default' 	=> 500,
				'condition' => [
					'show_animation_title' => 'yes',
				],
			]
		);

		//animate subtitle
		$this->add_control(
			'heading_animate_sub_title',
			[
				'label' => __( 'Animate Sub Title', 'ova-framework' ),
				'type' 	=> \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		$this->add_control(
			'show_animation_sub_title',
			[
				'label' => __( 'Show Animate', 'ova-framework' ),
				'type' 	=> Controls_Manager::SWITCHER,
				'default' => '',
			]
		);

		$this->add_control(
			'animate_style_sub_title',
			[
				'label' 	=> __( 'Type Animate', 'ova-framework' ),
				'type' 		=> Controls_Manager::SELECT,
				'options' 	=> $animation_array,
				'default' 	=> 'fadeInUp',
				'condition' => [
					'show_animation_sub_title' => 'yes',
				],
			]
		);

		$this->add_control(
			'animation_dur_sub_title',
			[
				'label' 	=> __( 'Time Animation', 'ova-framework' ),
				'type' 		=> \Elementor\Controls_Manager::NUMBER,
				'default' 	=> 1000,
				'condition' => [
					'show_animation_sub_title' => 'yes',
				],
			]
		);

		//animate desciption
		$this->add_control(
			'heading_animate_desc',
			[
				'label' => __( 'Animate Description', 'ova-framework' ),
				'type' 	=> \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		$this->add_control(
			'show_animation_desc',
			[
				'label' => __( 'Show Animate', 'ova-framework' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => '',
			]
		);

		$this->add_control(
			'animate_style_desc',
			[
				'label' 	=> __( 'Type Animate', 'ova-framework' ),
				'type' 		=> Controls_Manager::SELECT,
				'options' 	=> $animation_array,
				'default' 	=> 'fadeInUp',
				'condition' => [
					'show_animation_desc' => 'yes',
				],
			]
		);

		$this->add_control(
			'animation_dur_desc',
			[
				'label' 	=> __( 'Time Animation', 'ova-framework' ),
				'type' 		=> \Elementor\Controls_Manager::NUMBER,
				'default' 	=> 1000,
				'condition' => [
					'show_animation_desc' => 'yes',
				],
			]
		);

		$this->add_control(
			'animation_dur_delay_desc',
			[
				'label' 	=> __( 'Time Delay Animation Item', 'ova-framework' ),
				'type' 		=> \Elementor\Controls_Manager::NUMBER,
				'default' 	=> 500,
				'condition' => [
					'show_animation_desc' => 'yes',
				],
			]
		);

		//animate form
		$this->add_control(
			'heading_animate_form',
			[
				'label' => __( 'Animate Form', 'ova-framework' ),
				'type' 	=> \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		
		$this->add_control(
			'show_animation_form',
			[
				'label' 	=> __( 'Show Animate', 'ova-framework' ),
				'type' 		=> Controls_Manager::SWITCHER,
				'default' 	=> '',
			]
		);

		$this->add_control(
			'animate_style_form',
			[
				'label' 	=> __( 'Type Animate', 'ova-framework' ),
				'type' 		=> Controls_Manager::SELECT,
				'options' 	=> $animation_array,
				'default' 	=> 'fadeInUp',
				'condition' => [
					'show_animation_form' => 'yes',
				],
			]
		);

		$this->add_control(
			'animation_dur_form',
			[
				'label' 	=> __( 'Time Animation', 'ova-framework' ),
				'type' 		=> \Elementor\Controls_Manager::NUMBER,
				'default' 	=> 1000,
				'condition' => [
					'show_animation_form' => 'yes',
				],
			]
		);

		$this->end_controls_section();

		// section style form
		$this->start_controls_section(
			'section_form_style',
			[
				'label' => __( 'Form', 'ova-framework' ),
				'tab' 	=> Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_control(
				'bg_form',
				[
					'label' => __( 'Background', 'ova-framework' ),
					'type' 	=> Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .ova-banner-1 .home-section .container .row .wp-form-register' => 'background-color: {{VALUE}};',
					],
				]
			);

			$this->add_responsive_control(
				'padding_form',
				[
					'label' 	 => __( 'Padding', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-banner-1 .home-section .container .row .wp-form-register' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->add_responsive_control(
				'margin_form',
				[
					'label' 	 => __( 'Margin', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-banner-1 .home-section .container .row .wp-form-register' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->add_group_control(
				Group_Control_Box_Shadow::get_type(),
				[
					'name' => 'form_shadow',
					'selector' => '{{WRAPPER}} .ova-banner-1 .home-section .container .row .wp-form-register',
				]
			);

		$this->end_controls_section();
		// end section style title form

		// section style title form
		$this->start_controls_section(
			'section_title_form_style',
			[
				'label' => __( 'Title Form', 'ova-framework' ),
				'tab' 	=> Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'title_form_typography',
					'selector' => '{{WRAPPER}} .ova-banner-1 .home-section .container .row .wp-form-register h3',
				]
			);

			$this->add_control(
				'color_title_form',
				[
					'label' => __( 'Color', 'ova-framework' ),
					'type' 	=> Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .ova-banner-1 .home-section .container .row .wp-form-register h3' => 'color : {{VALUE}};',
					],
				]
			);

			$this->add_responsive_control(
				'padding_title_form',
				[
					'label' 	 => __( 'Padding', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-banner-1 .home-section .container .row .wp-form-register h3' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->add_responsive_control(
				'margin_title_form',
				[
					'label' 	 => __( 'Margin', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-banner-1 .home-section .container .row .wp-form-register h3' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->add_responsive_control(
	            'align_title_form',
	            [
	                'label' => __( 'Alignment', 'ova_framework' ),
	                'type' 	=> Controls_Manager::CHOOSE,
	                'options' => [
	                    'left' => [
	                        'title' => __( 'Left', 'ova_framework' ),
	                        'icon' 	=> 'eicon-text-align-left',
	                    ],
	                    'center' => [
	                        'title' => __( 'Center', 'ova_framework' ),
	                        'icon' 	=> 'eicon-text-align-center',
	                    ],
	                    'right' => [
	                        'title' => __( 'Right', 'ova_framework' ),
	                        'icon' 	=> 'eicon-text-align-right',
	                    ],
	                    'justify' => [
	                        'title' => __( 'Justified', 'ova_framework' ),
	                        'icon' => 'eicon-text-align-justify',
                    	],
	                ],
	                'selectors' => [
	                    '{{WRAPPER}} .ova-banner-1 .home-section .container .row .wp-form-register h3' => 'text-align: {{VALUE}}',
	                ],
	            ]
	        );

		$this->end_controls_section();
		// end section style title form

		// section style content
		$this->start_controls_section(
			'section_content_row_style',
			[
				'label' => __( 'Content', 'ova-framework' ),
				'tab' 	=> Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_responsive_control(
				'padding_content',
				[
					'label' 	 => __( 'Padding', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-banner-1 .home-section .container .row .padding-top-40' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;',
					],
				]
			);

			$this->add_responsive_control(
				'margin_content',
				[
					'label' 	 => __( 'Margin', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-banner-1 .home-section .container .row .padding-top-40' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;',
					],
				]
			);

			$this->add_responsive_control(
	            'align_content',
	            [
	                'label' => __( 'Alignment', 'ova_framework' ),
	                'type' 	=> Controls_Manager::CHOOSE,
	                'options' => [
	                    'left' => [
	                        'title' => __( 'Left', 'ova_framework' ),
	                        'icon' 	=> 'eicon-text-align-left',
	                    ],
	                    'center' => [
	                        'title' => __( 'Center', 'ova_framework' ),
	                        'icon' 	=> 'eicon-text-align-center',
	                    ],
	                    'right' => [
	                        'title' => __( 'Right', 'ova_framework' ),
	                        'icon' 	=> 'eicon-text-align-right',
	                    ],
	                    'justify' => [
	                        'title' => __( 'Justified', 'ova_framework' ),
	                        'icon' => 'eicon-text-align-justify',
                    	],
	                ],
	                'selectors' => [
	                    '{{WRAPPER}} .ova-banner-1 .home-section .container .row .padding-top-40' => 'text-align: {{VALUE}}',
	                ],
	            ]
	        );

		$this->end_controls_section();
		// end section style content

		// section style title
		$this->start_controls_section(
			'section_title_style',
			[
				'label' => __( 'Title', 'ova-framework' ),
				'tab' 	=> Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'title_typography',
					'selector' => '{{WRAPPER}} .ova-banner-1 .home-section .container .row .padding-top-40 h1',
				]
			);

			$this->add_control(
				'color_title',
				[
					'label' => __( 'Color', 'ova-framework' ),
					'type' 	=> Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .ova-banner-1 .home-section .container .row .padding-top-40 h1' => 'color : {{VALUE}};',
					],
				]
			);

			$this->add_responsive_control(
				'padding_title',
				[
					'label' 	 => __( 'Padding', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-banner-1 .home-section .container .row .padding-top-40 h1' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->add_responsive_control(
				'margin_title',
				[
					'label' 	 => __( 'Margin', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-banner-1 .home-section .container .row .padding-top-40 h1' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

		$this->end_controls_section();
		// end section style title

		// section style description
		$this->start_controls_section(
			'section_description_style',
			[
				'label' => __( 'Description', 'ova-framework' ),
				'tab' 	=> Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'description_typography',
					'selector' => '{{WRAPPER}} .ova-banner-1 .home-section .container .row .padding-top-40 p',
				]
			);

			$this->add_control(
				'color_description',
				[
					'label' => __( 'Color', 'ova-framework' ),
					'type' 	=> Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .ova-banner-1 .home-section .container .row .padding-top-40 p' => 'color : {{VALUE}};',
					],
				]
			);

			$this->add_responsive_control(
				'padding_description',
				[
					'label' 	 => __( 'Padding', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-banner-1 .home-section .container .row .padding-top-40 p' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->add_responsive_control(
				'margin_description',
				[
					'label' 	 => __( 'Margin', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-banner-1 .home-section .container .row .padding-top-40 p' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

		$this->end_controls_section();
		// end section style description

		// section style items
		$this->start_controls_section(
			'section_items_style',
			[
				'label' => __( 'Items', 'ova-framework' ),
				'tab' 	=> Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'items_typography',
					'selector' => '{{WRAPPER}} .ova-banner-1 .home-section .container .row .home-benefits li',
				]
			);

			$this->add_control(
				'color_text_items',
				[
					'label' => __( 'Color Text', 'ova-framework' ),
					'type' 	=> Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .ova-banner-1 .home-section .container .row .home-benefits li' => 'color : {{VALUE}};',
					],
				]
			);

			$this->add_control(
				'color_icon_items',
				[
					'label' => __( 'Color Icon', 'ova-framework' ),
					'type' 	=> Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .ova-banner-1 .home-section .container .row .home-benefits li i' => 'color : {{VALUE}};',
					],
				]
			);

			$this->add_responsive_control(
				'padding_items',
				[
					'label' 	 => __( 'Padding', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-banner-1 .home-section .container .row .home-benefits' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->add_responsive_control(
				'margin_items',
				[
					'label' 	 => __( 'Margin', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-banner-1 .home-section .container .row .home-benefits' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

		$this->end_controls_section();
		// end section style description

	}

	protected function render() {

		$settings = $this->get_settings();

		$data_animate_title = $data_animate_sub_title = $data_animate_desc = $data_animate_form = $data_animate_class_title = $data_animate_class_sub_title = $class_animate_desc = $data_animate_class_form = '';

		$time_dur_desc = $time_item = 0;


		if ($settings['show_animation_title'] === 'yes') {

			$data_animate_title = "data-animation='" . $settings['animate_style_title'] . "' data-animation_dur='".$settings['animation_dur_title']."' style='animation-duration: ".$settings['animation_dur_title']."ms; opacity: 1;'";
			$data_animate_class_title = "animated elementor-slide-title " . $settings['animate_style_title'];
		}

		if ($settings['show_animation_sub_title'] === 'yes') {


			$data_animate_sub_title = "data-animation='" . $settings['animate_style_sub_title'] . "' data-animation_dur='".$settings['animation_dur_sub_title']."' style='animation-duration: ".$settings['animation_dur_sub_title']."ms; opacity: 1;'";
			$data_animate_class_sub_title = "animated elementor-slide-sub-title " . $settings['animate_style_sub_title'];
		}

		if ($settings['show_animation_desc'] === 'yes') {

			$data_animate_desc = "data-animation='" . $settings['animate_style_desc'] . "'";
			$class_animate_desc = "animated elementor-slide-desc " . $settings['animate_style_desc'];

			$time_dur_desc = $settings['animation_dur_desc'];
			$time_item = $settings['animation_dur_delay_desc'];
		}

		if ($settings['show_animation_form'] === 'yes') {

			$data_animate_form = "data-animation='" . $settings['animate_style_form'] . "' data-animation_dur='".$settings['animation_dur_form']."' style='animation-duration: ".$settings['animation_dur_form']."ms; opacity: 1;'";
			$data_animate_class_form = "animated elementor-slide-form " . $settings['animate_style_form'];
		}

		?>
		<div class="ova-banner-1">
			<div class="home-section" id="home_wrapper" style="
				    background: <?php echo $settings['bgcolor_1']; ?>;
				    background: -webkit-linear-gradient(left top, <?php echo $settings['bgcolor_1']; ?>, <?php echo $settings['bgcolor_2']; ?>);
				    background: -o-linear-gradient(bottom right, <?php echo $settings['bgcolor_1']; ?>, <?php echo $settings['bgcolor_2']; ?>);
				    background: -moz-linear-gradient(bottom right, <?php echo $settings['bgcolor_1']; ?>, <?php echo $settings['bgcolor_2']; ?>);
				    background: linear-gradient(to bottom right, <?php echo $settings['bgcolor_1']; ?>, <?php echo $settings['bgcolor_2']; ?>);
				    background-size: cover;
			">
				<!--begin container -->
				<div class="container">
					<!--begin row -->
					<div class="row">
						<!--begin col-md-7-->
						<div class="col-md-7 padding-top-40">
							<h1 <?php echo $data_animate_title ?> class="<?php echo $data_animate_class_title ?>"  ><?php echo esc_html($settings['title']) ?></h1>
							<p <?php echo $data_animate_sub_title ?>  class="<?php echo $data_animate_class_sub_title  ?>" ><?php echo esc_html($settings['desc']) ?></p>
							<ul class="home-benefits " >
								<?php 
								if (!empty($settings['tabs'])) : foreach ($settings['tabs'] as $item) : ?>
									<?php if ( !empty( $item['text'] ) && !empty( $item['icon'] ) ): ?>
									<li <?php echo $data_animate_desc ?> class="<?php echo $class_animate_desc ?>" data-animation_dur="<?php echo $time_dur_desc ?>" style="animation-duration: <?php echo $time_dur_desc ?>ms; opacity: 1;" ><i class="<?php echo esc_attr($item['icon']) ?>"></i><?php echo esc_html($item['text']) ?></li>
									<?php
								endif;
									$time_dur_desc += $time_item;
								endforeach; endif; 
								?>
							</ul>
						</div>
						<!--end col-md-7-->
						<!--begin col-md-5-->
						<div class="col-md-5">

							<div <?php echo $data_animate_form ?> class="wp-form-register <?php echo $data_animate_class_form ?>" >
								<h3><?php echo esc_html($settings['title_form']) ?></h3>
								<div class="ova-form-register">
									<?php echo $settings['shortcode'] ?>
								</div>
							</div>
						</div>
						<!--end col-md-5-->
					</div>
					<!--end row -->
				</div>
				<!--end container -->
			</div>
			<!--end home section -->
		</div>
		
		
		<?php
	}
}