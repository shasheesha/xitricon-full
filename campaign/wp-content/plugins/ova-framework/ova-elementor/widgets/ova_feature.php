<?php
namespace ova_framework\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Core\Schemes\Typography;
use Elementor\Group_Control_Typography;
use Elementor\Core\Schemes\Color;
use Elementor\Group_Control_Border;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


class ova_feature extends Widget_Base {

	public function get_name() {
		return 'ova_feature';
	}

	public function get_title() {
		return __( 'Feature', 'ova-framework' );
	}

	public function get_icon() {
		return 'eicon-social-icons';
	}

	public function get_categories() {
		return [ 'ovatheme' ];
	}

	public function get_script_depends() {
		return [ 'script-elementor' ];
	}

	protected function register_controls() {
		$this->start_controls_section(
			'section_content',
			[
				'label' => __( 'Feature', 'ova-framework' ),
			]
		);

		$this->add_control(
			'icon',
			[
				'label' 	=> __( 'Icon', 'ova-framework' ),
				'type' 		=> \Elementor\Controls_Manager::TEXT,
				'default' 	=> 'pe-7s-tools',
			]
		);

		$this->add_control(
			'title',
			[
				'label' 	=> __( 'Title', 'ova-framework' ),
				'type' 		=> \Elementor\Controls_Manager::TEXT,
				'default' 	=> 'flaticon-circus-tent',
			]
		);

		$this->add_control(
			'content',
			[
				'label' 	=> __( 'Icons Muzze', 'ova-framework' ),
				'type' 		=> \Elementor\Controls_Manager::TEXTAREA,
				'default' 	=> __('Utise wisi enim minim veniam, quis tation ullamcorper suscipit et loboti nisl consequat nihis.', 'ova-framework'),
			]
		);

		$this->add_control(
			'color_icon',
			[
				'label' 	=> __( 'Color Icon', 'ova-framework' ),
				'type' 		=> Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} i:before' => 'color : {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'font_size_icon',
			[
				'label' => __( 'Font Size Icon', 'ova-framework' ),
				'type' 	=> Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 1,
						'max' => 150,
					],
				],
				'selectors' => [
					'{{WRAPPER}} i:before' => 'font-size: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'background_color_icon',
			[
				'label' => __( 'Background Color Icon', 'ova-framework' ),
				'type' 	=> Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .feature-box i' => 'background-color : {{VALUE}};',
				],
			]
		);

		$this->end_controls_section();
		// end tab section_content

				// section style content
		$this->start_controls_section(
			'section_content_style',
			[
				'label' => __( 'Content', 'ova-framework' ),
				'tab' 	=> Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_control(
				'bg_content',
				[
					'label' => __( 'Background', 'ova-framework' ),
					'type' 	=> Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .ova-feature .feature-box' => 'background-color: {{VALUE}};',
					],
				]
			);

			$this->add_responsive_control(
				'padding_content',
				[
					'label' 	 => __( 'Padding', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-feature .feature-box' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->add_responsive_control(
				'margin_content',
				[
					'label' 	 => __( 'Margin', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-feature .feature-box' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->add_responsive_control(
	            'align_content',
	            [
	                'label' => __( 'Alignment', 'ova_framework' ),
	                'type' 	=> Controls_Manager::CHOOSE,
	                'options' => [
	                    'left' => [
	                        'title' => __( 'Left', 'ova_framework' ),
	                        'icon' 	=> 'eicon-text-align-left',
	                    ],
	                    'center' => [
	                        'title' => __( 'Center', 'ova_framework' ),
	                        'icon' 	=> 'eicon-text-align-center',
	                    ],
	                    'right' => [
	                        'title' => __( 'Right', 'ova_framework' ),
	                        'icon' 	=> 'eicon-text-align-right',
	                    ],
	                    'justify' => [
	                        'title' => __( 'Justified', 'ova_framework' ),
	                        'icon' => 'eicon-text-align-justify',
                    	],
	                ],
	                'selectors' => [
	                    '{{WRAPPER}} .ova-feature .feature-box' => 'text-align: {{VALUE}}',
	                ],
	            ]
	        );

		$this->end_controls_section();
		// end section style content

		// section style title
		$this->start_controls_section(
			'section_title_style',
			[
				'label' => __( 'Title', 'ova-framework' ),
				'tab' 	=> Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'title_typography',
					'selector' => '{{WRAPPER}} .ova-feature .feature-box .feature-box-text h4',
				]
			);

			$this->add_control(
				'color_title',
				[
					'label' => __( 'Color', 'ova-framework' ),
					'type' 	=> Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .ova-feature .feature-box .feature-box-text h4' => 'color: {{VALUE}};',
					],
				]
			);

			$this->add_responsive_control(
				'padding_title',
				[
					'label' 	 => __( 'Padding', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-feature .feature-box .feature-box-text h4' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->add_responsive_control(
				'margin_title',
				[
					'label' 	 => __( 'Margin', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-feature .feature-box .feature-box-text h4' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

		$this->end_controls_section();
		// end section style title

		// section style description
		$this->start_controls_section(
			'section_description_style',
			[
				'label' => __( 'Description', 'ova-framework' ),
				'tab' 	=> Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'description_typography',
					'selector' => '{{WRAPPER}} .ova-feature .feature-box .feature-box-text p',
				]
			);

			$this->add_control(
				'color_description',
				[
					'label' => __( 'Color', 'ova-framework' ),
					'type' 	=> Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .ova-feature .feature-box .feature-box-text p' => 'color: {{VALUE}};',
					],
				]
			);

			$this->add_responsive_control(
				'padding_description',
				[
					'label' 	 => __( 'Padding', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-feature .feature-box .feature-box-text p' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->add_responsive_control(
				'margin_description',
				[
					'label' 	 => __( 'Margin', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-feature .feature-box .feature-box-text p' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

		$this->end_controls_section();
		// end section style description

	}

	protected function render() {

		$settings = $this->get_settings_for_display();

		?>
		<div class="ova-feature">
			<div class="feature-box light-green wow fadeIn" >

				<i class="<?php echo $settings['icon'] ?>"></i>

				<div class="feature-box-text">

					<h4><?php echo esc_html($settings['title']) ?></h4>

					<p><?php echo esc_html($settings['content']) ?></p>

				</div>

			</div>
		</div>
		<?php
	}
}
