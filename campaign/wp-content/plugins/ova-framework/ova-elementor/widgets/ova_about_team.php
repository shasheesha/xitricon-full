<?php
namespace ova_framework\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Core\Schemes\Typography;
use Elementor\Group_Control_Typography;
use Elementor\Core\Schemes\Color;
use Elementor\Group_Control_Border;
use Elementor\Repeater;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


class ova_about_team extends Widget_Base {

	public function get_name() {
		return 'ova_about_team';
	}

	public function get_title() {
		return __( 'About Team', 'ova-framework' );
	}

	public function get_icon() {
		return 'eicon-person';
	}

	public function get_categories() {
		return [ 'ovatheme' ];
	}

	public function get_script_depends() {
		return [ 'script-elementor' ];
	}

	protected function register_controls() {

		$this->start_controls_section(
			'section_content',
			[
				'label' => __( 'Content', 'ova-framework' ),
			]
		);

		$this->add_control(
			'image',
			[
				'label' => __( 'Image ', 'ova-framework' ),
				'type' 	=> \Elementor\Controls_Manager::MEDIA,
			]
		);

		$this->add_control(
			'title',
			[
				'label' 	=> __( 'Title', 'ova-framework' ),
				'type' 		=> \Elementor\Controls_Manager::TEXT,
				'default' 	=> __('ALEXANDRA SMITHS', 'ova-framework'),
			]
		);

		$this->add_control(
			'content',
			[
				'label' 	=> __( 'Description', 'ova-framework' ),
				'type' 		=> \Elementor\Controls_Manager::TEXTAREA,
				'default' 	=> __('Graduating with a degree in Spanish, English and French, Alexandra has always loved writing.', 'ova-framework'),
				'row' 		=> 5,
			]
		);

		$this->add_control(
			'line_color',
			[
				'label' 	=> __( 'Line Color', 'ova-framework' ),
				'type' 		=> Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .team-info::after' => 'background : {{VALUE}}!important;',
				],
				'default' 	=> '#ed4e38'
			]
		);

		$this->add_control(
			'job',
			[
				'label' 	=> __( 'Job ', 'ova-framework' ),
				'type' 		=> \Elementor\Controls_Manager::TEXT,
				'default' 	=> __('SEO Specialist', 'ova-framework'),
			]
		);

		$this->add_control(
			'color_icon',
			[
				'label' 	=> __( 'Color Icon', 'ova-framework' ),
				'type' 		=> Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .ova-about-team .team-item .team-icon a i' => 'color : {{VALUE}};',
				],
			]
		);

		$repeater = new \Elementor\Repeater();

		$repeater->add_control(
			'icon',
			[
				'label' 	=> __( 'Icon ', 'ova-framework' ),
				'type' 		=> \Elementor\Controls_Manager::TEXT,
				'default' 	=> __( 'fa fa-twitter', 'ova-framework' ),
			]
		);

		$repeater->add_control(
			'link',
			[
				'label' 		=> __( 'Link', 'ova-framework' ),
				'type' 			=> \Elementor\Controls_Manager::URL,
				'placeholder' 	=> __( 'https://your-link.com', 'ova-framework' ),
				'show_external' => false,
				'default' => [
					'url' 		  => '#',
					'is_external' => false,
					'nofollow' 	  => false,
				],
			]
		);

		$repeater->add_control(
			'background_color_icon',
			[
				'label' 	=> __( 'Type Background Color Icon', 'ova-framework' ),
				'type' 		=> Controls_Manager::SELECT,
				'default' 	=> 'twitter',
				'options' 	=> [
					'twitter' 	=> __('Twitter', 'ova-framework'),
					'pinterest' => __('Pinterest', 'ova-framework'),
					'facebook' 	=> __('Facebook', 'ova-framework'),
					'dribble' 	=> __('Dribble', 'ova-framework'),
				]
			]
		);

		$this->add_control(
			'tabs',
			[
				'label' 	  => __( 'Items', 'ova-framework' ),
				'type' 		  => Controls_Manager::REPEATER,
				'fields' 	  => $repeater->get_controls(),
				'title_field' => '{{{ icon }}}',
				'default' => [
					[
						'icon' => 'fa fa-twitter',
						'background_color_icon' => 'twitter',
					],
					[
						'icon' => 'fa fa-pinterest',
						'background_color_icon' => 'pinterest',
					],
					[
						'icon' => 'fa fa-facebook',
						'background_color_icon' => 'facebook',
					],
					[
						'icon' => 'fa fa-dribbble',
						'background_color_icon' => 'dribble',
					],
				],
			]
		);

		$this->end_controls_section();
		// end tab section_content

		// section style content
		$this->start_controls_section(
			'section_content_style',
			[
				'label' => __( 'Content', 'ova-framework' ),
				'tab' 	=> Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_control(
				'bg_content',
				[
					'label' => __( 'Background', 'ova-framework' ),
					'type' 	=> Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .ova-about-team .team-item' => 'background-color: {{VALUE}};',
					],
				]
			);

			$this->add_group_control(
	            Group_Control_Border::get_type(), [
	                'name' 		=> 'content_border',
	                'selector' 	=> '{{WRAPPER}} .ova-about-team .team-item',
	                'separator' => 'before',
	            ]
	        );

	        $this->add_control(
	            'content_border_radius',
	            [
	                'label' => __( 'Border Radius', 'ova_framework' ),
	                'type' 	=> Controls_Manager::DIMENSIONS,
	                'size_units' => [ 'px', '%' ],
	                'selectors'  => [
	                    '{{WRAPPER}} .ova-about-team .team-item' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
	                ],
	            ]
	        );

			$this->add_responsive_control(
				'padding_content',
				[
					'label' 	 => __( 'Padding', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-about-team .team-item' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;',
					],
				]
			);

			$this->add_responsive_control(
				'margin_content',
				[
					'label' 	 => __( 'Margin', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-about-team .team-item' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;',
					],
				]
			);

			$this->add_responsive_control(
	            'align_content',
	            [
	                'label' => __( 'Alignment', 'ova_framework' ),
	                'type' 	=> Controls_Manager::CHOOSE,
	                'options' => [
	                    'left' => [
	                        'title' => __( 'Left', 'ova_framework' ),
	                        'icon' 	=> 'eicon-text-align-left',
	                    ],
	                    'center' => [
	                        'title' => __( 'Center', 'ova_framework' ),
	                        'icon' 	=> 'eicon-text-align-center',
	                    ],
	                    'right' => [
	                        'title' => __( 'Right', 'ova_framework' ),
	                        'icon' 	=> 'eicon-text-align-right',
	                    ],
	                    'justify' => [
	                        'title' => __( 'Justified', 'ova_framework' ),
	                        'icon' => 'eicon-text-align-justify',
                    	],
	                ],
	                'selectors' => [
	                    '{{WRAPPER}} .ova-about-team .team-item' => 'text-align: {{VALUE}}',
	                ],
	            ]
	        );

		$this->end_controls_section();
		// end section style content

		// section style image
		$this->start_controls_section(
			'section_img_style',
			[
				'label' => __( 'Image', 'ova-framework' ),
				'tab' 	=> Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_control(
				'bg_img',
				[
					'label' => __( 'Background', 'ova-framework' ),
					'type' 	=> Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .ova-about-team .team-item img.team-img' => 'background-color: {{VALUE}};',
					],
				]
			);

			$this->add_group_control(
	            Group_Control_Border::get_type(), [
	                'name' 		=> 'img_border',
	                'selector' 	=> '{{WRAPPER}} .ova-about-team .team-item img.team-img',
	                'separator' => 'before',
	            ]
	        );

	        $this->add_control(
	            'img_border_radius',
	            [
	                'label' => __( 'Border Radius', 'ova_framework' ),
	                'type' 	=> Controls_Manager::DIMENSIONS,
	                'size_units' => [ 'px', '%' ],
	                'selectors'  => [
	                    '{{WRAPPER}} .ova-about-team .team-item img.team-img' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
	                ],
	            ]
	        );

			$this->add_responsive_control(
				'padding_img',
				[
					'label' 	 => __( 'Padding', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-about-team .team-item img.team-img' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;',
					],
				]
			);

			$this->add_responsive_control(
				'margin_img',
				[
					'label' 	 => __( 'Margin', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-about-team .team-item img.team-img' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;',
					],
				]
			);

		$this->end_controls_section();
		// end section style content

		// section style title
		$this->start_controls_section(
			'section_title_style',
			[
				'label' => __( 'Title', 'ova-framework' ),
				'tab' 	=> Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'title_typography',
					'selector' => '{{WRAPPER}} .ova-about-team .team-item h3',
				]
			);

			$this->add_control(
				'color_title',
				[
					'label' => __( 'Color', 'ova-framework' ),
					'type' 	=> Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .ova-about-team .team-item h3' => 'color : {{VALUE}};',
					],
				]
			);

			$this->add_responsive_control(
				'padding_title',
				[
					'label' 	 => __( 'Padding', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-about-team .team-item h3' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->add_responsive_control(
				'margin_title',
				[
					'label' 	 => __( 'Margin', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-about-team .team-item h3' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;',
					],
				]
			);

		$this->end_controls_section();
		// end section style title

		// section style job
		$this->start_controls_section(
			'section_job_style',
			[
				'label' => __( 'Job', 'ova-framework' ),
				'tab' 	=> Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'job_typography',
					'selector' => '{{WRAPPER}} .ova-about-team .team-item .team-info p',
				]
			);

			$this->add_control(
				'color_job',
				[
					'label' => __( 'Color', 'ova-framework' ),
					'type' 	=> Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .ova-about-team .team-item .team-info p' => 'color : {{VALUE}};',
					],
				]
			);

			$this->add_responsive_control(
				'padding_job',
				[
					'label' 	 => __( 'Padding', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-about-team .team-item .team-info p' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->add_responsive_control(
				'margin_job',
				[
					'label' 	 => __( 'Margin', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-about-team .team-item .team-info p' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

		$this->end_controls_section();
		// end section style job

		// section style description
		$this->start_controls_section(
			'section_description_style',
			[
				'label' => __( 'Description', 'ova-framework' ),
				'tab' 	=> Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'description_typography',
					'selector' => '{{WRAPPER}} .ova-about-team .team-item p',
				]
			);

			$this->add_control(
				'color_description',
				[
					'label' => __( 'Color', 'ova-framework' ),
					'type' 	=> Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .ova-about-team .team-item p' => 'color : {{VALUE}};',
					],
				]
			);

			$this->add_responsive_control(
				'padding_description',
				[
					'label' 	 => __( 'Padding', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-about-team .team-item p' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->add_responsive_control(
				'margin_description',
				[
					'label' 	 => __( 'Margin', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-about-team .team-item p' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

		$this->end_controls_section();
		// end section style description

	}

	protected function render() {
		$settings = $this->get_settings_for_display();
		$tabs = $settings['tabs'];
		?>
		<div class="ova-about-team">

			<div class="team-item">

				<img src="<?php echo esc_attr($settings['image']['url']) ?>" class="team-img" alt="<?php echo esc_attr($settings['title']) ?>">

				<h3><?php echo esc_html($settings['title']) ?></h3>

				<div class="team-info"><p><?php echo esc_attr($settings['job']) ?></p></div>

				<p><?php echo esc_attr($settings['content']) ?></p>

				<ul class="team-icon">
					<?php if (!empty($tabs)) : foreach ($tabs as $item) : ?>
						<li><a target="_blank" class="<?php echo $item['background_color_icon'] ?> twitter" href="<?php echo esc_attr($item['link']['url']) ?>" ><i class="<?php echo esc_attr($item['icon']) ?>"></i></a></li>
					<?php endforeach; endif ?>

				</ul>

			</div>

			
		</div>
		<?php
	}
}
