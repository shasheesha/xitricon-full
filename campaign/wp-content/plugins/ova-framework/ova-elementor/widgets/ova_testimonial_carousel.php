<?php
namespace ova_framework\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Core\Schemes\Typography;
use Elementor\Group_Control_Typography;
use Elementor\Core\Schemes\Color;
use Elementor\Group_Control_Border;
use Elementor\Repeater;
use Elementor\Group_Control_Text_Shadow;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Elementor Hello World
 *
 * Elementor widget for hello world.
 *
 * @since 1.0.0
 */
class ova_testimonial_carousel extends Widget_Base {

	public function get_name() {
		return 'ova_testimonial_carousel';
	}

	public function get_title() {
		return __( 'Testimonial Carousel', 'ova-framework' );
	}

	public function get_icon() {
		return 'eicon-testimonial';
	}

	public function get_categories() {
		return [ 'ovatheme' ];
	}


	public function get_script_depends() {
		wp_enqueue_style( 'magnific-popup', OVA_PLUGIN_URI.'assets/libs/magnific-popup/style-magnific-popup.css' );
		wp_enqueue_style( 'owl-carousel', OVA_PLUGIN_URI.'assets/libs/owl-carousel/assets/owl.carousel.min.css' );
		wp_enqueue_script( 'owl-carousel', OVA_PLUGIN_URI.'assets/libs/owl-carousel/owl.carousel.min.js', array('jquery'), false, true );
		return [ 'script-elementor' ];
	}

	protected function register_controls() {

		/*************************************************************************************************
		SECTION VERSION
		*************************************************************************************************/

		$this->start_controls_section(
			'section_content',
			[
				'label' => __( 'Content', 'ova-framework' ),
			]
		);

		$repeater = new \Elementor\Repeater();

		$repeater->add_control(
			'image',
			[
				'label' => __( 'Image ', 'ova-framework' ),
				'type' 	=> \Elementor\Controls_Manager::MEDIA,
			]
		);

		$repeater->add_control(
			'content',
			[
				'label' 	=> __( 'Content ', 'ova-framework' ),
				'type' 		=> \Elementor\Controls_Manager::TEXTAREA,
				'default' 	=> __('The attention of a traveller, should be particularly turned, in the first place, to the various works of Nature, to mark the distinctions of the climates he may explore, and to offer such useful observations on the different productions as may occur.', 'ova-framework'),
				'row' 		=> 5,
			]
		);

		$repeater->add_control(
			'name',
			[
				'label' 	=> __( 'Name ', 'ova-framework' ),
				'type' 		=> \Elementor\Controls_Manager::TEXT,
				'default' 	=> __( 'Alexandra Smith', 'ova-framework' ),
			]
		);

		$repeater->add_control(
			'job',
			[
				'label' 	=> __( 'Job ', 'ova-framework' ),
				'type' 		=> \Elementor\Controls_Manager::TEXT,
				'default' 	=> __( ' - App Magazine Editor', 'ova-framework' ),
			]
		);

		$this->add_control(
			'tabs',
			[
				'label' 		=> __( 'Items', 'ova-framework' ),
				'type' 			=> Controls_Manager::REPEATER,
				'fields' 		=> $repeater->get_controls(),
				'title_field' 	=> '{{{ name }}}',
			]
		);

		$this->end_controls_section();

		############################ END SECTION VERSIONTION TESTIMONIAL 1  ##############################

		/*****************************************************************
		START SECTION ADDITIONAL VERSIONT
		******************************************************************/

		$this->start_controls_section(
			'section_additional_options',
			[
				'label' => __( 'Additional Options', 'ova-framework' ),
			]
		);


		$this->add_control(
			'margin_items',
			[
				'label' 	=> __( 'Margin Right Items', 'ova-framework' ),
				'type' 		=> Controls_Manager::NUMBER,
				'default' 	=> 30,
			]

		);

		$this->add_control(
			'slides_to_scroll',
			[
				'label' 		=> __( 'Slides to Scroll', 'ova-framework' ),
				'type' 			=> Controls_Manager::NUMBER,
				'description' 	=> __( 'Set how many slides are scrolled per swipe.', 'ova-framework' ),
				'default' 		=> '1',
			]
		);

		$this->add_control(
			'pause_on_hover',
			[
				'label'   => __( 'Pause on Hover', 'ova-framework' ),
				'type' 	  => Controls_Manager::SWITCHER,
				'default' => 'yes',
				'options' => [
					'yes' 	=> __( 'Yes', 'ova-framework' ),
					'no' 	=> __( 'No', 'ova-framework' ),
				],
				'frontend_available' => true,
			]
		);

		$this->add_control(
			'infinite',
			[
				'label'   => __( 'Infinite Loop', 'ova-framework' ),
				'type' 	  => Controls_Manager::SWITCHER,
				'default' => 'yes',
				'options' => [
					'yes' 	=> __( 'Yes', 'ova-framework' ),
					'no' 	=> __( 'No', 'ova-framework' ),
				],
				'frontend_available' => true,
			]
		);

		$this->add_control(
			'autoplay',
			[
				'label'   => __( 'Autoplay', 'ova-framework' ),
				'type' 	  => Controls_Manager::SWITCHER,
				'default' => 'yes',
				'options' => [
					'yes' 	=> __( 'Yes', 'ova-framework' ),
					'no' 	=> __( 'No', 'ova-framework' ),
				],
				'frontend_available' => true,
			]
		);

		$this->add_control(
			'autoplay_speed',
			[
				'label' 	=> __( 'Autoplay Speed', 'ova-framework' ),
				'type' 		=> Controls_Manager::NUMBER,
				'default' 	=> 3000,
				'step' 		=> 500,
				'condition' => [
					'autoplay' => 'yes',
				],
				'frontend_available' => true,
				'condition' => [
					'autoplay' => 'yes',
				],
			]
		);

		$this->add_control(
			'smartspeed',
			[
				'label'   => __( 'Smart Speed', 'ova-framework' ),
				'type'    => Controls_Manager::NUMBER,
				'default' => 500,

			]
		);

		$this->end_controls_section();
		#########################    END SECTION ADDITIONAL  VERSION 1  #########################

		// section style content
		$this->start_controls_section(
			'section_content_style',
			[
				'label' => __( 'Content', 'ova-framework' ),
				'tab' 	=> Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'content_typography',
					'selector' => '{{WRAPPER}} .ova-testimonial-carousel .wp-ova-testimonial .owl-carousel .item .testim-inner p',
				]
			);

			$this->add_control(
				'color_content',
				[
					'label' => __( 'Color', 'ova-framework' ),
					'type' 	=> Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .ova-testimonial-carousel .wp-ova-testimonial .owl-carousel .item .testim-inner p' => 'color : {{VALUE}};',
					],
				]
			);

			$this->add_responsive_control(
				'padding_content',
				[
					'label' 	 => __( 'Padding', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-testimonial-carousel .wp-ova-testimonial .owl-carousel .item .testim-inner p' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->add_responsive_control(
				'margin_content',
				[
					'label' 	 => __( 'Margin', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-testimonial-carousel .wp-ova-testimonial .owl-carousel .item .testim-inner p' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->add_responsive_control(
	            'align_content',
	            [
	                'label' => __( 'Alignment', 'ova_framework' ),
	                'type' 	=> Controls_Manager::CHOOSE,
	                'options' => [
	                    'left' => [
	                        'title' => __( 'Left', 'ova_framework' ),
	                        'icon' 	=> 'eicon-text-align-left',
	                    ],
	                    'center' => [
	                        'title' => __( 'Center', 'ova_framework' ),
	                        'icon' 	=> 'eicon-text-align-center',
	                    ],
	                    'right' => [
	                        'title' => __( 'Right', 'ova_framework' ),
	                        'icon' 	=> 'eicon-text-align-right',
	                    ],
	                    'justify' => [
	                        'title' => __( 'Justified', 'ova_framework' ),
	                        'icon' => 'eicon-text-align-justify',
                    	],
	                ],
	                'selectors' => [
	                    '{{WRAPPER}} .ova-testimonial-carousel .wp-ova-testimonial .owl-carousel .item .testim-inner p' => 'text-align: {{VALUE}}',
	                ],
	            ]
	        );

	        $this->add_group_control(
				Group_Control_Text_Shadow::get_type(),
				[
					'name' => 'title_shadow',
					'selector' => '{{WRAPPER}} .ova-testimonial-carousel .wp-ova-testimonial .owl-carousel .item .testim-inner p',
				]
			);

		$this->end_controls_section();
		// end section style content

		// section style name
		$this->start_controls_section(
			'section_name_style',
			[
				'label' => __( 'Name', 'ova-framework' ),
				'tab' 	=> Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'name_typography',
					'selector' => '{{WRAPPER}} .ova-testimonial-carousel .wp-ova-testimonial .owl-carousel .item .testim-inner h6',
				]
			);

			$this->add_control(
				'color_name',
				[
					'label' => __( 'Color', 'ova-framework' ),
					'type' 	=> Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .ova-testimonial-carousel .wp-ova-testimonial .owl-carousel .item .testim-inner h6' => 'color : {{VALUE}};',
					],
				]
			);

			$this->add_responsive_control(
				'padding_name',
				[
					'label' 	 => __( 'Padding', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-testimonial-carousel .wp-ova-testimonial .owl-carousel .item .testim-inner h6' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->add_responsive_control(
				'margin_name',
				[
					'label' 	 => __( 'Margin', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-testimonial-carousel .wp-ova-testimonial .owl-carousel .item .testim-inner h6' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

		$this->end_controls_section();
		// end section style name

		// section style job
		$this->start_controls_section(
			'section_job_style',
			[
				'label' => __( 'Job', 'ova-framework' ),
				'tab' 	=> Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'job_typography',
					'selector' => '{{WRAPPER}} .ova-testimonial-carousel .wp-ova-testimonial .owl-carousel .item .testim-inner h6 .job-text',
				]
			);

			$this->add_control(
				'color_job',
				[
					'label' => __( 'Color', 'ova-framework' ),
					'type' 	=> Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .ova-testimonial-carousel .wp-ova-testimonial .owl-carousel .item .testim-inner h6 .job-text' => 'color : {{VALUE}};',
					],
				]
			);

			$this->add_responsive_control(
				'padding_job',
				[
					'label' 	 => __( 'Padding', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-testimonial-carousel .wp-ova-testimonial .owl-carousel .item .testim-inner h6 .job-text' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->add_responsive_control(
				'margin_job',
				[
					'label' 	 => __( 'Margin', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-testimonial-carousel .wp-ova-testimonial .owl-carousel .item .testim-inner h6 .job-text' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

		$this->end_controls_section();
		// end section style job

		// section style dots
		$this->start_controls_section(
			'section_dots_style',
			[
				'label' => __( 'Dots', 'ova-framework' ),
				'tab' 	=> Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_control(
				'bg_dots',
				[
					'label' => __( 'Background', 'ova-framework' ),
					'type' 	=> Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .ova-testimonial-carousel .wp-ova-testimonial .owl-carousel .owl-dots .owl-dot' => 'background-color : {{VALUE}};',
					],
				]
			);

			$this->add_control(
				'bg_dots_active',
				[
					'label' => __( 'Background Active', 'ova-framework' ),
					'type' 	=> Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .ova-testimonial-carousel .wp-ova-testimonial .owl-carousel .owl-dots .owl-dot.active' => 'background-color : {{VALUE}}; border-color: {{VALUE}};',
					],
				]
			);

		$this->end_controls_section();
		// end section style dots

	}

	/**
	 * Render the widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		$tabs = $settings['tabs'];

		$data_options['slideBy'] 			= $settings['slides_to_scroll'];
		$data_options['margin'] 			= $settings['margin_items'];
		$data_options['autoplayHoverPause'] = $settings['pause_on_hover'] === 'yes' ? true : false;
		$data_options['loop'] 			 	= $settings['infinite'] === 'yes' ? true : false;
		$data_options['autoplay'] 			= $settings['autoplay'] === 'yes' ? true : false;
		$data_options['autoplayTimeout']	= $data_options['autoplay'] ? $settings['autoplay_speed'] : 3000;
		$data_options['smartSpeed']			= $settings['smartspeed'];


		?>

		<div class="ova-testimonial-carousel">
			<div class="wp-ova-testimonial">
				<div class="list-testimonial slide-testimonial owl-carousel" data-options="<?php echo esc_attr(json_encode($data_options)) ?>">
					<?php if (!empty($tabs)) : foreach ($tabs as $item) : ?>
						<div class="item">
							<div class="testim-inner">

								<img src="<?php echo esc_attr($item['image']['url']) ?>" alt="<?php echo esc_attr($item['content']) ?>" class="testim-img">

								<p><?php echo esc_html($item['content']) ?></p>

								<h6><?php echo esc_html($item['name']) ?><span class="job-text"><?php echo esc_html($item['job']) ?></span></h6>

							</div>
						</div>
					<?php endforeach; endif; ?>
				</div>
			</div>
		</div>
		<!-- end .ova-project-carousel -->

		<?php
	}
}

