<?php
namespace ova_framework\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Core\Schemes\Typography;
use Elementor\Group_Control_Typography;
use Elementor\Core\Schemes\Color;
use Elementor\Group_Control_Border;
use Elementor\Repeater;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


class ova_feauture_list_howdy extends Widget_Base {

	public function get_name() {
		return 'ova_feauture_list_howdy';
	}

	public function get_title() {
		return __( 'Feauture List', 'ova-framework' );
	}

	public function get_icon() {
		return 'eicon-info-box';
	}

	public function get_categories() {
		return [ 'ovatheme' ];
	}

	public function get_script_depends() {
		return [ 'script-elementor' ];
	}

	protected function register_controls() {

		$animation_array =array(
			'bounce'  => 'bounce',
			'flash'  => 'flash',
			'pulse'  => 'pulse',
			'rubberBand'  => 'rubberBand',
			'shake'  => 'shake',
			'swing'  => 'swing',
			'tada'  => 'tada',
			'wobble'  => 'wobble',
			'jello'  => 'jello',
			'bounceIn'  => 'bounceIn',
			'bounceInDown'  => 'bounceInDown',
			'bounceInLeft'  => 'bounceInLeft',
			'bounceInRight'  => 'bounceInRight',
			'bounceInUp'  => 'bounceInUp',
			'bounceOut'  => 'bounceOut',
			'bounceOutDown'  => 'bounceOutDown',
			'bounceOutLeft'  => 'bounceOutLeft',
			'bounceOutRight'  => 'bounceOutRight',
			'bounceOutUp'  => 'bounceOutUp',
			'fadeIn'  => 'fadeIn',
			'fadeInDown'  => 'fadeInDown',
			'fadeInDownBig'  => 'fadeInDownBig',
			'fadeInLeft'  => 'fadeInLeft',
			'fadeInLeftBig'  => 'fadeInLeftBig',
			'fadeInRight'  => 'fadeInRight',
			'fadeInRightBig'  => 'fadeInRightBig',
			'fadeInUp'  => 'fadeInUp',
			'fadeInUpBig'  => 'fadeInUpBig',
			'fadeOut'  => 'fadeOut',
			'fadeOutDown'  => 'fadeOutDown',
			'fadeOutDownBig'  => 'fadeOutDownBig',
			'fadeOutLeft'  => 'fadeOutLeft',
			'fadeOutLeftBig'  => 'fadeOutLeftBig',
			'fadeOutRight'  => 'fadeOutRight',
			'fadeOutRightBig'  => 'fadeOutRightBig',
			'fadeOutUp'  => 'fadeOutUp',
			'fadeOutUpBig'  => 'fadeOutUpBig',
			'flip'  => 'flip',
			'flipInX'  => 'flipInX',
			'flipInY'  => 'flipInY',
			'flipOutX'  => 'flipOutX',
			'flipOutY'  => 'flipOutY',
			'lightSpeedIn'  => 'lightSpeedIn',
			'lightSpeedOut'  => 'lightSpeedOut',
			'rotateIn'  => 'rotateIn',
			'rotateInDownLeft'  => 'rotateInDownLeft',
			'rotateInDownRight'  => 'rotateInDownRight',
			'rotateInUpLeft'  => 'rotateInUpLeft',
			'rotateInUpRight'  => 'rotateInUpRight',
			'rotateOut'  => 'rotateOut',
			'rotateOutDownLeft'  => 'rotateOutDownLeft',
			'rotateOutDownRight'  => 'rotateOutDownRight',
			'rotateOutUpLeft'  => 'rotateOutUpLeft',
			'rotateOutUpRight'  => 'rotateOutUpRight',
			'slideInUp'  => 'slideInUp',
			'slideInDown'  => 'slideInDown',
			'slideInLeft'  => 'slideInLeft',
			'slideInRight'  => 'slideInRight',
			'slideOutUp'  => 'slideOutUp',
			'slideOutDown'  => 'slideOutDown',
			'slideOutLeft'  => 'slideOutLeft',
			'slideOutRight'  => 'slideOutRight',
			'zoomIn'  => 'zoomIn',
			'zoomInDown'  => 'zoomInDown',
			'zoomInLeft'  => 'zoomInLeft',
			'zoomInRight'  => 'zoomInRight',
			'zoomInUp'  => 'zoomInUp',
			'zoomOut'  => 'zoomOut',
			'zoomOutDown'  => 'zoomOutDown',
			'zoomOutLeft'  => 'zoomOutLeft',
			'zoomOutRight'  => 'zoomOutRight',
			'zoomOutUp'  => 'zoomOutUp',
			'hinge'  => 'hinge',
			'jackInTheBox'  => 'jackInTheBox',
			'rollIn'  => 'rollIn',
			'rollOut'  => 'rollOut'


		);

		$this->start_controls_section(
			'section_content',
			[
				'label' => __( 'Content', 'ova-framework' ),
			]
		);

		$repeater = new \Elementor\Repeater();

		$repeater->add_control(
			'icon',
			[
				'label' 	=> __( 'Icon ', 'ova-framework' ),
				'type' 		=> \Elementor\Controls_Manager::TEXT,
				'default' 	=> __( 'pe-7s-plane hi-icon blue', 'ova-framework' ),
			]
		);

		$repeater->add_control(
			'color_icon',
			[
				'label' => __( 'Color Icon', 'ova-framework' ),
				'type' 	=> Controls_Manager::COLOR,
				
			]
		);

		$repeater->add_control(
			'content',
			[
				'label'   => __( 'Content ', 'ova-framework' ),
				'type' 	  => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Tendis tempor ante acu ipsum finibus, et atimus etims verdaderais quis ratione lorem nets et sequi tempor.', 'ova-framework' ),
				'row' 	  => 2,
			]
		);


		$this->add_control(
			'tabs',
			[
				'label' 		=> __( 'Items', 'ova-framework' ),
				'type' 			=> Controls_Manager::REPEATER,
				'fields' 		=> $repeater->get_controls(),
				'title_field' 	=> '{{{ icon }}}',
			]
		);

		$this->add_control(
			'heading_animate_item',
			[
				'label' 	=> __( 'Animate Item', 'ova-framework' ),
				'type' 		=> \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);


		$this->add_control(
			'show_animation_item',
			[
				'label' 	=> __( 'Show Animate', 'ova-framework' ),
				'type' 		=> Controls_Manager::SWITCHER,
				'default' 	=> '',
			]
		);

		$this->add_control(
			'animate_style_item',
			[
				'label' 	=> __( 'Type Animate', 'ova-framework' ),
				'type' 		=> Controls_Manager::SELECT,
				'options' 	=> $animation_array,
				'default' 	=> 'fadeInUp',
				'condition' => [
					'show_animation_item' => 'yes',
				],
			]
		);

		$this->add_control(
			'animation_dur_item',
			[
				'label' 	=> __( 'Time Animation', 'ova-framework' ),
				'type' 		=> \Elementor\Controls_Manager::NUMBER,
				'default' 	=> 1000,
				'condition' => [
					'show_animation_item' => 'yes',
				],
			]
		);

		$this->add_control(
			'animation_dur_delay_item_to_item',
			[
				'label' 	=> __( 'Time Delay Animation Item to Item', 'ova-framework' ),
				'type' 		=> \Elementor\Controls_Manager::NUMBER,
				'default' 	=> 500,
				'condition' => [
					'show_animation_item' => 'yes',
				],
			]
		);

		$this->end_controls_section();

		// end tab section_content

		// section style content
		$this->start_controls_section(
			'section_content_style',
			[
				'label' => __( 'Content', 'ova-framework' ),
				'tab' 	=> Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'content_typography',
					'selector' => '{{WRAPPER}} .ova-feauture-list-howdy .features-list-hero li',
				]
			);

			$this->add_control(
				'color_content',
				[
					'label' => __( 'Color', 'ova-framework' ),
					'type' 	=> Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .ova-feauture-list-howdy .features-list-hero li' => 'color : {{VALUE}};',
					],
				]
			);

			$this->add_responsive_control(
				'padding_content',
				[
					'label' 	 => __( 'Padding', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-feauture-list-howdy .features-list-hero li' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->add_responsive_control(
				'margin_content',
				[
					'label' 	 => __( 'Margin', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-feauture-list-howdy .features-list-hero li' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

		$this->end_controls_section();
		// end section style content

		// section style icon
		$this->start_controls_section(
			'section_icon_style',
			[
				'label' => __( 'Icon', 'ova-framework' ),
				'tab' 	=> Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'icon_typography',
					'selector' => '{{WRAPPER}} .ova-feauture-list-howdy .features-list-hero li i',
				]
			);

			$this->add_control(
				'color_icon',
				[
					'label' => __( 'Color', 'ova-framework' ),
					'type' 	=> Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .ova-feauture-list-howdy .features-list-hero li i' => 'color : {{VALUE}};',
					],
				]
			);

			$this->add_responsive_control(
				'padding_icon',
				[
					'label' 	 => __( 'Padding', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-feauture-list-howdy .features-list-hero li i' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->add_responsive_control(
				'margin_icon',
				[
					'label' 	 => __( 'Margin', 'ova-framework' ),
					'type' 		 => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-feauture-list-howdy .features-list-hero li i' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

		$this->end_controls_section();
		// end section style icon

	}

	protected function render() {

		$settings = $this->get_settings_for_display();
		$tabs 	  = $settings['tabs'];
		$animate_item 	= $class_animate_item = '';
		$time_dur_item 	= $time_dur_delay_item = $time_item = 0;

		if ($settings['show_animation_item'] === 'yes') {
			$data_animate_item  = "data-animation='" . $settings['animate_style_item'] . "'";
			$class_animate_item = "animated elementor-slide-item " . $settings['animate_style_item'];

			$time_dur_item 	= $settings['animation_dur_item'];
			$time_item 		= $settings['animation_dur_delay_item_to_item'];
		}


		
		?>
		<div class="ova-feauture-list-howdy">
			<ul class="features-list-hero">
				<?php 

				if (!empty($tabs)) : foreach ($tabs as $item) : 

				?>
					<li <?php $data_animate_item ?> class="<?php echo $class_animate_item ?>"  data-animation_dur="<?php echo $time_dur_item ?>" style="animation-duration: <?php echo $time_dur_item ?>ms; opacity: 1;" >
						<?php if ($item['icon'] != '') : ?>
							<i style="color: <?php echo esc_attr( $item['color_icon'] ) ?>" class="<?php echo esc_attr($item['icon']) ?>"></i> 
						<?php endif ?>
						<?php if ($item['content'] != '') : ?>
							<?php echo esc_html($item['content']) ?>
						<?php endif ?>
					</li>
				<?php 
					$time_dur_item += $time_item;
					endforeach; endif 
				?>
			</ul>
		</div>
		<?php
	}
}
