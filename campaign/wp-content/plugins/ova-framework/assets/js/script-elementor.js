(function($){
	"use strict";
	

	$(window).on('elementor/frontend/init', function () {

		
		/* Menu Shrink */
		elementorFrontend.hooks.addAction('frontend/element_ready/ova_menu.default', function(){

			$( '.ova_menu_clasic .ova_openNav' ).on( 'click', function(){
				$( this ).closest('.ova_wrap_nav').find( '.ova_nav' ).removeClass( 'hide' );
				$( this ).closest('.ova_wrap_nav').find( '.ova_nav' ).addClass( 'show' );
				$( '.ova_menu_clasic  .ova_closeCanvas' ).css( 'width', '100%' );

				
				$( 'body' ).css( 'background-color', 'rgba(0,0,0,0.4)' );
				
			});

			$( '.ova_menu_clasic  .ova_closeNav' ).on( 'click', function(){
				$( this ).closest('.ova_wrap_nav').find( '.ova_nav' ).removeClass( 'show' );
				$( this ).closest('.ova_wrap_nav').find( '.ova_nav' ).addClass( 'hide' );
				$( '.ova_closeCanvas' ).css( 'width', '0' );


				
				$( 'body' ).css( 'background-color', 'transparent' );
				
			});

			// Display in mobile
			$( '.ova_menu_clasic li.menu-item button.dropdown-toggle').off('click').on( 'click', function() {
				$(this).parent().toggleClass('active_sub');
			});

			$(window).scroll(function () {
				if( $('.ovamenu_shrink').length > 0 ){

					var header = $('.ovamenu_shrink');
					var scroll = $(window).scrollTop();

					if (scroll >= 200) {
						header.addClass('ready_active');
					}else{
						header.removeClass('ready_active');
					}
					if (scroll >= 250) {
						header.addClass('active_fixed');
					} else{
						header.removeClass('active_fixed');
					}

				}

			});
		});


		/* Slide Project */
		elementorFrontend.hooks.addAction('frontend/element_ready/ova_project_carousel.default', function(){
			$(".slide-project").each(function(){
				var owlsl = $(this) ;
				var owlsl_df = {
					margin: 0, 
					responsive: false, 
					smartSpeed:500,
					autoplay:false,
					autoplayTimeout: 6000,
					items:1,
					loop:true, 
					nav: true, 
					dots: true,
					center:false,
					autoWidth:false,
					thumbs:false, 
					autoplayHoverPause: true,
					slideBy: 1,
					prev:'<i class="arrow_carrot-left"></i>',
					next:'<i class="arrow_carrot-right"></i>',
				};
				var owlsl_ops = owlsl.data('options') ? owlsl.data('options') : {};
				owlsl_ops = $.extend({}, owlsl_df, owlsl_ops);
				owlsl.owlCarousel({
					autoWidth: owlsl_ops.autoWidth,
					margin: owlsl_ops.margin,
					items: owlsl_ops.items,
					loop: owlsl_ops.loop,
					autoplay: owlsl_ops.autoplay,
					autoplayTimeout: owlsl_ops.autoplayTimeout,
					center: owlsl_ops.center,
					nav: owlsl_ops.nav,
					dots: owlsl_ops.dots,
					thumbs: owlsl_ops.thumbs,
					autoplayHoverPause: owlsl_ops.autoplayHoverPause,
					slideBy: owlsl_ops.slideBy,
					smartSpeed: owlsl_ops.smartSpeed,
					navText:[owlsl_ops.prev,owlsl_ops.next],
					responsive : {
					    // breakpoint from 480 up
					    0 : {
					    	items : 1,
					    },
					    767 : {
					    	items : 2,
					    },
					    // breakpoint from 1024 up
					    1024 : {
					    	items : 3,
					    }
					}
				});

			});

			$('a.popup1').magnificPopup({
				type: 'image',
				gallery: {
					enabled:true
				}
			}); 


		});
		// end Slide Project

		/* Slide testimonial */
		elementorFrontend.hooks.addAction('frontend/element_ready/ova_testimonial_carousel.default', function(){
			$(".slide-testimonial").each(function(){
				var owlsl = $(this) ;
				var owlsl_df = {
					margin: 0, 
					responsive: false, 
					smartSpeed:500,
					autoplay:false,
					autoplayTimeout: 6000,
					items:1,
					loop:true, 
					nav: true, 
					dots: true,
					center:false,
					autoWidth:false,
					thumbs:false, 
					autoplayHoverPause: true,
					slideBy: 1,
					prev:'<i class="arrow_carrot-left"></i>',
					next:'<i class="arrow_carrot-right"></i>',
				};
				var owlsl_ops = owlsl.data('options') ? owlsl.data('options') : {};
				owlsl_ops = $.extend({}, owlsl_df, owlsl_ops);
				owlsl.owlCarousel({
					autoWidth: owlsl_ops.autoWidth,
					margin: owlsl_ops.margin,
					items: owlsl_ops.items,
					loop: owlsl_ops.loop,
					autoplay: owlsl_ops.autoplay,
					autoplayTimeout: owlsl_ops.autoplayTimeout,
					center: owlsl_ops.center,
					nav: owlsl_ops.nav,
					dots: owlsl_ops.dots,
					thumbs: owlsl_ops.thumbs,
					autoplayHoverPause: owlsl_ops.autoplayHoverPause,
					slideBy: owlsl_ops.slideBy,
					smartSpeed: owlsl_ops.smartSpeed,
					navText:[owlsl_ops.prev,owlsl_ops.next],
				});

			});

		});
		// end testimonial

		/* Menu howdy */
		elementorFrontend.hooks.addAction('frontend/element_ready/ova_menu_howdy.default', function(){
			$('.ova-menu-howdy ul.menu li a').on('click', function(){
				$('.ova-menu-howdy ul.menu li a').removeClass('active');
				$(this).addClass('active');
			});

			$(window).scroll(function () {
				if( $('.ova-menu-howdy').length > 0 ){

					var header = $('.ova-menu-howdy');
					var scroll = $(window).scrollTop();

					if (scroll >= 200) {
						header.addClass('ready_active');
					}else{
						header.removeClass('ready_active');
					}
					if (scroll >= 250) {
						header.addClass('active_fixed');
					} else{
						header.removeClass('active_fixed');
					}

				}

			});
		});


		/* banner 1 */
		elementorFrontend.hooks.addAction('frontend/element_ready/ova_banner_1.default', function(){

			function fadeInReset(element) {
				$(element).find('*[data-animation]').each(function(){
					var animation = $(this).data( 'animation' );
					$(this).removeClass( 'animated' );
					$(this).removeClass( animation );
					$(this).css({ opacity: 0 });
				});
			}

			function fadeIn(element) {

				var $title = $(element).find( '.active .elementor-slide-title' );
				var animation_title = $date.data( 'animation' );
				var duration_title  = parseInt( $date.data( 'animation_dur' ) );

				setTimeout(function(){
					$title.addClass(animation_title).addClass('animated').css({ opacity: 1 });
				}, duration_title);

				var $sub_title = $(element).find( '.active .elementor-slide-sub-title' );
				var animation_sub_title = $date.data( 'animation' );
				var duration_sub_title  = parseInt( $date.data( 'animation_dur' ) );

				setTimeout(function(){
					$sub_title.addClass(animation_sub_title).addClass('animated').css({ opacity: 1 });
				}, duration_sub_title);

				var $desc = $(element).find( '.active .elementor-slide-desc' );
				var animation_desc = $date.data( 'animation' );
				var duration_desc  = parseInt( $date.data( 'animation_dur' ) );

				setTimeout(function(){
					$desc.addClass(animation_desc).addClass('animated').css({ opacity: 1 });
				}, duration_desc);

				var $form = $(element).find( '.active .elementor-slide-form' );
				var animation_form = $date.data( 'animation' );
				var duration_form  = parseInt( $date.data( 'animation_dur' ) );

				setTimeout(function(){
					$form.addClass(animation_form).addClass('animated').css({ opacity: 1 });
				}, duration_form);

			}
		});
		// end banner 1

		/* exhibitions slide */
		elementorFrontend.hooks.addAction('frontend/element_ready/ova_feauture_list_howdy.default', function(){

			function fadeInReset(element) {
				$(element).find('*[data-animation]').each(function(){
					var animation = $(this).data( 'animation' );
					$(this).removeClass( 'animated' );
					$(this).removeClass( animation );
					$(this).css({ opacity: 0 });
				});
			}

			function fadeIn(element) {

				var $item = $(element).find( '.active .elementor-slide-item' );
				var animation_item = $date.data( 'animation' );
				var duration_item  = parseInt( $date.data( 'animation_dur' ) );

				setTimeout(function(){
					$item.addClass(animation_item).addClass('animated').css({ opacity: 1 });
				}, duration_item);

			}

		});
        // end exhibitions slide

        /* banner 6 */
        elementorFrontend.hooks.addAction('frontend/element_ready/ova_banner_6.default', function(){

        	var date = $('.due_date').data('day').split(' ');
        	var day = date[0].split('-');
        	var time = date[1].split(':');
        	var date_countdown = new Date( day[0], day[1]-1, day[2], time[0], time[1] );
        	$('.due_date').countdown({until: date_countdown, format: 'DHMS'});

        });
        // end banner 6


        elementorFrontend.hooks.addAction('frontend/element_ready/ova_banner_4.default', function(){

        	function getId(url) {
        		var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
        		var match = url.match(regExp);

        		if (match && match[2].length == 11) {
        			return match[2];
        		} else {
        			return 'error';
        		}
        	}

        	var url = $("#video-l").attr('video-url');

        	var videoId = getId(url);

        	var iframeMarkup = '<iframe width="555" height="320" src="//www.youtube.com/embed/' 
        	+ videoId + '" frameborder="0" allowfullscreen></iframe>';

        	$('.video-youtube').append( iframeMarkup );

        });


        /* Menu Henbergar */
        elementorFrontend.hooks.addAction('frontend/element_ready/ova_henbergar_menu.default', function(){

        	$( '.ova_menu_canvas .ova_openNav' ).on( 'click', function(){
        		$( this ).closest('.ova_wrap_nav').find( '.ova_nav_canvas' ).removeClass( 'hide' );
        		$( this ).closest('.ova_wrap_nav').find( '.ova_nav_canvas' ).addClass( 'show' );
        		$( '.ova_menu_canvas .ova_closeCanvas' ).css( 'width', '100%' );

        		$( this ).closest('.ova_wrap_nav').find( '.ova_nav_canvas' ).addClass('active');


        		$( 'body' ).css( 'background-color', 'rgba(0,0,0,0.2)' );

        	});

        	$( '.ova_menu_canvas .ova_closeNav' ).on( 'click', function(){
        		$( this ).closest('.ova_wrap_nav').find( '.ova_nav_canvas' ).removeClass( 'show' );
        		$( this ).closest('.ova_wrap_nav').find( '.ova_nav_canvas' ).addClass( 'hide' );
        		$( '.ova_menu_canvas .ova_closeCanvas' ).css( 'width', '0' );

        		$( this ).closest('.ova_wrap_nav').find( '.ova_nav_canvas' ).removeClass('active');



        		$( 'body' ).css( 'background-color', 'transparent' );

        	});

       // Display in mobile
       $( '.ova_menu_canvas li.menu-item button.dropdown-toggle').off('click').on( 'click', function() {
       	$(this).parent().toggleClass('active_sub');
       });
       

       $(window).scroll(function () {
       	if( $('.ovamenu_shrink').length > 0 ){

       		var header = $('.ovamenu_shrink');
       		var scroll = $(window).scrollTop();

       		if (scroll >= 200) {
       			header.addClass('ready_active');
       		}else{
       			header.removeClass('ready_active');
       		}
       		if (scroll >= 250) {
       			header.addClass('active_fixed');
       		} else{
       			header.removeClass('active_fixed');
       		}

       	}

       });

   });
        /* end Menu Henbergar */


        /* Menu Henbergar */
        elementorFrontend.hooks.addAction('frontend/element_ready/video_popup.default', function(){
        	$(document).ready( function(){
        		$("#video").videoPopup({
        			autoplay: 1,
        			controlsColor: 'white',
        			showVideoInformations: 0,
        			width: 1000,
        			customOptions: {
        				rel: 0,
        				end: 60
        			}
        		});
        	});
        	

        });
        /* end Menu Henbergar */


    });

})(jQuery);
