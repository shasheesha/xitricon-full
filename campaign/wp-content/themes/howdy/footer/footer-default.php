<footer class="footer">
	<div class="container">
		<div class="row">
			<?php esc_html_e( 'Design and Develop by Ovatheme', 'howdy' ); ?>
		</div>
	</div>
</footer>