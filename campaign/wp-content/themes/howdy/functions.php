<?php
	if(defined('HOWDY_URL') 	== false) 	define('HOWDY_URL', get_template_directory());
	if(defined('HOWDY_URI') 	== false) 	define('HOWDY_URI', get_template_directory_uri());

	load_theme_textdomain( 'howdy', HOWDY_URL . '/languages' );
	
	// require libraries, function
	require( HOWDY_URL.'/inc/init.php' );

	// Add js, css
	require( HOWDY_URL.'/extend/add_js_css.php' );
	
	// require walker menu
	require_once (HOWDY_URL.'/inc/ova_walker_nav_menu.php');
	

	// register menu, widget
	require( HOWDY_URL.'/extend/register_menu_widget.php' );

	// require content
	require_once (HOWDY_URL.'/content/define_blocks_content.php');
	
	// require breadcrumbs
	require( HOWDY_URL.'/extend/breadcrumbs.php' );

	// Hooks
	require( HOWDY_URL.'/inc/class_hook.php' );

	
	/* Customize */
	//  include plugin.php to use is_plugin_active()
	if( current_user_can('customize') ){
	    require_once HOWDY_URL.'/customize/custom-control/google-font.php';
	    require_once HOWDY_URL.'/customize/custom-control/heading.php';
	}
	require_once HOWDY_URL.'/customize/class-customize.php';
    require_once HOWDY_URL.'/customize/render-style.php';
    
    
    
	
	// Require metabox
	if( is_admin() ){
		// Require TGM
		require_once ( HOWDY_URL.'/install_resource/active_plugins.php' );		
	}