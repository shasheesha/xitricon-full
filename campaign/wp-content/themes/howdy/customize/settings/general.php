<?php 
$general_css = '';


/* Primary Font */
$default_primary_font = json_decode( howdy_default_primary_font() );
$primary_font = json_decode( get_theme_mod( 'primary_font' ) ) ? json_decode( get_theme_mod( 'primary_font' ) ) : $default_primary_font;
$primary_font_family = $primary_font->font;


/* General Typo */
$general_font_size = get_theme_mod( 'general_font_size', '17px' );
$general_line_height = get_theme_mod( 'general_line_height', '28px' );
$general_letter_space = get_theme_mod( 'general_letter_space', '0px' );
$general_color = get_theme_mod( 'general_color', '#7c8595' );


$primary_color = get_theme_mod( 'primary_color', '#008aff' );

/* Second Font */
$default_second_font = json_decode( howdy_default_second_font() );
$second_font = json_decode( get_theme_mod( 'second_font' ) ) ? json_decode( get_theme_mod( 'second_font' ) ) : $default_second_font;
$second_font_family = $second_font->font;


$general_css .= <<<CSS

body{
	font-family: {$primary_font_family}, sans-serif;
	font-weight: 400;
	font-size: {$general_font_size};
	line-height: {$general_line_height};
	letter-spacing: {$general_letter_space};
	color: {$general_color};
}
p{
	color: {$general_color};
	line-height: {$general_line_height};
}

h1,h2,h3,h4,h5,h6,.second_font, ul.home-benefits li,
.howdy-according .elementor-accordion-item .elementor-tab-title a,
ul.pricing-list li.price-value,
ul.pricing-list li.price-title,
.ova-banner-3 .banner-3 .wp-button a:not(:last-child),
.ova-banner-3 .banner-3 .wp-button a:last-child
{
	font-family: {$second_font_family}, sans-serif;
}

a,
article.post-wrap .post-meta .post-meta-content i,
.sidebar .widget ul li a,
ul.commentlists li.comment .comment_item .comment-details .author-name .date_reply .date,
article.post-wrap h2.post-title a:hover,
.blogname
{
	color: {$primary_color};
}
article.post-wrap .post-footer .post-readmore a,
.content_comments .wrap_comment_form .comment-form .form-submit #submit{
	background-color: {$primary_color};	
}
.content_comments .wrap_comment_form .comment-form .form-submit #submit{
	border-color: {$primary_color};	
}

@media (max-width: 991px){
.content_comments .commentlists li.comment article.comment_item .comment-author .author-name.mobile .date {
    font-size: 14px;
    color: {$primary_color};
}
.content_comments .commentlists li.comment article.comment_item .comment-author .author-name.mobile .ova_reply{
	display: flex;
	

}
.content_comments .commentlists li.comment article.comment_item .comment-author .author-name.mobile .ova_reply a{
	color: #343434;
}
.content_comments .commentlists li.comment article.comment_item .comment-author .author-name.mobile .ova_reply a.comment-reply-link{
	padding-right: 5px;
	color: #343434;
}


}




CSS;



return $general_css;


