<?php
add_action('wp_enqueue_scripts', 'howdy_theme_scripts_styles');
add_action('wp_enqueue_scripts', 'howdy_theme_script_default');


function howdy_theme_scripts_styles() {

    // enqueue the javascript that performs in-link comment reply fanciness
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' ); 
    }
    
    /* Add Javascript  */
    wp_enqueue_script( 'bootstrap', HOWDY_URI.'/assets/libs/bootstrap/js/bootstrap.bundle.min.js' , array( 'jquery' ), null, true );
    wp_enqueue_script( 'select2', HOWDY_URI.'/assets/libs/select2/select2.min.js' , array( 'jquery' ), null, true );

    wp_enqueue_script('owl-carousel-js', HOWDY_URI.'/assets/libs/owl-carousel/owl.carousel.min.js', array('jquery'),null,true);
    

    wp_enqueue_script('howdy-script', HOWDY_URI.'/assets/js/script.js', array('jquery'),null,true);

    /* Add Css  */
    wp_enqueue_style('bootstrap', HOWDY_URI.'/assets/libs/bootstrap/css/bootstrap.min.css', array(), null);
    

    wp_enqueue_style( 'select2', HOWDY_URI. '/assets/libs/select2/select2.min.css', array(), null );

    wp_enqueue_style('v4-shims', HOWDY_URI.'/assets/libs/fontawesome/css/v4-shims.min.css', array(), null);
    wp_enqueue_style('fontawesome', HOWDY_URI.'/assets/libs/fontawesome/css/all.min.css', array(), null);
    wp_enqueue_style('elegant-font', HOWDY_URI.'/assets/libs/elegant_font/ele_style.css', array(), null);
    wp_enqueue_style('owl-carousel-css', HOWDY_URI.'/assets/libs/owl-carousel/assets/owl.carousel.min.css', array(), null);

    wp_enqueue_style( 'flaticon', HOWDY_URI.'/assets/libs/flaticon/font/flaticon.css', array(), null );
    
    
    
    wp_enqueue_style('howdy-theme', HOWDY_URI.'/assets/css/theme.css', array(), null);

    

}

function howdy_theme_script_default(){

    if ( is_child_theme() ) {
      wp_enqueue_style( 'howdy-parent-style', trailingslashit( get_template_directory_uri() ) . 'style.css', array(), null );
    }

    wp_enqueue_style( 'howdy-style', get_stylesheet_uri(), array(), null );
}