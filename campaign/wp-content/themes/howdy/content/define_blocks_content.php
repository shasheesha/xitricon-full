<?php

/* This is functions define blocks to display post */

if ( ! function_exists( 'howdy_content_thumbnail' ) ) {
  function howdy_content_thumbnail( $size ) {
    if ( has_post_thumbnail()  && ! post_password_required() || has_post_format( 'image') )  :
      the_post_thumbnail( $size, array('class'=> 'img-responsive' ));
    endif;
  }
}

if ( ! function_exists( 'howdy_postformat_video' ) ) {
  function howdy_postformat_video( ) { ?>
    <?php if(has_post_format('video') && wp_oembed_get(get_post_meta(get_the_id(), "howdy_met_embed_media", true))){ ?>
	    <div class="js-video postformat_video">
	        <?php echo wp_oembed_get(get_post_meta(get_the_id(), "howdy_met_embed_media", true)); ?>
	    </div>
    <?php } ?>
  <?php }
}

if ( ! function_exists( 'howdy_postformat_audio ') ) {
  function howdy_postformat_audio( ) { ?>
    <?php if(has_post_format('audio') && wp_oembed_get(get_post_meta(get_the_id(), "howdy_met_embed_media", true))){ ?>
	    <div class="js-video postformat_audio">
	        <?php echo wp_oembed_get(get_post_meta(get_the_id(), "howdy_met_embed_media", true)); ?>
	    </div>
    <?php } ?>
  <?php }
}

if ( ! function_exists( 'howdy_content_title' ) ) {
  function howdy_content_title() { ?>

    <?php if ( is_single() ) : ?>
      <h1 class="post-title">
          <?php the_title(); ?>
      </h1>
    <?php else : ?>
      <h2 class="post-title">
        <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
          <?php the_title(); ?>
        </a>
      </h2>
      <?php endif; ?>

 <?php }
}


if ( ! function_exists( 'howdy_content_meta' ) ) {
  function howdy_content_meta( ) { ?>
	    <span class="post-meta-content">
		    <span class=" post-date">
		        <span class="left"><i class="flaticon-calendar"></i></span>
		        <span class="right"><?php the_time( get_option( 'date_format' ));?></span>
		    </span>
		    <span class="slash">/</span>
		    <span class=" post-author">
		        <span class="left"><i class="fas fa-user-alt"></i></span>
		        <span class="right"><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php the_author_meta( 'display_name' ); ?></a></span>
		    </span>
		    <span class="slash">/</span>
		    <span class=" comment">
		        <span class="left"><i class="fas fa-comment-alt"></i></span>
		        <span class="right"><a href="<?php the_permalink();?>">                    
		            <?php comments_popup_link(
		            	esc_html__(' 0 comment', 'howdy'), 
		            	esc_html__(' 1 comment', 'howdy'), 
		            	' % '.esc_html__('comments', 'howdy'),
		            	'',
                  		esc_html__( 'Comment off', 'howdy' )
		            ); ?>
		        </a></span>                
		    </span>
		</span>
  <?php }
}

if ( ! function_exists( 'howdy_content_meta_single' ) ) {
  function howdy_content_meta_single( ) { ?>
	    <span class="post-meta-content">
		    <span class=" post-date">
		        <span class="left"><i class="flaticon-calendar"></i></span>
		        <span class="right"><?php the_time( get_option( 'date_format' ));?></span>
		    </span>
		    <span class="slash">/</span>
		    <span class="general-meta categories">
		        <i class="fa fa-bookmark"></i>
		        <?php the_category('&sbquo;&nbsp;'); ?>
		    </span>
		    <span class="slash">/</span>
		    <span class=" post-author">
		        <span class="left"><i class="fas fa-user-alt"></i></span>
		        <span class="right"><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php the_author_meta( 'display_name' ); ?></a></span>
		    </span>
		    <span class="slash">/</span>
		    <span class=" comment">
		        <span class="left"><i class="fas fa-comment-alt"></i></span>
		        <span class="right"><a href="<?php the_permalink();?>">                    
		            <?php comments_popup_link(
		            	esc_html__(' 0 comment', 'howdy'), 
		            	esc_html__(' 1 comment', 'howdy'), 
		            	' % '.esc_html__('comments', 'howdy'),
		            	'',
                  		esc_html__( 'Comment off', 'howdy' )
		            ); ?>
		        </a></span>                
		    </span>
		</span>
  <?php }
}

if ( ! function_exists( 'howdy_content_body' ) ) {
  function howdy_content_body( ) { ?>
  	<div class="post-excerpt">
		<?php if(is_single()){
		    the_content();
		    wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'howdy' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '%',
				'separator'   => '',
			) );             
		}else{
			the_excerpt();
		}?>
	</div>

	<?php 
	}
}

if ( ! function_exists( 'howdy_content_readmore' ) ) {
  function howdy_content_readmore( ) { ?>
  	<div class="post-footer">
		<div class="post-readmore">
		    <a class="btn btn-theme btn-theme-transparent" href="<?php the_permalink(); ?>"><?php  esc_html_e('Read more', 'howdy'); ?></a>
		</div>
	</div>
 <?php }
}

if ( ! function_exists( 'howdy_content_tag' ) ) {
  function howdy_content_tag( ) { ?>
	
	<?php if (has_tag() || has_filter( 'ova_share_social' )) : ?>
	 <footer class="post-tag-howdy">
	        <?php if(has_tag()){ ?>
	            <div class="post-tags-howdy">
	            	<span class="ovatags"><?php esc_html_e('Tags: ', 'howdy'); ?></span>
	                <?php the_tags('','',''); ?>
	            </div>
	        <?php } ?>

	        <?php if( has_filter( 'ova_share_social' ) ){ ?>
		        <div class="share_social-howdy">
		        	<?php echo apply_filters('ova_share_social', get_the_permalink(), get_the_title() ); ?>
		        </div>
	        <?php } ?>
	    </footer>
	<?php endif ?>
	
 <?php }
}

if ( ! function_exists( 'howdy_content_gallery' ) ) {
 	function howdy_content_gallery( ) {

			$gallery = get_post_meta(get_the_ID(), 'howdy_met_file_list', true) ? get_post_meta(get_the_ID(), 'howdy_met_file_list', true) : '';

		    $k = 0;
		    if($gallery){ $i=0; ?>

		        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
				  <!-- Indicators -->
				  <ol class="carousel-indicators">
				  	<?php foreach ($gallery as $key => $value) { ?>
				    	<li data-target="#carousel-example-generic" data-slide-to="<?php echo esc_attr($i); ?>" class="<?php echo ($i==0) ? 'active':''; ?>"></li>
				    <?php $i++; } ?>
				  </ol>

				  <!-- Wrapper for slides -->
				  <div class="carousel-inner" role="listbox">
				  	<?php foreach ($gallery as $key => $value) { ?>
					    <div class="item <?php echo esc_attr($k==0)?'active':'';$k++; ?>">
					      <img class="img-responsive" src="<?php  echo esc_attr($value); ?>" alt="<?php the_title_attribute(); ?>">
					    </div>
				   	<?php } ?>
				   </div>

				</div>

		       
		        <?php
		    }
	}
}

//Custom comment List:
if ( ! function_exists( 'howdy_theme_comment' ) ) {
function howdy_theme_comment($comment, $args, $depth) {

   $GLOBALS['comment'] = $comment; ?>   
   <li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
     <article class="comment_item" id="comment-<?php comment_ID(); ?>">

         <header class="comment-author">
         	<?php echo get_avatar($comment,$size='70', $default = 'mysteryman' ); ?>
         	<div class="author-name mobile">

            	<div class="name">
            		<?php printf('%s', get_comment_author_link()) ?>	
            	</div>

            	<div class="date_reply">
	            	<div class="date">
	            		<?php printf(get_comment_date()) ?>	
	            	</div>

	                <div class="ova_reply">
			            <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
			            <?php edit_comment_link( '<i class="icon_pencil-edit"></i>'.esc_html__('Edit', 'howdy'), '  ', '' );?>
		            </div>
	            </div>
	        </div>
         </header>

         <section class="comment-details">

            <div class="author-name desk">

            	<div class="name">
            		<?php printf('%s', get_comment_author_link()) ?>	
            	</div>

            	<div class="date_reply">
	            	<div class="date">
	            		<?php printf(get_comment_date()) ?>	
	            	</div>

	                <div class="ova_reply">
			            <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
			            <?php edit_comment_link( '<i class="icon_pencil-edit"></i>'.esc_html__( 'Edit', 'howdy' ), '  ', '' );?>
		            </div>
	        	</div>

	        </div> 

            <div class="comment-body clearfix comment-content">
			    <?php comment_text() ?>
			</div>

        </section>

          <?php if ($comment->comment_approved == '0') : ?>
             <em><?php esc_html_e('Your comment is awaiting moderation.', 'howdy') ?></em>
             <br />
          <?php endif; ?>
        
     </article>
<?php
}
}