(function($){
	"use strict";
	$(document).ready(function(){

		/* Scroll to top */
		howdy_scrollUp();
		function howdy_scrollUp(options) {
		           
		    var defaults = {
		        scrollName: 'scrollUp', 
		        topDistance: 600, 
		        topSpeed: 800, 
		        animation: 'fade', 
		        animationInSpeed: 200, 
		        animationOutSpeed: 200, 
		        scrollText: '<i class="fas fa-angle-up"></i>', 
		        scrollImg: false, 
		        activeOverlay: false 
		    };

		    var o = $.extend({}, defaults, options),
		            scrollId = '#' + o.scrollName;


		    $('<a/>', {
		        id: o.scrollName,
		        href: '#top',
		        title: o.scrollText
		    }).appendTo('body');


		    if (!o.scrollImg) {

		        $(scrollId).html(o.scrollText);
		    }


		    $(scrollId).css({'display': 'none', 'position': 'fixed', 'z-index': '2147483647'});


		    if (o.activeOverlay) {
		        $("body").append("<div id='" + o.scrollName + "-active'></div>");
		        $(scrollId + "-active").css({'position': 'absolute', 'top': o.topDistance + 'px', 'width': '100%', 'border-top': '1px dotted ' + o.activeOverlay, 'z-index': '2147483647'});
		    }


		    $(window).scroll(function () {
		        switch (o.animation) {
		            case "fade":
		                $(($(window).scrollTop() > o.topDistance) ? $(scrollId).fadeIn(o.animationInSpeed) : $(scrollId).fadeOut(o.animationOutSpeed));
		                break;
		            case "slide":
		                $(($(window).scrollTop() > o.topDistance) ? $(scrollId).slideDown(o.animationInSpeed) : $(scrollId).slideUp(o.animationOutSpeed));
		                break;
		            default:
		                $(($(window).scrollTop() > o.topDistance) ? $(scrollId).show(0) : $(scrollId).hide(0));
		        }
		    });

		    
		    $(scrollId).on( "click", function (event) {
		        $('html, body').animate({scrollTop: 0}, o.topSpeed);
		        event.preventDefault();
		    });

		}

		/* Fix empty menu in test_uni_data */
		if( $( '.widget_nav_menu ul li' ).length > 0 ){
			$( '.widget_nav_menu ul li a:empty' ).parent().css('display','none');
		}

		/* Select 2 */
		$('select').select2({ 
			width: '100%'
		});


		/* Stick Menu When Scroll Page */
		function howdy_scroll(){
			if( $('.ovamenu_shrink').length > 0 ){
				
				var header = $('.ovamenu_shrink');
				var scroll = $(window).scrollTop();

				if (scroll >= 250) {
		             header.addClass('active_fixed');
		        } else{
		            header.removeClass('active_fixed');
		        }

			}
		}
		$(window).scroll(function () { howdy_scroll(); });


		/* Stick Menu Howdy When Scroll Page */
		function howdy_scroll_2(){
			if( $('.fixed_menu_howdy').length > 0 ){
				
				var header = $('.fixed_menu_howdy');
				var scroll = $(window).scrollTop();

				if (scroll >= 80) {
		             header.addClass('active_section_menu');
		        } else{
		            header.removeClass('active_section_menu');
		        }

			}
		}
		$(window).scroll(function () { howdy_scroll_2(); });

		/* Popup Image - PrettyPhoto */
		if( $("a[data-gal^='prettyPhoto']").length > 0 ){
		 	$("a[data-gal^='prettyPhoto']").prettyPhoto({hook: 'data-gal', theme: 'facebook',slideshow:5000, autoplay_slideshow:true});
	    }
	    

	    $( '.ovatheme_header_default li.menu-item button.dropdown-toggle').off('click').on( 'click', function() {
		    $(this).parent().toggleClass('active_sub');
		});


		$('#button_menu_howdy').on('click', function() {

			$('#menu_howdy').slideToggle( "slow" );

		});


		$('.ova_menu_clasic ul.menu li a').on('click', function() {
			$('.ova_nav ul.menu li a').removeClass('current_active');
			$(this).addClass('current_active');
		})


	});	

})(jQuery);