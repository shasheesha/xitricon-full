<?php get_header();  ?>

<div class="error_404_page">
	<div class="container">
		<div class="pnf-content">
			<img src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/404_error.png' );?>" alt="<?php esc_attr_e( 'Page Not Found', 'howdy' ) ?>">
			<h3 class="second_font"><?php esc_html_e( 'Ohh! Page Not Found', 'howdy' ); ?></h3>
			<p class="desc"><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try a search?', 'howdy' ); ?></p>
			
			<?php get_search_form(); ?>

			
		</div>		
	</div>   
</div>

<?php get_footer(); ?>