<div class="position-fixed bottom-0 end-0 p-3" style="z-index: 11">
<div id="submit-success" class="toast align-items-center text-white bg-primary border-0" role="alert" aria-live="assertive" aria-atomic="true">
  <div class="d-flex">
    <div class="toast-body">
        
      Thank you for providing us your details.<br>We will get back to you as soon as possible.
    </div>
    <button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>
  </div>
</div>
</div>