<footer>
        <div class="container">
            <div class="row footer-top">
                <div class="col-md-4 footer-logo">
                    <img class="main-logo" src="./assets/logo2.svg" alt="" srcset="">
                </div>
                <div class="col-md-8 nav-menu">
                    <ul>
                        <li><a href="./index.php">Home</a></li>
                        <li><a href="./about-us.php">About Us</a></li>
                        <li><a href="./service.php">Services</a></li>
                        <li><a href="./navulia.php">Navulia E-Procurement</a></li>
                        <li><a href="./contact.php">Contact Us</a></li>
                    </ul>
                </div>
            </div>
            <div class="row top-border footer-bottom">
                <div class="col-md-6 other-links-area">
                    <a href="./policy.php">Privacy Policy</a>
                    <a href="./termsConditions.php">Terms & Conditions</a>
                    <a href="./gdpr.php">GDPR Compliance</a>
                </div>
                <div class="col-md-6 social-media">
                    <p class="hint-text">Follow us on</p>
                    <div class="media-icon-area">
                        <div class="media-icon">
                            <a href="https://www.linkedin.com/company/xitricon-private-limited/">
                                <img src="./assets/icons/Layer 1 8.svg" alt="">
                            </a>    
                        </div>
                        <div class="media-icon">
                            <a href="https://www.facebook.com/xitricon">
                                <img src="./assets/icons/Layer 2 5.svg" alt="">
                            </a>
                        </div>
                        <div class="media-icon">
                            <a href="https://twitter.com/xitricon">
                                <img src="./assets/icons/Layer 3 2.svg" alt="">
                            </a>
                        </div>
                        <div class="media-icon">
                            <a href="https://www.instagram.com/xitricon/">
                                <img src="./assets/icons/Layer 4 1.svg" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <div style="text-align: center;
    padding: 12px;
    letter-spacing: 1px;
    font-size:10px;
    background-image: linear-gradient(to right, #152c4f, #174c9c);
    color: white;">© ALL RIGHTS RESERVED 2023 XITRICON (PRIVATE) LIMITED</div>
