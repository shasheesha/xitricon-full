<div class="modal fade subscribe-modal" id="subscribe-modal" tabindex="-1" aria-labelledby="subscribe-modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <h3 class="title-text">Stay Updated</h3>
                <p class="primary-para">Stay in the loop! Enter your email below to receive the latest updates on our Products and Services.</p>
                <div class="subscribe-form-area">
                    <form action="" method="">
                        <div class="form-floating">
                            <input type="email" class="form-control" id="newsletter_email" placeholder="ex: name@example.com" required>
                            <label for="email">Email</label>
                            <!-- <div class="valid-feedback">Looks good!</div> -->
                            <div class="invalid-feedback">Please provide e-mail address.</div>
                        </div>
                        <button class="btn btn-primary cus-primary" onclick="addNewSubscriber()" type="button">Subscribe</button>
                    </form>
                </div>
                <button type="button" class="btn btn-secondary trans-close-btn" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>