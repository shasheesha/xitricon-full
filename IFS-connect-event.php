<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>IFS Connect UK & Ireland | Event - XITRICON </title>
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Plus Jakarta Sans' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 
</head>

<body>

    <main>
        <div class="hero">
        <!--<div class="py-3"></div>-->
            <?php include 'componets/nav-blue.php' ?>
            <?php include 'componets/mobile-nav.php' ?>

            <?php include 'componets/IFS-connect-event.php' ?>

            <!-- Footer -->
            <?php include 'componets/footer.php' ?>
            <!-- Footer end -->

             
            <!-- PopUp -->
            <?php include 'componets/popup.php' ?>
            <?php include 'componets/thank-you-contact-success.php' ?>
            <!-- PopUp -->
    </main>
    <!-- 
    <script>
        function buttonClickEvent() {
            window.location.href = "service.php";
        }
    </script> -->

    <script>
    window.onscroll = function() {
        myFunction()
    };

    var navbar = document.getElementById("navbar");
    var sticky = navbar.offsetTop;

    function myFunction() {
        if (window.pageYOffset > sticky) {
            navbar.classList.add("sticky-w")
        } else {
            navbar.classList.remove("sticky-w");
        }
    }
    </script>
</body>

<?php include 'componets/script_includes.php' ?>

</html>