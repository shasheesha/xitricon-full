<div class="container py-5 px-4">
    <div class="col-12">
        <h1 class="blog-header" font="bold">Senior/Lead IFS FSM-PSO Consultant</h1>
    </div>

    <!--div style="padding-top:20px; padding-bottom:10px">
        <img class="text-center about-banner" src="assets/img/blog-cover-img3.png" width="100%">
    </div-->

    <div class="col-12 py-3">
        <p class="mt-2 lh-lg font-size-p">About The Role:</p>
        <p>Xitricon is a consulting, technology and digital transformation services company, headquartered in Dubai, UAE.
        <br>
       We are actively looking for a Senior/Lead IFS FSM AND PSO Consultants with a strong background in Implementations and Support for IFS Projects. In this role, you will be exposed to IFS latest versions Cloud and other emerging technologies in various growth industries. This role will require interaction with customers and within a team. Individual must have good communication, and execution skills. </p>

        <p class="mt-2 lh-lg font-size-p">Key Responsibilities:</p>

         <!--  a href="https://connect.ifs.com/uki/agenda/"--><!--button class="btn2 btn-default main-bg-color" type="submit">Click to Register</button></a-->
         <ul>
            <li>5+ Years of experience in guiding our customers to improve their business through a deep understanding of the core IFS solution and industry accelerators.</li>
            <li>Understanding the customer’s business, industry and local field service management drivers, practices and policies.</li>
            <li>Acting as a trusted advisor in articulating and critically evaluating customer-specific business solutions.</li>
            <li>Seeking opportunities to increase customer satisfaction and deepen customer relationships.</li>
            <li>Understanding customer business needs and guiding them in the most effective usage of IFS Products.</li>
            <li>Leading and owning the solution definition and scope for the FSM portion of the IFS Solution.</li>
            <li>Identifying and bridging the gap between the customer’s business requirements and IFS Products.</li>
            <li>Must be PSO certified. Hands-on experience of all the Configuration, Designing and Customization capabilities of PSO functionalities.</li>
            <li> Deep understanding of planning, scheduling and optimization capabilities of IFS PSO application. Have experience in configuring all the parameters given in PSO application as per customer requirement. Solid hands-on experience integration between IFS FSM with PSO application. </li>
            <li>Have experience of third-party Integration with PSO applications. Experience in designing complex optimization and scheduling solutions (IFS PSO). Experience in sizing and understanding of the IFS hosted solutions.</li>
            <li>Conducting customer training sessions and workshops.</li>
            <li>Guiding and assisting the customer with testing and Go Live cut-over strategies.</li>
            <li>Keeping up to date with regulatory and compliance requirements of the Field Service Management domain in the key focus industries.</li>
            <li>Managing, coaching and mentoring colleagues.</li>
            <li>Contributing to the continuous improvement of existing IFS products, processes and methodology.</li>
            <li>Ensure high quality deliverables through IFS' best practices.</li>

            </ul>
            <p class="mt-2 lh-lg font-size-p">Requirements:</p>
                    <ul><li>An academic background with a Bachelor or Master’s  Degree in Engineering, Business, Information Technology or equivalent.</li>
        <li>Experience delivering Field Service Management ERP/IT Solutions in a similar role.</li>
        <li>Proven ability to lead and influence teams, colleagues, customers and partners.</li>
        <li>Ability to articulate and critically evaluate customer-specific business needs and solutions in a customer facing role.</li>
        <li>Expertise in business process mapping and deployment.</li>
        <li>Strong problem-solving ability, analytical skills and strategic thinking.</li>
        <li>Excellent interpersonal skills including stakeholder management, negotiation and influencing skills.</li>
        <li>Excellent communication and presentation skills at the executive level.</li>
        <li>Ability to work independently and within a team setting.</li>
        <li>Fluent in English and local language, both oral and written.</li>
        <li>Ability to travel nationally and internationally.</li>
        </ul>

            <p class="mt-2 lh-lg font-size-p">Benefits:</p>
            <ul>
             <li>A highly competitive compensation package. </li>
             <li>Opportunity to work remotely.</li>
             <li>An international and diverse work atmosphere working with large clients in multiple geographies. </li>
            </ul>
            <p class="mt-2 lh-lg font-size-p">Equal Opportunity Employer: </p>
            <p>At Xitricon we are building a company where a diverse mix of talented people want to come, stay, and do their best work. We know that our company runs on the hard work and the dedication of our passionate and creative employees.</p>

            <p>If you are interested in this opportunity, please submit your CV to <a href="mailto:shireen.anis@xitricon.com">shireen.anis@xitricon.com</a></p>

        <!-- quote form start here 
                            <section class="quote-form" style="background-image: none;" data-scroll-index="1">
                                <h2 class="form-heading text-center text-uppercase">Enquiry</h2>
                                <span class="form-title text-center"></span>
                                <form id="contactForm" method="post" action="submit_form.php"data-toggle="validator">
                                    <fieldset>
                                        <div class="form-group">
                                            <input type="text" id="name" placeholder="Your Name" class="form-control" required data-error="NEW ERROR MESSAGE">
                                        </div>
                                        <div class="form-group">
                                            <input type="email" id="email" placeholder="Your Email" class="form-control" required data-error="NEW ERROR MESSAGE">
                                        </div>
                                        <div class="form-group">
                                            <input type="tel" id="phone" placeholder="Phone Number" class="form-control" required data-error="NEW ERROR MESSAGE">
                                        </div>
                                        <div id="msgSubmit" class="form-message hidden"></div>
                                        <span class="info"><i class="fa fa-info-circle main-color" aria-hidden="true"></i> We will never share your personal info</span>
                                        <button class="btn2 btn-default main-bg-color" type="submit" id="form-submit">Submit</button>
                                    </fieldset>
                                </form>
                            </section>
                          quote form end here -->

        <!--div class="col-12">
            <img class="text-center about-banner" src="IFS/Social Image Xitricon_1200X630.jpg" width="50%" style="padding-right: 10px;">
        </div-->
        <!--div class="col-4">
            <img class="text-center about-banner" src="assets/img/blog-sub5.png" width="100%" style="padding-right: 10px;">
        </div>
        <div class="col-4">
            <img class="text-center about-banner" src="assets/img/blog-sub6.png" width="100%" style="padding-right: 10px;">
        </div-->
    </div>
</div>