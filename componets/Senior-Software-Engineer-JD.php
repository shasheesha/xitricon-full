<div class="container py-5 px-4">
    <div class="col-12">
        <h1 class="blog-header" font="bold">Senior Software Engineer</h1>
    </div>    
        <div class="col-12 py-3">
            <p class="mt-2 lh-lg font-size-p">Job Summary:</p>
            <p>We are seeking a highly skilled Senior Software Engineer to design, develop, and maintain our cloud-based software products and solutions. The successful candidate will work closely with the development team, product owners, and stakeholders to understand business requirements and design software solutions that meet those requirements. The Senior Software Engineer will also be responsible for implementing and maintaining software development best practices and mentoring junior software engineers. 
            </p>
            <p class="mt-2 lh-lg font-size-p">Key Responsibilities:</p>

        
         <ul>
            <li>Collaborate with product owners and stakeholders to understand business requirements and design software solutions that meet those requirements. </li>
            <li>Develop and maintain software products and solutions, ensuring they are scalable, reliable, and high performing. </li>
            <li>Implement and maintain software development best practices, including continuous integration and continuous deployment, code reviews, and test-driven development. </li>
            <li>Mentor and guide junior software engineers, sharing knowledge and best practices to ensure the development team is operating at the highest level. </li>
            <li>Work collaboratively with cross-functional teams including product management, UX, and QA to ensure timely delivery of high-quality software products and solutions. </li>
            <li>Troubleshoot and debug production issues as needed. </li>
            <li>Stay up to date with emerging trends and technologies in software engineering.  </li>

            </ul> 
               
             <p class="mt-2 lh-lg font-size-p">Requirements:</p>
            <ul>
            <li>Bachelor's or master's degree in computer science or related field. </li>
            <li>At least 3 years of experience in software development in enterprise applications. </li>
            <li>Strong proficiency in one or more programming languages such as Java, Python, or JavaScript. </li>
            <li>Experience with cloud-based product development, preferably on AWS. </li>
            <li>Strong understanding of software development best practices, including object-oriented programming, design patterns, and clean code principles. </li>
            <li>Experience with Agile software development methodologies. </li>
            <li>Experience with version control systems such as Git. </li>
            <li>Excellent analytical and problem-solving skills. </li>
            <li>Strong communication and interpersonal skills. </li>
            <li>Ability to work independently and in a team environment. </li>

            </ul>
             <p class="mt-2 lh-lg font-size-p">Desired Skills: </p>
            <ul>
            <li>Experience with microservices architecture and containerization (Docker, Kubernetes). </li>
            <li>Experience with NoSQL and SQL databases. </li>
            <li>Familiarity with UI frameworks such as Angular, Flutter, React, or Vue.js. </li>
            <li>Familiarity with DevOps tools and practices. </li>
            <li>Familiarity with machine learning and artificial intelligence technologies. </li>
            <li>Experience with API documentation tools such as Swagger.  </li>
            </ul>
            <p class="mt-2 lh-lg font-size-p">Company Culture:</p>
                    <p>Xitricon is a consulting, technology, and digital transformation services company, headquartered in Dubai, UAE. <br>

            If you have the experience and skills required for this position and are excited about the prospect of leading a hybrid agile software development project, we encourage you to apply. We offer competitive compensation packages, flexible work arrangements, and opportunities for career growth and development.
                    </p>
            <p class="mt-2 lh-lg font-size-p">Benefits:</p>
            <ul>
              <li>A highly competitive compensation package </li>
              <li>Medical benefits (with cover for family) </li>
              <li>Opportunity to work remotely </li>
              <li>An international and diverse work atmosphere working with large clients in multiple geographies </li>
            </ul>
            <p class="mt-2 lh-lg font-size-p">Equal Opportunity Employer: </p>
            <p>At Xitricon we are building a company where a diverse mix of talented people want to come, stay, and do their best work. We know that our company runs on the hard work and the dedication of our passionate and creative employees.</p>

            <p>If you are interested in this opportunity, please submit your CV to <a href="mailto:shireen.anis@xitricon.com">shireen.anis@xitricon.com</a>
            </p>

       
    </div>
</div>