<!-- Modal -->
<div class="modal modal-background-fade" id="modalsuccess" tabindex="-1" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">

            <div class="col popup-padding px-5 text-center">

                <img src="assets/img/success.svg" alt="" class="mb-3 success-ico">

                <p> We wanted to let you know that an email has been sent to your registered email address with more
                    information on Navulia E-procurement. We highly recommend that you check your inbox and take a
                    moment to review the details.
                </p>


            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal modal-background-fade" id="modalsuccess_navulia" tabindex="-1" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">

            <div class="col popup-padding px-5 text-center">

                <img src="assets/img/success.svg" alt="" class="mb-3 success-ico">

                <p> Thank you for submitting your details.
                    We've sent an email to your registered email address to download the Navulia e-procurement brochure.
                </p>


            </div>
        </div>
    </div>
</div>