<div class="container py-5 px-4">
    <div class="col-12">
        <h1 class="blog-header" font="bold">Senior Business Analyst / Business Analyst</h1>
    </div>

    <!--div style="padding-top:20px; padding-bottom:10px">
        <img class="text-center about-banner" src="assets/img/blog-cover-img3.png" width="100%">
    </div-->

    <div class="col-12 py-3">
        <p class="mt-2 lh-lg font-size-p">Job Summary:</p>
        <p>We are seeking a highly skilled Business Analyst to understand the business requirements, translate the requirements into Agile based epics and user stories. The successful candidate will work closely with the senior stakeholders, product owners, and development teams to understand business requirements and design software products that meet those requirements. 
        </p>
        <p class="mt-2 lh-lg font-size-p">Key Responsibilities:</p>

         <!--  a href="https://connect.ifs.com/uki/agenda/"--><!--button class="btn2 btn-default main-bg-color" type="submit">Click to Register</button></a-->
         <ul>
  <li>Understanding the client’s business objectives, challenges, and key success metrics.</li>
  <li>Experience in technical writing through user stories based on the business requirements. </li>
  <li>Excellent understanding of Epics & User Stories in Agile methodology of project handling.</li>
  <li>Requirements gathering, solution validation with developers, customer change management experience through building training materials, user guides etc.  </li>
  <li>Identifying and proposing WordPress solutions aligned with the client’s business goals, in coordination with the team. This may involve evaluating the offerings of multiple solutions to identify those that best fit the client’s use case.  </li>
  <li>Working on business pitches and presentations.  </li>
  <li>Assisting team members with the up keeping and upgrades of internal processes. </li>
</ul> 
   
 <p class="mt-2 lh-lg font-size-p">Must Haves: </p>
<ul>
  <li>Being able to speak & write fluently in English. This would be crucial to communicate effectively with internal and external stakeholders.  </li>
  <li>Writing compelling business documents and being able to present thoughts and approaches efficiently. </li>
  <li>Having excellent collaboration skills to comfortably work with cross-functional groups such as client account management, project management, engineering, and business teams. </li>
  <li>7+ years of experience in client servicing, business solutions, or semi-technical roles.  </li>
 
</ul>
 <p class="mt-2 lh-lg font-size-p">Good to Have: </p>
<ul>
  <li>Any previous experience with sales, presentations, contracting, staffing, and working on client solutions.   </li>
  <li>Actively maintaining a blog or journal. Even better if you use WordPress for it. </li>
  <li>Any exposure to platforms such as WordPress, Drupal, Adobe Experience Manager, and Sitecore.(Technology as per your requirement) </li>
  <li>While we do not expect you to code, it will be good if you have a technical understanding of PHP or WordPress..(Technology as per your requirement).</li>
  <li>Any prior experience in a customer service role would be a plus. Familiarity with IT terms & processes would be helpful.  </li>
</ul>
<p class="mt-2 lh-lg font-size-p">Company Culture:</p>
        <p>Xitricon is a consulting, technology, and digital transformation services company, headquartered in Dubai, UAE. <br>

If you have the experience and skills required for this position and are excited about the prospect of leading a hybrid agile software development project, we encourage you to apply. We offer competitive compensation packages, flexible work arrangements, and opportunities for career growth and development.
        </p>
<p class="mt-2 lh-lg font-size-p">Benefits:</p>
<ul>
  <li>A highly competitive compensation package </li>
  <li>Medical benefits (with cover for family) </li>
  <li>Opportunity to work remotely </li>
  <li>An international and diverse work atmosphere working with large clients in multiple geographies </li>
</ul>
<p class="mt-2 lh-lg font-size-p">Equal Opportunity Employer: </p>
<p>At Xitricon we are building a company where a diverse mix of talented people want to come, stay, and do their best work. We know that our company runs on the hard work and the dedication of our passionate and creative employees.</p>

<p>If you are interested in this opportunity, please submit your CV to <a href="mailto:shireen.anis@xitricon.com">shireen.anis@xitricon.com</a></p>

        <!-- quote form start here 
                            <section class="quote-form" style="background-image: none;" data-scroll-index="1">
                                <h2 class="form-heading text-center text-uppercase">Enquiry</h2>
                                <span class="form-title text-center"></span>
                                <form id="contactForm" method="post" action="submit_form.php"data-toggle="validator">
                                    <fieldset>
                                        <div class="form-group">
                                            <input type="text" id="name" placeholder="Your Name" class="form-control" required data-error="NEW ERROR MESSAGE">
                                        </div>
                                        <div class="form-group">
                                            <input type="email" id="email" placeholder="Your Email" class="form-control" required data-error="NEW ERROR MESSAGE">
                                        </div>
                                        <div class="form-group">
                                            <input type="tel" id="phone" placeholder="Phone Number" class="form-control" required data-error="NEW ERROR MESSAGE">
                                        </div>
                                        <div id="msgSubmit" class="form-message hidden"></div>
                                        <span class="info"><i class="fa fa-info-circle main-color" aria-hidden="true"></i> We will never share your personal info</span>
                                        <button class="btn2 btn-default main-bg-color" type="submit" id="form-submit">Submit</button>
                                    </fieldset>
                                </form>
                            </section>
                          quote form end here -->

        <!--div class="col-12">
            <img class="text-center about-banner" src="IFS/Social Image Xitricon_1200X630.jpg" width="50%" style="padding-right: 10px;">
        </div-->
        <!--div class="col-4">
            <img class="text-center about-banner" src="assets/img/blog-sub5.png" width="100%" style="padding-right: 10px;">
        </div>
        <div class="col-4">
            <img class="text-center about-banner" src="assets/img/blog-sub6.png" width="100%" style="padding-right: 10px;">
        </div-->
    </div>
</div>