<div class="container py-5 px-4">
    <div class="col-12">
        <h1 class="blog-header" font="bold">Quality Assurance Engineer</h1>
    </div>

 

    <div class="col-12 py-3">
        <p class="mt-2 lh-lg font-size-p">Job Summary:</p>
        <p>We are looking for an experienced Quality Assurance (QA) Engineer to ensure the quality of our software products and solutions. The successful candidate will be responsible for testing software products and solutions, identifying defects and bugs, and collaborating with the development team to ensure that the products and solutions meet business requirements and user expectations. The QA Engineer will also be responsible for creating test plans, test cases, and automated test scripts, and for reporting test results to the development team and project manager.
        </p>
        <p class="mt-2 lh-lg font-size-p">Key Responsibilities:</p>

         <!--  a href="https://connect.ifs.com/uki/agenda/"--><!--button class="btn2 btn-default main-bg-color" type="submit">Click to Register</button></a-->
         <ul>
        <li>Test software products and solutions to ensure that they meet business requirements and user expectations.</li>
        <li>Create test plans, test cases, and automated test scripts based on business requirements and user stories.</li>
        <li>Collaborate with the development team to identify and fix defects and bugs in software products and solutions.</li>
        <li>Report test results to the development team and project manager, and work with them to prioritize and resolve issues.</li>
        <li>Develop and maintain a library of test cases, automated test scripts, and testing tools.</li>
        <li>Participate in code reviews and provide feedback on testability, reliability, and scalability.</li>
        <li>Stay up to date with emerging trends and technologies in software quality assurance and testing.</li>
        </ul> 
           
         <p class="mt-2 lh-lg font-size-p">Requirements:</p>
        <ul>
        <li>Bachelor's degree in computer science or related field.</li>
        <li>At least 3 years of experience in software quality assurance and testing.</li>
        <li>Strong knowledge of software testing methodologies, tools, and processes.</li>
        <li>Experience in creating test plans, test cases, and automated test scripts.</li>
        <li>Experience with manual and automated testing of web applications, APIs, and mobile applications.</li>
        <li>Strong analytical and problem-solving skills.</li>
        <li>Excellent written and verbal communication skills.</li>
        <li>Ability to work independently and in a team environment.</li>
        <li>Experience with Agile methodologies and software development lifecycle.</li>
        </ul>
         <p class="mt-2 lh-lg font-size-p">Desired Skills: </p>
        <ul>
        <li>Experience with testing tools such as Selenium, Appium, JMeter, and Postman.</li>
        <li>Experience with test management tools such as TestRail and Zephyr.</li>
        <li>Experience with performance testing and load testing.</li>
        <li>Familiarity with continuous integration and continuous delivery (CI/CD) pipelines.</li>
        <li>Experience with cloud-based testing and virtualization.</li>
        <li>Experience with security testing and compliance standards.</li>
        <li>Familiarity with test-driven development (TDD) and behavior-driven development (BDD).</li>
        </ul>
        <p class="mt-2 lh-lg font-size-p">Company Culture:</p>
                <p>Xitricon is a consulting, technology, and digital transformation services company, headquartered in Dubai, UAE. <br>

        If you have the experience and skills required for this position and are excited about the prospect of leading a hybrid agile software development project, we encourage you to apply. We offer competitive compensation packages, flexible work arrangements, and opportunities for career growth and development.
                </p>
        <p class="mt-2 lh-lg font-size-p">Benefits:</p>
        <ul>
          <li>A highly competitive compensation package </li>
          <li>Medical benefits (with cover for family) </li>
          <li>Opportunity to work remotely </li>
          <li>An international and diverse work atmosphere working with large clients in multiple geographies </li>
        </ul>
        <p class="mt-2 lh-lg font-size-p">Equal Opportunity Employer: </p>
        <p>At Xitricon we are building a company where a diverse mix of talented people want to come, stay, and do their best work. We know that our company runs on the hard work and the dedication of our passionate and creative employees.</p>

        <p>If you are interested in this opportunity, please submit your CV to <a href="mailto:shireen.anis@xitricon.com">shireen.anis@xitricon.com</a>
        </p>
        
    </div>
</div>