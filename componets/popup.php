<!-- The Modal -->

<!-- Modal -->
<div class="modal modal-background-fade" id="modal1" tabindex="-1" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg ">
        <div class="modal-content">

            <div class="d-flex row">
                <div class="col popup-padding px-5">
                    <h1 class="py-4" style="font-weight: bold;" id="demo">
                        Schedule a call
                    </h1>
                    <div class="popup-description">
                        <p style="font-size: 12px;">Do you have any questions or want to learn more about the services
                            we offer? <br><br>Fill out the
                            form
                            below
                            and our team will get back to you! </p>
                    </div>
                    <form method="post" id="mail_form2">
                        <input type="text" class="form-control my-2" id="fullname" name="name" placeholder="Name"
                            required />
                        <input type="email" class="form-control my-2" id="email" name="email" placeholder="Email" />
                        <?php include 'componets/country-pickup.php'?>
                        <textarea id="comment" class="form-control my-2" name="comment" rows="4" placeholder="Comment"
                            cols="50"></textarea>

                        <div id="captcha"></div>

                        <button id="sendmail2" type="submit" class="g-recaptcha btn btn-primary btn-style">
                            Send
                        </button>
                    </form>
                </div>
                <div class="col contact-icon">
                    <div class="my-4">
                        <div class="address-section cursor-style " onclick="phoneBtn()">
                            <div class="pop-icons">
                                <img src="assets/img/phone-new.svg" style="height: 28px; width: 28px;">
                            </div>
                            <div class="pop-txt-bottom">
                                <label style="font-weight: bold;">PHONE</label>
                                <label style="color: #FF7D00; font-weight: normal !important;">+94 77 378 8295</label>
                            </div>
                        </div>
                        <div class="address-section cursor-style " onclick="whtsappBtn()">
                            <div class="pop-icons">
                                <img src="assets/img/wtsapp.svg" style="height: 28px; width: 28px;">
                            </div>
                            <div class="pop-txt-bottom">
                                <label style="font-weight: bold;">WHATSAPP</label>
                                <label style="color: #FF7D00; font-weight: normal !important;">+94 77 378 8295</label>
                            </div>
                        </div>

                        <div class="address-section cursor-style " onclick="emailBtn()">
                            <div class="pop-icons">
                                <img src="assets/img/email-icon.svg">
                            </div>
                            <div class="pop-txt-bottom">
                                <label style="font-weight: bold;">EMAIL</label>
                                <label
                                    style="color: #FF7D00; font-weight: normal !important;">sales@xitricon.com</label>
                            </div>
                        </div>



                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
function phoneBtn() {
    window.location.href = "callto:+9477378829";
}

function whtsappBtn() {
    window.location.href = "//api.whatsapp.com/send?phone=+9477378829&text=Xitricon Web";
}

function emailBtn() {
    window.location.href = "mailto:info@xitricon.com";
}
</script>

<!-- Latest compiled and minified JavaScript -->
<!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script> -->