<div class="container py-5 px-4">
    <div class="col-12">
        <h1 class="blog-header" font="bold">Project Manager/Sr. project Manager</h1>
    </div>

    <!--div style="padding-top:20px; padding-bottom:10px">
        <img class="text-center about-banner" src="assets/img/blog-cover-img3.png" width="100%">
    </div-->

    <div class="col-12 py-3">
        <p class="mt-2 lh-lg font-size-p">About The Role:</p>
        <p>Xitricon is a consulting, technology and digital transformation services company, headquartered in Dubai, UAE.
<br>
We are actively looking for a Project Manager/Sr. project Manager with a strong background in Plan, execute, monitor, control, and close large projects according to defined objectives and overall project management best practices and Support for ERP Projects. In this role, you will be exposed to IFS latest versions like  Aurena development Cloud and other emerging technologies. This role require interaction with customers and within a team. Individual must have good communication, and execution skills. 
        </p>
        <p class="mt-2 lh-lg font-size-p">Key Responsibilities:</p>

         <!--  a href="https://connect.ifs.com/uki/agenda/"--><!--button class="btn2 btn-default main-bg-color" type="submit">Click to Register</button></a-->
         <ul>
<li>Plan, execute, monitor, control, and close large projects according to defined objectives and overall project management best practices. </li>
<li>Serve as the primary interface between technology teams on cross- department integration projects. </li>
<li>Work with business analysts, PMs, tech leads, and project stakeholders to create and maintain functional and technical specifications, and related project documents / artifacts. </li>
<li>Create and manage project schedule, dependencies, and critical path though the project life cycle. </li>
<li>Ability to adapt project plan to the changing needs of the business. </li>
<li>Encourage decision- making and resolve conflicts in a timely manner.</li>
<li>Track and report project status for stakeholders, including upper management and users.</li>
<li>Coordinate simultaneous project work of multiple technology teams including in- house and external development, QA, and ITOps. </li>
<li>Risk / Issue identification, management, and mitigation.</li>
<li>Conduct post- execution project analysis and document lessons learned.</li>
</ul> 
   
 <p class="mt-2 lh-lg font-size-p">Requirements:</p>
   <ul><li>Total Experience: 15+ Years</li>
<li>On-site Experience in Years: 2 Year</li>
<li>Implementation Experience: 5-6 Full Life Cycle Implementation</li>
<li>Technical Experience Requirement: Not a must.</li>
<li>Skills Desired: ERP Consultant, ERP FI, Controlling, IFRS,ERP, Supply Chain, SCM</li>
<li>Preferred Qualification: MBA Finance/CA/CPA, B.Com, Graduate</li>
<li>Domain Knowledge: Experience in Banking, Logistics, Shipping, Ports, Terminal is highly desired.</li>
<li>Product Experience: Finance and any other ERP module like GRC, Material Management etc.</li>
<li>Travel Long Term/Short Term: Should be ready to travel abroad for short term on client site.</li>
<li>Certifications: PMP preferred</li>
<li>Project Management Skills: Must have completed 2 end to end implementation as a Project Manager</li>
<li>Types of Project: Implementations, Roll-out, Business Process Re-engineering.</li>
<li>Team Management Skills: Must have</li>
<li>Client Communication Skills: Is a must</li>
<li>Employment Nature (Direct/Sub-con): Direct</li>
</ul>
 <p class="mt-2 lh-lg font-size-p">Summary Job Description:</p>
<ul>
  <li>Job involves ERP implementation, roll-outs for our global clients.</li>
<li>He/She should be able to play key roles in  Global Template Creation, Implementations and roll-out.</li>
<li>He/She should be able independently and convincingly talk to clients at all level.</li>
<li>He/She should be willing to take ownership and work with team.</li>
<li>He/She should be knowledgeable in Best Practice Implementation. Leading project teams to deliver end to end project assignments successfully</li>
<li>Lead the planning and implementation of project facilitate the definition of project scope, milestones goals and deliverable</li>
<li>Manage project change and change requests maximise offshore leverage</li>
<li>Define project tasks and resource requirements. Assemble and coordinate project staff Manage project budget, revenue, margin and overall financial performance</li>
<li>Manage project resource allocation, team management and motivation Plan and schedule project timeline</li>
<li>Track project deliverable using appropriate tools. Provide direction and support to project team.</li>
<li>Client relationship management Quality assurance. Constantly monitor and report on progress of the project to all stakeholders</li>
<li>Present reports defining project progress, problems and solutions. Implement and manage project changes and interventions to achieve project outputs. Project evaluations and assessment of results</li>

</ul>

<p class="mt-2 lh-lg font-size-p">Benefits:</p>
<ul>
  <li>A highly competitive compensation package </li>
  <li>Medical benefits (with cover for family) </li>
  <li>Opportunity to work remotely </li>
  <li>An international and diverse work atmosphere working with large clients in multiple geographies </li>
</ul>
<p class="mt-2 lh-lg font-size-p">Equal Opportunity Employer: </p>
<p>At Xitricon we are building a company where a diverse mix of talented people want to come, stay, and do their best work. We know that our company runs on the hard work and the dedication of our passionate and creative employees.</p>

<p>If you are interested in this opportunity, please submit your CV to <a href="mailto:shireen.anis@xitricon.com">shireen.anis@xitricon.com</a></p>

        <!-- quote form start here 
                            <section class="quote-form" style="background-image: none;" data-scroll-index="1">
                                <h2 class="form-heading text-center text-uppercase">Enquiry</h2>
                                <span class="form-title text-center"></span>
                                <form id="contactForm" method="post" action="submit_form.php"data-toggle="validator">
                                    <fieldset>
                                        <div class="form-group">
                                            <input type="text" id="name" placeholder="Your Name" class="form-control" required data-error="NEW ERROR MESSAGE">
                                        </div>
                                        <div class="form-group">
                                            <input type="email" id="email" placeholder="Your Email" class="form-control" required data-error="NEW ERROR MESSAGE">
                                        </div>
                                        <div class="form-group">
                                            <input type="tel" id="phone" placeholder="Phone Number" class="form-control" required data-error="NEW ERROR MESSAGE">
                                        </div>
                                        <div id="msgSubmit" class="form-message hidden"></div>
                                        <span class="info"><i class="fa fa-info-circle main-color" aria-hidden="true"></i> We will never share your personal info</span>
                                        <button class="btn2 btn-default main-bg-color" type="submit" id="form-submit">Submit</button>
                                    </fieldset>
                                </form>
                            </section>
                          quote form end here -->

        <!--div class="col-12">
            <img class="text-center about-banner" src="IFS/Social Image Xitricon_1200X630.jpg" width="50%" style="padding-right: 10px;">
        </div-->
        <!--div class="col-4">
            <img class="text-center about-banner" src="assets/img/blog-sub5.png" width="100%" style="padding-right: 10px;">
        </div>
        <div class="col-4">
            <img class="text-center about-banner" src="assets/img/blog-sub6.png" width="100%" style="padding-right: 10px;">
        </div-->
    </div>
</div>