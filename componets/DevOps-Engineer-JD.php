<div class="container py-5 px-4">
        <div class="col-12">
        <h1 class="blog-header" font="bold">DevOps Engineer</h1>
        </div>

   
        <div class="col-12 py-3">
        <p class="mt-2 lh-lg font-size-p">Job Summary:</p>
        <p>We are looking for a highly skilled DevOps Engineer to manage the software development process and support continuous integration and deployment (CI/CD) pipelines. The successful candidate will work closely with the development team to ensure that software products and solutions are delivered to production environments quickly and securely.
        </p>
        <p class="mt-2 lh-lg font-size-p">Key Responsibilities:</p>

         <ul>
            <li>Develop and maintain CI/CD pipelines using modern DevOps tools and technologies. </li>
            <li>Collaborate with development teams to ensure that the software development process is optimized for speed, security, and scalability. </li>
            <li>Implement and maintain monitoring and alerting systems to ensure high availability and performance of production environments. </li>
            <li>Work with cloud infrastructure providers such as AWS to ensure that infrastructure is provisioned and managed efficiently and cost-effectively. </li>
            <li>Develop and maintain infrastructure-as-code (IAC) using tools such as Terraform and Ansible. </li>
            <li>Automate and streamline the software development process, including build, test, and deployment processes. </li>
            <li>Implement and maintain security measures to ensure that production environments are secure and compliant with industry standards and regulations. </li>
            <li>Troubleshoot and resolve issues related to infrastructure, CI/CD pipelines, and production environments. </li>
            <li>Develop and maintain documentation related to infrastructure and CI/CD pipelines.  </li>
        </ul> 
   
             <p class="mt-2 lh-lg font-size-p">Requirements:</p>
            <ul>
            <li>Bachelor's degree in computer science, Engineering, or a related field. </li>
            <li>At least 3 years of experience in DevOps, infrastructure engineering, or a related field. </li>
            <li>Strong understanding of CI/CD pipelines and modern DevOps tools and technologies. </li>
            <li>Experience with cloud infrastructure providers such as AWS or Azure. </li>
            <li>Experience with infrastructure-as-code (IAC) using tools such as Terraform and Ansible. </li>
            <li>Experience with monitoring and alerting systems such as Nagios, Prometheus, and Grafana. </li>
            <li>Experience with containerization technologies such as Docker and Kubernetes. </li>
            <li>Strong scripting skills using languages such as Bash, Python, or Ruby. </li>
            <li>Excellent analytical and problem-solving skills. </li>
            <li>Excellent communication and interpersonal skills. </li>
            <li>Ability to work independently and in a team environment. </li>
            </ul>
             <p class="mt-2 lh-lg font-size-p">Desired Skills: </p>
            <ul>
            <li>AWS Certified DevOps Engineer or similar certification. </li>
            <li>Experience with container orchestration technologies such as Docker Swarm or Nomad. </li>
            <li>Experience with configuration management tools such as Chef or Puppet. </li>
            <li>Experience with logging and aggregation tools such as ELK or Splunk. </li>
            <li>Experience with security tools and technologies such as IDS/IPS and WAF. </li>
            <li>Experience with database administration and management. </li>
            <li>Familiarity with Agile methodologies and software development lifecycle.</li>
                        </ul>
            <p class="mt-2 lh-lg font-size-p">Company Culture:</p>
                    <p>Xitricon is a consulting, technology, and digital transformation services company, headquartered in Dubai, UAE. <br>

            If you have the experience and skills required for this position and are excited about the prospect of leading a hybrid agile software development project, we encourage you to apply. We offer competitive compensation packages, flexible work arrangements, and opportunities for career growth and development.
                    </p>
            <p class="mt-2 lh-lg font-size-p">Benefits:</p>
            <ul>
              <li>A highly competitive compensation package </li>
              <li>Medical benefits (with cover for family) </li>
              <li>Opportunity to work remotely </li>
              <li>An international and diverse work atmosphere working with large clients in multiple geographies </li>
            </ul>
            <p class="mt-2 lh-lg font-size-p">Equal Opportunity Employer: </p>
            <p>At Xitricon we are building a company where a diverse mix of talented people want to come, stay, and do their best work. We know that our company runs on the hard work and the dedication of our passionate and creative employees.</p>

            <p>If you are interested in this opportunity, please submit your CV to <a href="mailto:shireen.anis@xitricon.com">shireen.anis@xitricon.com</a>
            </p>

            </div>

</div>