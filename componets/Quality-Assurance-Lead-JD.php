<div class="container py-5 px-4">
    <div class="col-12">
        <h1 class="blog-header" font="bold">Quality Assurance Lead</h1>
    </div>

    <!--div style="padding-top:20px; padding-bottom:10px">
        <img class="text-center about-banner" src="assets/img/blog-cover-img3.png" width="100%">
    </div-->

    <div class="col-12 py-3">
        <p class="mt-2 lh-lg font-size-p">Job Summary:</p>
        <p>We are seeking a highly skilled Quality Assurance (QA) Lead to manage and execute the quality assurance process for our software product development project. The successful candidate will work closely with the development team, product owners, and project manager to ensure the software products and solutions meet the business requirements and are of high quality. The QA Lead will be responsible for designing and implementing testing strategies, conducting tests, and documenting defects and issues found during the testing process. 
        </p>
        <p class="mt-2 lh-lg font-size-p">Key Responsibilities:</p>

         <!--  a href="https://connect.ifs.com/uki/agenda/"--><!--button class="btn2 btn-default main-bg-color" type="submit">Click to Register</button></a-->
         <ul>
  <li>Design and implement the testing strategy and quality assurance process for the software product development project. </li>
<li>Develop and execute test plans, test cases, and test scripts to ensure that software products and solutions meet the business requirements and are of high quality. </li>
<li>Work with the development team, product owners, and project manager to prioritize testing activities and identify risks and issues related to quality. </li>
<li>Document defects and issues found during testing and work with the development team to resolve them. </li>
<li>Analyze test results and provide recommendations to improve the quality of software products and solutions. </li>
<li>Communicate testing progress, test results, and other relevant information to stakeholders and project team members. </li>
<li>Mentor and train other QA team members to improve their skills and knowledge. </li>

</ul> 
   
 <p class="mt-2 lh-lg font-size-p">Requirements:</p>
<ul>
<li>Bachelor's degree in computer science or related field. </li>
<li>At least 5 years of experience in software quality assurance, with a focus on Agile software development. </li>
<li>Strong knowledge of software testing methodologies, tools, and techniques. </li>
<li>Experience in designing and implementing testing strategies and quality assurance processes for complex software products and solutions. </li>
<li>Experience in creating and executing test plans, test cases, and test scripts. </li>
<li>Experience in analyzing test results and providing recommendations for improving software quality. </li>
<li>Excellent problem-solving and analytical skills. </li>
<li>Excellent communication and interpersonal skills. </li>
<li>Ability to work independently and in a team environment. </li>
<li>Experience with Agile software development methodologies. </li>

 
</ul>
 <p class="mt-2 lh-lg font-size-p">Desired Skills: </p>
<ul>
  <li>Experience in testing cloud-based software products and solutions. </li>
<li>Experience with test automation tools such as Selenium, JMeter, or similar. </li>
<li>Familiarity with performance testing and security testing. </li>
<li>Experience with DevOps practices and tools such as continuous integration and continuous deployment. </li>
<li>Familiarity with containerization technologies such as Docker and Kubernetes. </li>
<li>Familiarity with test-driven development (TDD) and behavior-driven development (BDD). </li>

</ul>
<p class="mt-2 lh-lg font-size-p">Company Culture:</p>
        <p>Xitricon is a consulting, technology, and digital transformation services company, headquartered in Dubai, UAE. <br>

If you have the experience and skills required for this position and are excited about the prospect of leading a hybrid agile software development project, we encourage you to apply. We offer competitive compensation packages, flexible work arrangements, and opportunities for career growth and development.
        </p>
<p class="mt-2 lh-lg font-size-p">Benefits:</p>
<ul>
  <li>A highly competitive compensation package </li>
  <li>Medical benefits (with cover for family) </li>
  <li>Opportunity to work remotely </li>
  <li>An international and diverse work atmosphere working with large clients in multiple geographies </li>
</ul>
<p class="mt-2 lh-lg font-size-p">Equal Opportunity Employer: </p>
<p>At Xitricon we are building a company where a diverse mix of talented people want to come, stay, and do their best work. We know that our company runs on the hard work and the dedication of our passionate and creative employees.</p>

<p>If you are interested in this opportunity, please submit your CV to <a href="mailto:shireen.anis@xitricon.com">shireen.anis@xitricon.com</a></p>

        <!-- quote form start here 
                            <section class="quote-form" style="background-image: none;" data-scroll-index="1">
                                <h2 class="form-heading text-center text-uppercase">Enquiry</h2>
                                <span class="form-title text-center"></span>
                                <form id="contactForm" method="post" action="submit_form.php"data-toggle="validator">
                                    <fieldset>
                                        <div class="form-group">
                                            <input type="text" id="name" placeholder="Your Name" class="form-control" required data-error="NEW ERROR MESSAGE">
                                        </div>
                                        <div class="form-group">
                                            <input type="email" id="email" placeholder="Your Email" class="form-control" required data-error="NEW ERROR MESSAGE">
                                        </div>
                                        <div class="form-group">
                                            <input type="tel" id="phone" placeholder="Phone Number" class="form-control" required data-error="NEW ERROR MESSAGE">
                                        </div>
                                        <div id="msgSubmit" class="form-message hidden"></div>
                                        <span class="info"><i class="fa fa-info-circle main-color" aria-hidden="true"></i> We will never share your personal info</span>
                                        <button class="btn2 btn-default main-bg-color" type="submit" id="form-submit">Submit</button>
                                    </fieldset>
                                </form>
                            </section>
                          quote form end here -->

        <!--div class="col-12">
            <img class="text-center about-banner" src="IFS/Social Image Xitricon_1200X630.jpg" width="50%" style="padding-right: 10px;">
        </div-->
        <!--div class="col-4">
            <img class="text-center about-banner" src="assets/img/blog-sub5.png" width="100%" style="padding-right: 10px;">
        </div>
        <div class="col-4">
            <img class="text-center about-banner" src="assets/img/blog-sub6.png" width="100%" style="padding-right: 10px;">
        </div-->
    </div>
</div>