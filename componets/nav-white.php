<div id="navbar">
    <header class="d-flex align-items-center justify-content-center justify-content-md-between py-3">
        <a href="index.php" class="d-flex align-items-center col-md-3 mb-2 mb-md-0 text-dark text-decoration-none">
            <img class="d-block" src="assets/img/logo-w.svg" alt="" width="213" height="102">
        </a>

        <ul class="menu-mobile nav col-12 col-md-auto mb-2 justify-content-center mb-md-0">
            <li><a href="index.php" class="nav-link px-4">Home</a></li>
            <li><a href="about-us.php" class="nav-link px-4 ">About</a></li>
            <li><a href="service.php" class="nav-link px-4">Services</a></li>
            <li><a href="navulia.php" class="nav-link px-4">Navulia E-procurement</a></li>
            <li><a href="contact.php" class="nav-link px-4">Contact Us</a></li>
            <!-- <li><a href="blog.php" class="nav-link px-4">Blogs</a></li> -->
        </ul>

        <div class="col-md-3 ttext-center">
            <!-- <button type="button" class="btn btn-custom" value="Schedule a call" data-bs-toggle="modal"
                data-bs-target="#modal1">Schedule
                call<img class="px-2" src="assets/img/arrow-right.svg" /></button> -->
        </div>
    </header>
</div>