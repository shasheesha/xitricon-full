<div class="container py-5 px-4">
    <div class="col-12">
        <h1 class="blog-header" font="bold">Experience your Future at IFS Connect </h1>
    </div>

    <div style="padding-top:20px; padding-bottom:10px">
        <img class="text-center about-banner" src="assets/img/blog-cover-img3.png" width="100%">
    </div>

    <div class="col-12 py-3">
        <p class="mt-2 lh-lg font-size-p">Date: Tuesday 16, May<br>
Time: 8:00 - 18:00<br>
Location: Millennium Point, Curzon St, Birmingham B4 7XG
</p>
        <p class="mt-2 lh-lg font-size-p">Get ready to mark your calendar for an exclusive event you don't want to miss! Join us for a day of networking with like-minded professionals, engaging discussions, and face-to-face interactions. Meet the Xitricon team at our booth  to discuss how we can help you navigate your digital transformation journey.
        </p>
        <p class="mt-2 lh-lg font-size-p"> With a plethora of dynamic keynote speeches, lively panel discussions, and an array of breakout sessions to choose from, the second edition of IFS Connect is set to be an electrifying experience. So be sure to secure your spot by registering now! Don't miss out on this exciting chance to learn, connect, and grow your business.</p>
         <a href="https://connect.ifs.com/uki/agenda/"><button class="btn2 btn-default main-bg-color" type="submit">Click to Register</button></a> 
                <!--p class="mt-2 lh-lg font-size-p"> Have any additional questions regarding the event? Fill out the form and our team will get back you.</p-->
    </div>

    <div class="text-center d-flex justify-content-between">
        <!-- quote form start here 
                            <section class="quote-form" style="background-image: none;" data-scroll-index="1">
                                <h2 class="form-heading text-center text-uppercase">Enquiry</h2>
                                <span class="form-title text-center"></span>
                                <form id="contactForm" method="post" action="submit_form.php"data-toggle="validator">
                                    <fieldset>
                                        <div class="form-group">
                                            <input type="text" id="name" placeholder="Your Name" class="form-control" required data-error="NEW ERROR MESSAGE">
                                        </div>
                                        <div class="form-group">
                                            <input type="email" id="email" placeholder="Your Email" class="form-control" required data-error="NEW ERROR MESSAGE">
                                        </div>
                                        <div class="form-group">
                                            <input type="tel" id="phone" placeholder="Phone Number" class="form-control" required data-error="NEW ERROR MESSAGE">
                                        </div>
                                        <div id="msgSubmit" class="form-message hidden"></div>
                                        <span class="info"><i class="fa fa-info-circle main-color" aria-hidden="true"></i> We will never share your personal info</span>
                                        <button class="btn2 btn-default main-bg-color" type="submit" id="form-submit">Submit</button>
                                    </fieldset>
                                </form>
                            </section>
                          quote form end here -->

        <div class="col-12">
            <img class="text-center about-banner" src="IFS/Social Image Xitricon_1200X630.jpg" width="50%" style="padding-right: 10px;">
        </div>
        <!--div class="col-4">
            <img class="text-center about-banner" src="assets/img/blog-sub5.png" width="100%" style="padding-right: 10px;">
        </div>
        <div class="col-4">
            <img class="text-center about-banner" src="assets/img/blog-sub6.png" width="100%" style="padding-right: 10px;">
        </div-->
    </div>
</div>