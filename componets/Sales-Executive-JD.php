<div class="container py-5 px-4">
    <div class="col-12">
        <h1 class="blog-header" font="bold">Sales Executive</h1>
    </div>

    <!--div style="padding-top:20px; padding-bottom:10px">
        <img class="text-center about-banner" src="assets/img/blog-cover-img3.png" width="100%">
    </div-->

    <div class="col-12 py-3">
        <p class="mt-2 lh-lg font-size-p">Location: Saudi Arabia</p>
        <p class="mt-2 lh-lg font-size-p">About The Role:</p>
        <p>Xitricon is a consulting, technology and digital transformation services company, headquartered in Dubai, UAE.
        <br>
      We are actively looking for a Sales Executive based in Saudi Arabia, whose primary responsibilities will include generating leads, making preliminary calls, handling inquiries, engaging in proactive selling, initiating email and calling campaigns, and building and managing the sales pipeline. The person needs to connect with sales leads in the Middle East and arrange meetings, as well as identify prospective clients and customers in the Middle East market. The candidate must be able to effectively understand and describe Business Application solutions.</p>

        <!--p class="mt-2 lh-lg font-size-p">Key Responsibilities:</p>

           a href="https://connect.ifs.com/uki/agenda/"--><!--button class="btn2 btn-default main-bg-color" type="submit">Click to Register</button></a
         <ul>
            <li>Generate leads for the sales team through various channels and strategies.</li>
            <li>Make preliminary calls to potential clients to introduce the company's products and services.</li>
            <li>Handle inquiries from prospective customers, providing them with information and addressing their concerns.</li>
            <li>Engage in proactive selling by reaching out to potential clients and pitching the company's offerings.</li>
            <li>Initiate email and calling campaigns to nurture leads and convert them into sales opportunities.</li>
            <li>Build and manage the sales pipeline, ensuring a steady flow of potential customers.</li>
            <li>Connect with sales leads in the Middle East region and arrange meetings to discuss business opportunities.</li>
            <li>Identify and research prospective clients and customers in the Middle East market.</li>
            <li>Understand and effectively describe Business Application solutions to potential clients.</li>
        </ul-->
            <p class="mt-2 lh-lg font-size-p">Required Qualifications:</p>
        <ul>
            <li>Bachelor's or master's degree or other relevant professional qualification.</li>
            <li>ERP Sales experience mandatory.</li>
            <li>Minimum of 2-3 years' experience in Business Applications Sales or Business Development activities.</li>
            <li>Fundamental understanding of Business Applications.</li>
            <li>Excellent communication skills, both verbal and written.</li>
            <li>Competent computer skills to perform essential functions listed above (Word, Excel, PowerPoint).</li>
            <li>Well-developed presentation skills.</li>
            <li>Willingness to travel abroad.</li>

        </ul>

            
            <p class="mt-2 lh-lg font-size-p">Equal Opportunity Employer: </p>
            <p>At Xitricon we are building a company where a diverse mix of talented people want to come, stay, and do their best work. We know that our company runs on the hard work and the dedication of our passionate and creative employees.</p>

            <p>If you are interested in this opportunity, please submit your CV to <a href="mailto:shireen.anis@xitricon.com">shireen.anis@xitricon.com</a></p>

        
    </div>
</div>