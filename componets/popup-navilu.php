<!-- Modal -->
<div class="modal modal-background-fade" id="modal2" tabindex="-1" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">

            <div class="col popup-padding px-5">

                <form method="post" id="mail_form">
                    <input type="hidden" name="navilu" value="1">
                    <input type="text" class="form-control my-2" id="fullname" name="name" placeholder="Name"
                        required />

                    <div class="row">
                        <div class="col-6">
                            <input type="email" class="form-control my-2" id="email" name="email" placeholder="Email" />
                        </div>
                        <div class="col-6">
                            <?php include 'componets/country-pickup.php'?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-6">
                            <input type="text" class="form-control my-2" id="company" name="company"
                                placeholder="Company Name" />
                        </div>
                        <div class="col-6">
                            <input type="text" class="form-control my-2" id="designation" name="designation"
                                placeholder="Designation" />
                        </div>
                    </div>

                    <div id="captcha"></div>

                    <button id="sendmail" type="submit" class="btn btn-primary btn-style">
                        Send Email
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- PopUp end-->
<!-- <script>
     function phoneBtn() {
         window.location.href = "callto:+9477378829";
     }

     function whtsappBtn() {
         window.location.href = "//api.whatsapp.com/send?phone=+9477378829&text=Xitricon Web";
     }

     function emailBtn() {
         window.location.href = "mailto:info@xitricon.com";
     }
 </script> -->


<!-- Latest compiled and minified JavaScript -->
<!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script> -->