<div class="container d-flex social-icons flex-column align-items-end list-unstyled ">
<p class="mb-2 mobile-follow-txt">Follow on</p>
        <ul class="nav col-md-4 list-unstyled d-flex social-icons">
        
                <li class="ms-3"><a class="text-muted" href="https://www.linkedin.com/company/xitricon-private-limited/">
                                <img src="assets/img/linkedin.svg"></a>
                </li>

                <li class="ms-3"><a class="text-muted" href="https://www.facebook.com/xitricon">
                                <img src="assets/img/fb.svg"></a>
                </li>
                <li class="ms-3"><a class="text-muted" href="https://twitter.com/xitricon">
                                <img src="assets/img/twitter.svg"></a>
                </li>
                <li class="ms-3"><a class="text-muted" href="https://www.instagram.com/xitricon/">
                                <img src="assets/img/instagram.svg"></a>
                </li>
        </ul>
</div>