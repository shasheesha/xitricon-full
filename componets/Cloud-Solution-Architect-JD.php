<div class="container py-5 px-4">
    <div class="col-12">
        <h1 class="blog-header" font="bold">Software Architect</h1>
    </div>

    <!--div style="padding-top:20px; padding-bottom:10px">
        <img class="text-center about-banner" src="assets/img/blog-cover-img3.png" width="100%">
    </div-->

    <div class="col-12 py-3">
        <p class="mt-2 lh-lg font-size-p">Job Summary: </p>
        <p>We are seeking a highly skilled software architect to design and develop a scalable, reliable, and high-performance cloud-based software product and solutions. The successful candidate will work closely with the senior stakeholders, product owners, and development teams to understand business requirements and design software products that meet those requirements. The software architect will also be responsible for evaluating innovative technologies and recommending those that can improve the development process and enhance the quality of software products and solutions. 

        </p>
        <p class="mt-2 lh-lg font-size-p">Key Responsibilities:</p>
       <ul>
          <li>Collaborate with stakeholders, product owners, and development teams to understand business requirements and design software products that meet those requirements. </li>
        <li>Develop technical architectures, design patterns, and best practices to guide the development team in building high-quality software systems. </li>
        <li>Ensure that software products and solutions are scalable, maintainable, and reliable, and meet performance and security requirements. </li>
        <li>Evaluate innovative technologies, tools, and frameworks, and recommend those that can improve the development process and enhance the quality of software products and solutions. </li>
        <li>Participate in design reviews, code reviews, and provide feedback to developers to ensure that software products and solutions adhere to architectural standards and best practices. </li>
        <li>Provide technical leadership and mentorship to the development team, and work with team members to improve their technical skills and knowledge. 
        </li>
        </ul> 
   
 <p class="mt-2 lh-lg font-size-p">Required Qualifications:  </p>
        <ul>
          <li>MSc or BSc degree in computer science or related field. </li>
        <li>At least 3 years of experience in software architecture and design in enterprise applications. </li>
        <li>Strong understanding of software architecture principles, design patterns, and best practices. </li>
        <li>Experience in designing and developing scalable, reliable, and high-performance software systems. </li>
        <li>Experience in cloud-based products and solutions development. </li>
        <li>Good comprehension in SaaS solutions. </li>
        <li>Competence in Agile software development. </li>
        <li>Excellent analytical and problem-solving skills. </li>
        <li>Excellent communication and interpersonal skills. </li>
        <li>Ability to work independently and in a team environment. </li>
        <li>Experience with agile methodologies and software development lifecycle. </li>
        <li>Experience in leading and mentoring development teams. </li>
        </ul>

        <p class="mt-2 lh-lg font-size-p">Desired Skills:</p>
        <ul>
        <li>Experience with cloud-based architecture and deployment (preferably on AWS). </li>
        <li>Good Knowledge of microservices architecture and containerization (Docker, Kubernetes). </li>
        <li>Good knowledge of data storage strategies such as non-relational and relational. </li>
        <li>Good knowledge of microservices frameworks such as Spring Boot. </li>
        <li>Good knowledge of UI frameworks such as Angular 2+ and Flutter. </li>
        <li>Good knowledge of authentication and authorization frameworks, and API management. </li>
        <li>Good knowledge of enterprise system integration and RESTful webservices including the use of API documentation tools such as Swagger. </li>
        <li>Good knowledge of Agile style user story documentation in Gherkin style.  </li>
        <li>Experience with DevOps tools and practices (preferably on AWS). </li>
        <li>Familiarity with machine learning and artificial intelligence technologies. </li>
        <li>Familiarity with ERP systems and Procurement systems.</li>
        </ul> 
<p class="mt-2 lh-lg font-size-p">Company Culture:</p>
        <p>Xitricon is a consulting, technology, and digital transformation services company, headquartered in Dubai, UAE. <br>

If you have the experience and skills required for this position and are excited about the prospect of leading a hybrid agile software development project, we encourage you to apply. We offer competitive compensation packages, flexible work arrangements, and opportunities for career growth and development.
        </p>
<p class="mt-2 lh-lg font-size-p">Benefits:</p>
<ul>
  <li>A highly competitive compensation package </li>
  <li>Medical benefits (with cover for family) </li>
  <li>Opportunity to work remotely </li>
  <li>An international and diverse work atmosphere working with large clients in multiple geographies </li>
</ul>
<p class="mt-2 lh-lg font-size-p">Equal Opportunity Employer: </p>
<p>At Xitricon we’re building a company where a diverse mix of talented people want to come, stay, and do their best work. We know that our company runs on the hard work and the dedication of our passionate and creative employees.</p>
<p>If you are interested in this opportunity, please submit your CV to <a href="mailto:shireen.anis@xitricon.com">shireen.anis@xitricon.com</a></p>
     
    </div>
</div>