<div class="container">
    <div class="container">
        <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-md-between py-3 mb-4">
            <a href="index.php" class="mb-3 me-2 mb-md-0 text-muted text-decoration-none lh-1">
                <img src="assets/img/logo-b.svg">
            </a>
            <ul class="menu-mobile nav col-12 col-md-auto mb-2 justify-content-center mb-md-0">
                <li><a href="index.php" class="nav-link px-4 bt-nav-txt">Home</a></li>
                <li><a href="about-us.php" class="nav-link px-4 bt-nav-txt">About</a></li>
                <li><a href="service.php" class="nav-link px-4 bt-nav-txt">Services</a></li>
                <li><a href="navulia.php" class="nav-link px-4 bt-nav-txt">Navulia E-Procurement</a></li>
                <li><a href="contact.php" class="nav-link px-4 bt-nav-txt">Contact Us</a></li>
                <!-- <li><a href="blog.php" class="nav-link px-4 bt-nav-txt">Blogs</a></li> -->
            </ul>
        </div>
    </div>

    <footer
        class="d-flex flex-wrap align-items-center justify-content-center justify-content-md-between py-3 mb-4 border-top">
        <div class="col-md-6 col-xs-12">
            <ul class="menu-mobile nav col-12 col-md-auto mb-2">
                <li><a href="policy.php" class="nav-link px-4 bt-nav-txt">Privacy Policy</a></li>
                <li><a href="termsConditions.php" class="nav-link px-4 bt-nav-txt">Terms & Conditions</a></li>
                <li><a href="gdpr.php" class="nav-link px-4 bt-nav-txt">GDPR Compliance</a></li>
            </ul>
        </div>
        <div class="col-md-6 col-xs-12">
            <?php include 'social-icons.php' ?>
        </div>
    </footer>
</div>


<div style="text-align: center;
    padding: 12px;
    letter-spacing: 1px;
    font-size:10px;
    background-image: linear-gradient(to right, #152c4f, #174c9c);
    color: white;">© ALL RIGHTS RESERVED 2023 XITRICON (PRIVATE) LIMITED</div>