<div class="desktop-sec-view fade-in ">
    <div class="col text-center d-flex py-4">
        <div class="header-small-logo px-4" style="width: 320px;">
            <div class="col d-flex align-items-center justify-content-center"><img src="assets/img/ifs.svg"></div>
            <p class="header-small-logo-txt">IFS CUSTOMER CHOICE PARTNER</p>
            <p class="header-small-logo-txt-sub ">OF THE YEAR 2022</p>
        </div>
        <div class="icon-line1 "></div>
        <div class="header-small-logo px-4" style="width: 320px;">
            <div class="col d-flex align-items-center justify-content-center"><img src="assets/img/boomi.svg"></div>
            <p class="header-small-logo-txt">EMERGING MARKETS PARTNER</p>
        <p class="header-small-logo-txt-sub ">OF THE YEAR 2022</p>
        </div>
    </div>
</div>

<div class="mobile-sec-view">
    <div class="header-small-logo">
        <div class="col d-flex align-items-center justify-content-center"><img src="assets/img/ifs.svg"></div>
        <p class="header-small-logo-txt">IFS CUSTOMER CHOICE PARTNER</p>
            <p class="header-small-logo-txt-sub ">OF THE YEAR 2022</p>
    </div>
    <div class="header-small-logo">
        <div class="col d-flex align-items-center justify-content-center"><img src="assets/img/boomi.svg"></div>
        <p class="header-small-logo-txt">EMERGING MARKETS PARTNER</p>
        <p class="header-small-logo-txt-sub ">OF THE YEAR 2022</p>
    </div>
</div>