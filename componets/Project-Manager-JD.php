<div class="container py-5 px-4">
    <div class="col-12">
        <h1 class="blog-header" font="bold">Project Manager </h1>
    </div>

    <!--div style="padding-top:20px; padding-bottom:10px">
        <img class="text-center about-banner" src="assets/img/blog-cover-img3.png" width="100%">
    </div-->

    <div class="col-12 py-3">
        <p class="mt-2 lh-lg font-size-p">Job Summary:</p>
        <p>We are seeking an experienced project manager to lead a hybrid agile software development project. The successful candidate will be responsible for overseeing all aspects of the project, from planning and design to implementation and delivery. The ideal candidate will have experience in managing complex software development projects using a hybrid agile approach. 
        </p>
        <p class="mt-2 lh-lg font-size-p">Key Responsibilities:</p>

         <!c`   a href="https://connect.ifs.com/uki/agenda/"--><!--button class="btn2 btn-default main-bg-color" type="submit">Click to Register</button></a-->
         <ul>
  <li>Work closely with the Executive Consultant Product Development and the Product Owner to understand the project management philosophy and business objectives.</li>
  <li>Define project scope, goals, and deliverables that support business objectives in collaboration with senior management and stakeholders. </li>
  <li>Develop a detailed project plan that outlines tasks, timelines, and resources required to complete the project on time and within budget following the recommended hybrid agile process. </li>
  <li>Establish and maintain project budgets, forecasts, and metrics to track progress and ensure that the project is on track. </li>
  <li>Monitor project progress and adjust plans as necessary to ensure successful delivery of project objectives. </li>
  <li>Work closely with the development team to ensure that all project requirements are properly understood and documented. </li>
  <li>Manage project risks and issues, developing and implementing contingency plans as required. </li>
  <li>Communicate project status and progress to stakeholders, including senior management, on a regular basis. </li>
  <li>Ensure that project documentation is complete and accurate, including project plans, reports, and change requests. </li>
  <li>Mentor and coach team members, encouraging a collaborative and supportive team environment. </li>
</ul> 
   
 <p class="mt-2 lh-lg font-size-p">Requirements: </p>
<ul>
  <li>Bachelor's degree in computer science, engineering, or related field. </li>
  <li>Minimum of 5 years of experience in project management in a software development environment, with at least 2 years of experience in a hybrid agile environment. </li>
  <li>Experience in software development methodologies, including hybrid agile, waterfall, and Scrum. </li>
  <li>Knowledge of project management tools, such as ClickUp, Jira, Asana, or Trello. </li>
  <li>Strong leadership and communication skills, with the ability to influence and motivate team members. </li>
  <li>Ability to work independently and manage multiple tasks simultaneously. </li>
  <li>Experience managing remote teams and distributed projects is a plus. </li>
</ul>
<p class="mt-2 lh-lg font-size-p">Company Culture:</p>
        <p>Xitricon is a consulting, technology, and digital transformation services company, headquartered in Dubai, UAE. <br>

If you have the experience and skills required for this position and are excited about the prospect of leading a hybrid agile software development project, we encourage you to apply. We offer competitive compensation packages, flexible work arrangements, and opportunities for career growth and development.
        </p>
<p class="mt-2 lh-lg font-size-p">Benefits:</p>
<ul>
  <li>A highly competitive compensation package </li>
  <li>Medical benefits (with cover for family) </li>
  <li>Opportunity to work remotely </li>
  <li>An international and diverse work atmosphere working with large clients in multiple geographies </li>
</ul>
<p class="mt-2 lh-lg font-size-p">Equal Opportunity Employer: </p>
<p>At Xitricon we are building a company where a diverse mix of talented people want to come, stay, and do their best work. We know that our company runs on the hard work and the dedication of our passionate and creative employees.</p>
<p>If you are interested in this opportunity, please submit your CV to <a href="mailto:shireen.anis@xitricon.com">shireen.anis@xitricon.com</a></p>

        <!-- quote form start here 
                            <section class="quote-form" style="background-image: none;" data-scroll-index="1">
                                <h2 class="form-heading text-center text-uppercase">Enquiry</h2>
                                <span class="form-title text-center"></span>
                                <form id="contactForm" method="post" action="submit_form.php"data-toggle="validator">
                                    <fieldset>
                                        <div class="form-group">
                                            <input type="text" id="name" placeholder="Your Name" class="form-control" required data-error="NEW ERROR MESSAGE">
                                        </div>
                                        <div class="form-group">
                                            <input type="email" id="email" placeholder="Your Email" class="form-control" required data-error="NEW ERROR MESSAGE">
                                        </div>
                                        <div class="form-group">
                                            <input type="tel" id="phone" placeholder="Phone Number" class="form-control" required data-error="NEW ERROR MESSAGE">
                                        </div>
                                        <div id="msgSubmit" class="form-message hidden"></div>
                                        <span class="info"><i class="fa fa-info-circle main-color" aria-hidden="true"></i> We will never share your personal info</span>
                                        <button class="btn2 btn-default main-bg-color" type="submit" id="form-submit">Submit</button>
                                    </fieldset>
                                </form>
                            </section>
                          quote form end here -->

        <!--div class="col-12">
            <img class="text-center about-banner" src="IFS/Social Image Xitricon_1200X630.jpg" width="50%" style="padding-right: 10px;">
        </div-->
        <!--div class="col-4">
            <img class="text-center about-banner" src="assets/img/blog-sub5.png" width="100%" style="padding-right: 10px;">
        </div>
        <div class="col-4">
            <img class="text-center about-banner" src="assets/img/blog-sub6.png" width="100%" style="padding-right: 10px;">
        </div-->
    </div>
</div>