<div class="container py-5 px-4">
    <div class="col-12">
        <h1 class="blog-header">News & Updates</h1>
    </div>

    <div class="col-12 py-5 event-container cursor-style" onclick="buttonClickBlog2()">
        <div class="col event">
            <div class="d-flex blog-dt" style="align-items: center; margin-bottom:20px">
                <div style="background-color: #EBF2FE; padding: 8px; border-radius: 6px 0px 0px 6px;">
                    <span style="font-weight: bold;">EVENT</span>
                </div>
                <div style="padding:8px; text-transform:uppercase;">09th February, 2023</div>
            </div>
            <div class="blog-title">
                <p style="color:#232E52; font-size:28px; font-weight: bold;">Inspiring Future Careers at the University of Moratuwa </p>
            </div>
        </div>
        <div class="col">
            <div class="text-end blog-img-coloum">
                <div class="col">
                    <img class="text-center blog-img" src="assets/img/thum3.png">
                </div>
                <div class="col">
                    <img class="text-center blog-img" src="assets/img/thum2.png">
                </div>
            </div>
        </div>
    </div>

    <div class="col-12 py-5 cursor-style event-container" onclick="buttonClickBlog1()">
        <div class="col event ">
            <div class="d-flex blog-dt" style="align-items: center; margin-bottom:20px">
                <div style="background-color: #EBF2FE; padding: 8px; border-radius: 6px 0px 0px 6px;">
                    <span style="font-weight: bold;">EVENT</span>
                </div>
                <div style="padding:8px; text-transform:uppercase;">16th December, 2022</div>
            </div>
            <div class="blog-title">
                <p style="color:#232E52; font-size:28px; font-weight: bold;">Xitricon 2022 Awards Ceremony</p>
            </div>
        </div>
        <div class="col">
            <div class="text-center blog-img-coloum">
                <div class="col">
                    <img class="text-center blog-img" src="assets/img/thum1.png">
                </div>
                <div class="col">
                    <img class="text-center blog-img" src="assets/img/blog-sub2.png">
                </div>
            </div>
        </div>
    </div>

</div>



<script>
    function buttonClickBlog1() {
        window.location.href = "single-blog-1.php";
    }

    function buttonClickBlog2() {
        window.location.href = "single-blog-2.php";
    }
</script>