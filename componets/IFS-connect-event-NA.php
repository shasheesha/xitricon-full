<div class="container py-5 px-4">
    <div class="col-12">
        <h1 class="blog-header" font="bold">Welcome to IFS Connect North America 2023</h1>
    </div>

    <div style="padding-top:20px; padding-bottom:10px">
        <img class="text-center about-banner" src="assets/img/blog-cover-img4.png" width="100%">
    </div>
     <div style="padding-top:20px; padding-bottom:10px">
        <img class="text-center about-banner" src="assets/img/Banner_us.jpg" alt="Xitricon Sliver Sponsor" >
    </div>
    <div class="col-12 py-3">
        <p class="mt-2 lh-lg font-size-p">Date: 13rd-14th,June 2023<br>
                                    Location: US Bank Stadium, in Minneapolis, MN
            </p>
        <p class="mt-2 lh-lg font-size-p">We are thrilled to invite you for the IFS Connect, happening on June 13-14 at US Bank Stadium in Minneapolis, MN. This event is a one-of-a-kind opportunity for you to network with fellow professionals and interact with the Xitricon team.</p>
        <p class="mt-2 lh-lg font-size-p"> With a mix of keynote speeches, panel discussions, and a range of breakout sessions to choose from, this event promises to be an exciting and engaging experience. Don't let this opportunity slip by, secure your spot!</p>

        <p class="mt-2 lh-lg font-size-p">Visit us at the <span style="color:#ff8601"><strong>Xitricon booth</strong></span>  to grow your business.</p>
       
        <div class="container">
         <div class="col-12">
                        <h4 class="py-4" style="font-weight: bold;" id="demo">
                            Contact us now to book your appointment!
                        </h4></div>
                        <div>
                        <form method="post" id="mail_form2" class="col-4">
                            <input type="text" class="form-control my-2" id="fullname" name="name" placeholder="Name"
                                required />
                            <input type="email" class="form-control my-2" id="email" name="email" placeholder="Email" />
                             <input type="tel" class="form-control my-2" id="phone" name="phone" placeholder="Mobile phone" value="+44">
                            <?php include 'componets/country-pickup.php'?>
                            <textarea id="comment" class="form-control my-2" name="comment" rows="4"
                                placeholder="Comment" cols="50"></textarea>

                            <div id="captcha"></div>

                            <button type="submit" class="g-recaptcha btn btn-primary btn-style">
                                Send
                            </button>
                        </form></div>
                       <div class="col contact-icon">
                        <!--img class="text-center about-banner" src="IFS/Social Image Xitricon_1200X630.jpg" width="50%" style="padding-right: 10px;"-->
                    </div>
        </div>
         <!--a href="https://connect.ifs.com/uki/agenda/"><button class="btn2 btn-default main-bg-color" type="submit">Click to Register</button></a--> 
                <!--p class="mt-2 lh-lg font-size-p"> Have any additional questions regarding the event? Fill out the form and our team will get back you.</p-->
    </div>
         <!--a href="https://connect.ifs.com/na/"><button class="btn2 btn-default main-bg-color" type="submit">Click to Register</button></a--> 
                <!--p class="mt-2 lh-lg font-size-p"> Have any additional questions regarding the event? Fill out the form and our team will get back you.</p-->
    </div>

    <div class="text-center d-flex justify-content-between">
        <!-- quote form start here 
                            <section class="quote-form" style="background-image: none;" data-scroll-index="1">
                                <h2 class="form-heading text-center text-uppercase">Enquiry</h2>
                                <span class="form-title text-center"></span>
                                <form id="contactForm" method="post" action="submit_form.php"data-toggle="validator">
                                    <fieldset>
                                        <div class="form-group">
                                            <input type="text" id="name" placeholder="Your Name" class="form-control" required data-error="NEW ERROR MESSAGE">
                                        </div>
                                        <div class="form-group">
                                            <input type="email" id="email" placeholder="Your Email" class="form-control" required data-error="NEW ERROR MESSAGE">
                                        </div>
                                        <div class="form-group">
                                            <input type="tel" id="phone" placeholder="Phone Number" class="form-control" required data-error="NEW ERROR MESSAGE">
                                        </div>
                                        <div id="msgSubmit" class="form-message hidden"></div>
                                        <span class="info"><i class="fa fa-info-circle main-color" aria-hidden="true"></i> We will never share your personal info</span>
                                        <button class="btn2 btn-default main-bg-color" type="submit" id="form-submit">Submit</button>
                                    </fieldset>
                                </form>
                            </section>
                          quote form end here -->

        <!--<div class="col-12">
           
        </div>
        <div class="col-4">
            <img class="text-center about-banner" src="assets/img/blog-sub5.png" width="100%" style="padding-right: 10px;">
        </div>
        <div class="col-4">
            <img class="text-center about-banner" src="assets/img/blog-sub6.png" width="100%" style="padding-right: 10px;">
        </div-->
    </div>
</div>



