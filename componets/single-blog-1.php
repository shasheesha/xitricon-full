<div class="container py-5 px-4">
    <div class="col-12">
        <h1 class="blog-header">Xitricon 2022 Awards Ceremony</h1>
    </div>

    <div style="padding-top:20px; padding-bottom:10px">
        <img class="text-center about-banner" src="assets/img/blog-cover-img1.png" width="100%">
    </div>

    <div class="col-12 py-3">
        <p class="mt-2 lh-lg font-size-p">A glimpse of the Xitricon Excellence and Service Appreciation Awards Ceremony 2022, where all the topperformers were awarded and recognized for their hard work and commitment throughout the year. Xitricon – a leading consulting, technology, and digital transformation services company with offices in the UAE, Sri Lanka, the UK, India, and Canada, celebrated its Excellence and Service Appreciation Awards Night, in Movenpick Hotel, Colombo recently. Xitricon serves global companies in maritime operations,aerospace and defence, manufacturing, and service industries.</p>
        <p class="mt-2 lh-lg font-size-p">Xitricon’s top performers were recognized for their performance and excellence in several award categories, including Most Valuable Professional, Shining Star Award, Rookie Star Award, Project Excellence Award, and Service Appreciation Awards.The company claims to employ over 160 people. Xitricon employees are spread across many countries, with the majority of them working remotely. Nearly 40 employees were flown to Colombo from the UAE, India, and Pakistan to attend the gala awards ceremony.The evening was filled with much enthusiasm, camaraderie and celebrations. Congratulations to all the winners.</p>
    </div>

    <div class="text-center d-flex justify-content-between">
        <div class="col-4">
            <img class="text-center about-banner" src="assets/img/blog-sub1.png" width="100%" style="padding-right: 10px;">
        </div>
        <div class="col-4">
            <img class="text-center about-banner" src="assets/img/blog-sub2.png" width="100%" style="padding-right: 10px;">
        </div>
        <div class="col-4">
            <img class="text-center about-banner" src="assets/img/blog-sub3.png" width="100%" style="padding-right: 10px;">
        </div>
    </div>
</div>