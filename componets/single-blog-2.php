<div class="container py-5 px-4">
    <div class="col-12">
        <h1 class="blog-header">Inspiring Future Careers at the University of Moratuwa </h1>
    </div>

    <div style="padding-top:20px; padding-bottom:10px">
        <img class="text-center about-banner" src="assets/img/blog-cover-img2.png" width="100%">
    </div>

    <div class="col-12 py-3">
        <p class="mt-2 lh-lg font-size-p">On February 9th, 2023, the Xitricon team participated in the FIT Future Careers Day, which was organized by the Faculty of Information Technology at the University of Moratuwa in Sri Lanka. During the event, the team had the opportunity to meet with many bright and eager students and discuss the various career opportunities that exist in the tech industry, as well as the potential for growth and development within Xitricon.</p>
        <p class="mt-2 lh-lg font-size-p">The University of Moratuwa has a well-established reputation for producing highly skilled and sought-after IT graduates, and as such, students can feel confident that they are well-equipped with the necessary skills and knowledge to succeed in the technology sector. At Xitricon, we believe it is our responsibility to help cultivate and guide this young talent by offering them additional career opportunities and exposure to a global marketplace.
            Overall, the FIT Future Careers Day provided an excellent platform for us to connect with high-quality talent and to promote the many benefits of working at Xitricon.</p>
    </div>

    <div class="text-center d-flex justify-content-between">
        <div class="col-4">
            <img class="text-center about-banner" src="assets/img/blog-sub4.png" width="100%" style="padding-right: 10px;">
        </div>
        <div class="col-4">
            <img class="text-center about-banner" src="assets/img/blog-sub5.png" width="100%" style="padding-right: 10px;">
        </div>
        <div class="col-4">
            <img class="text-center about-banner" src="assets/img/blog-sub6.png" width="100%" style="padding-right: 10px;">
        </div>
    </div>
</div>