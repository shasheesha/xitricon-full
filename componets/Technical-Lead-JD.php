<div class="container py-5 px-4">
    <div class="col-12">
        <h1 class="blog-header" font="bold">Technical Lead</h1>
    </div>

    <!--div style="padding-top:20px; padding-bottom:10px">
        <img class="text-center about-banner" src="assets/img/blog-cover-img3.png" width="100%">
    </div-->

    <div class="col-12 py-3">
        <p class="mt-2 lh-lg font-size-p">Job Summary:</p>
        <p>We are seeking an experienced technical lead to oversee the technical aspects of a hybrid agile software development project. The successful candidate will be responsible for guiding the development team in building high-quality software systems that meet business requirements. The ideal candidate will have experience in managing software development projects using a hybrid agile approach and have a strong technical background in software development. 
        </p>
        <p class="mt-2 lh-lg font-size-p">Key Responsibilities:</p>

         <!--  a href="https://connect.ifs.com/uki/agenda/"--><!--button class="btn2 btn-default main-bg-color" type="submit">Click to Register</button></a-->
         <ul>
  <li>Lead and guide the development team in building high-quality software systems that meet business requirements.</li>
  <li>Ensure that software products and solutions are scalable, maintainable, and reliable, and meet performance and security requirements. </li>
  <li>Collaborate with stakeholders, product owners, and software architects to understand business requirements and design software products that meet those requirements. </li>
  <li>Participate in design reviews, code reviews, and provide feedback to developers to ensure that software products and solutions adhere to architectural standards and best practices.  </li>
  <li>Work closely with the project manager to establish project timelines, assign tasks, and ensure that the development team is meeting project goals and deadlines.  </li>
  <li>Identify and evaluate innovative technologies, tools, and frameworks, and recommend those that can improve the development process and enhance the quality of software products and solutions.  </li>
  <li>Provide technical leadership and mentorship to the development team, and work with team members to improve their technical skills and knowledge. </li>
  <li>Ensure that the development team adheres to agile methodologies and the software development lifecycle.  </li>
 
</ul> 
   
 <p class="mt-2 lh-lg font-size-p">Requirements: </p>
<ul>
  <li>Bachelor's degree in computer science, engineering, or related field.  </li>
  <li>Minimum of 5 years of experience in software development, with at least 2 years of experience in a technical leadership role. </li>
  <li>Strong technical background in software development, including experience with programming languages such as Java, Python, or JavaScript. </li>
  <li>Experience in software development methodologies, including hybrid agile, waterfall, and Scrum.  </li>
  <li>Good understanding of software architecture principles, design patterns, and best practices. </li>
  <li>Experience with cloud-based products and solutions development. </li>
  <li>Competence in Agile software development. </li>
  <li>Excellent analytical and problem-solving skills. </li>
  <li>Excellent communication and interpersonal skills. </li>
  <li>Ability to work independently and in a team environment. </li>
</ul>
 <p class="mt-2 lh-lg font-size-p">Desired Skills: </p>
<ul>
  <li>Experience with cloud-based architecture and deployment (preferably on AWS).   </li>
  <li>Good knowledge of microservices architecture and containerization (Docker, Kubernetes). </li>
  <li>Good knowledge of data storage strategies such as non-relational and relational. </li>
  <li>Good knowledge of microservices frameworks such as Spring Boot.  </li>
  <li>Good knowledge of UI frameworks such as Angular 2+ and Flutter.  </li>
  <li>Familiarity with machine learning and artificial intelligence technologies. </li>
  <li>Familiarity with ERP systems and Procurement systems.  </li>
  <li>Experience with DevOps tools and practices (preferably on AWS). </li>
</ul>
<p class="mt-2 lh-lg font-size-p">Company Culture:</p>
        <p>Xitricon is a consulting, technology, and digital transformation services company, headquartered in Dubai, UAE. <br>

If you have the experience and skills required for this position and are excited about the prospect of leading a hybrid agile software development project, we encourage you to apply. We offer competitive compensation packages, flexible work arrangements, and opportunities for career growth and development.
        </p>
<p class="mt-2 lh-lg font-size-p">Benefits:</p>
<ul>
  <li>A highly competitive compensation package </li>
  <li>Medical benefits (with cover for family) </li>
  <li>Opportunity to work remotely </li>
  <li>An international and diverse work atmosphere working with large clients in multiple geographies </li>
</ul>
<p class="mt-2 lh-lg font-size-p">Equal Opportunity Employer: </p>
<p>At Xitricon we are building a company where a diverse mix of talented people want to come, stay, and do their best work. We know that our company runs on the hard work and the dedication of our passionate and creative employees.</p>
<p>If you are interested in this opportunity, please submit your CV to <a href="mailto:shireen.anis@xitricon.com">shireen.anis@xitricon.com</a></p>

        <!-- quote form start here 
                            <section class="quote-form" style="background-image: none;" data-scroll-index="1">
                                <h2 class="form-heading text-center text-uppercase">Enquiry</h2>
                                <span class="form-title text-center"></span>
                                <form id="contactForm" method="post" action="submit_form.php"data-toggle="validator">
                                    <fieldset>
                                        <div class="form-group">
                                            <input type="text" id="name" placeholder="Your Name" class="form-control" required data-error="NEW ERROR MESSAGE">
                                        </div>
                                        <div class="form-group">
                                            <input type="email" id="email" placeholder="Your Email" class="form-control" required data-error="NEW ERROR MESSAGE">
                                        </div>
                                        <div class="form-group">
                                            <input type="tel" id="phone" placeholder="Phone Number" class="form-control" required data-error="NEW ERROR MESSAGE">
                                        </div>
                                        <div id="msgSubmit" class="form-message hidden"></div>
                                        <span class="info"><i class="fa fa-info-circle main-color" aria-hidden="true"></i> We will never share your personal info</span>
                                        <button class="btn2 btn-default main-bg-color" type="submit" id="form-submit">Submit</button>
                                    </fieldset>
                                </form>
                            </section>
                          quote form end here -->

        <!--div class="col-12">
            <img class="text-center about-banner" src="IFS/Social Image Xitricon_1200X630.jpg" width="50%" style="padding-right: 10px;">
        </div-->
        <!--div class="col-4">
            <img class="text-center about-banner" src="assets/img/blog-sub5.png" width="100%" style="padding-right: 10px;">
        </div>
        <div class="col-4">
            <img class="text-center about-banner" src="assets/img/blog-sub6.png" width="100%" style="padding-right: 10px;">
        </div-->
    </div>
</div>