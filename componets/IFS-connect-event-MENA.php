<div class="container py-5 px-4">
    <div class="col-12">
        <h1 class="blog-header" font="bold">Welcome to IFS Connect Middle East 2023</h1>
    </div>

    <div style="padding-top:20px; padding-bottom:10px">
        <img class="text-center about-banner" src="assets/img/blog-cover-img5.jpg" width="100%">
    </div>

      <div style="padding-top:20px; padding-bottom:10px">
        <img class="text-center about-banner" src="assets/img/Banner_us.jpg" alt="Xitricon Sliver Sponsor" >
    </div>

    <div class="col-12 py-3">
        <p class="mt-2 lh-lg font-size-p">Date: Thursday 08, June<br>
        Time: 09.00 to 17.30<br>
        Venue: Armani Hotel, Burj Khalifa, Dubai
        </p>
        <p class="mt-2 lh-lg font-size-p">Experience a day of tech discussions, networking, and hands-on experience at IFS Connect Dubai 2023. Discover insights and tools to foster agility, design for strength, build resilience, and tackle future challenges.
        </p>
        <p class="mt-2 lh-lg font-size-p"> Engage in inspiring presentations, networking, and knowledge sharing with industry partners and peers. Don't miss this invaluable experience!</p>

        <p class="mt-2 lh-lg font-size-p">Build agility, design for strength, and achieve resilience to future-proof your business.
        Meet the Xitricon team at our booth to discuss your digital transformation journey.
        </p>

        <p class="mt-2 lh-lg font-size-p">
        Join us at IFS Connect Dubai 2023, where knowledge meets opportunity, and relationships shape success. Secure your spot today!
        </p>

        <p class="mt-2 lh-lg font-size-p">Visit the <span style="color:#ff8601"><strong>Xitricon booth</strong></span> to network with our experts.
        </p>
       <div class="container">
         <div class="col-12">
                        <h4 class="py-4" style="font-weight: bold;" id="demo">
                            Contact us now to book your appointment!
                        </h4></div>
                        <div>
                        <form method="post" id="mail_form2" class="col-4">
                            <input type="text" class="form-control my-2" id="fullname" name="name" placeholder="Name"
                                required />
                            <input type="email" class="form-control my-2" id="email" name="email" placeholder="Email" />
                             <input type="tel" class="form-control my-2" id="phone" name="phone" placeholder="Mobile phone" value="+44">
                            <?php include 'componets/country-pickup.php'?>
                            <textarea id="comment" class="form-control my-2" name="comment" rows="4"
                                placeholder="Comment" cols="50"></textarea>

                            <div id="captcha"></div>

                            <button type="submit" class="g-recaptcha btn btn-primary btn-style">
                                Send
                            </button>
                        </form></div>
                       <div class="col contact-icon">
                        <!--img class="text-center about-banner" src="IFS/Social Image Xitricon_1200X630.jpg" width="50%" style="padding-right: 10px;"-->
                    </div>
        </div>
         <!--a href="https://connect.ifs.com/uki/agenda/"><button class="btn2 btn-default main-bg-color" type="submit">Click to Register</button></a--> 
                <!--p class="mt-2 lh-lg font-size-p"> Have any additional questions regarding the event? Fill out the form and our team will get back you.</p-->
    </div>

         <!--a href="https://connect.ifs.com/na/"><button class="btn2 btn-default main-bg-color" type="submit">Book a Meeting</button></a--> 
         
                <!--p class="mt-2 lh-lg font-size-p"> Have any additional questions regarding the event? Fill out the form and our team will get back you.</p-->
    </div>

    <div class="text-center d-flex justify-content-between">
        <!-- quote form start here 
                            <section class="quote-form" style="background-image: none;" data-scroll-index="1">
                                <h2 class="form-heading text-center text-uppercase">Enquiry</h2>
                                <span class="form-title text-center"></span>
                                <form id="contactForm" method="post" action="submit_form.php"data-toggle="validator">
                                    <fieldset>
                                        <div class="form-group">
                                            <input type="text" id="name" placeholder="Your Name" class="form-control" required data-error="NEW ERROR MESSAGE">
                                        </div>
                                        <div class="form-group">
                                            <input type="email" id="email" placeholder="Your Email" class="form-control" required data-error="NEW ERROR MESSAGE">
                                        </div>
                                        <div class="form-group">
                                            <input type="tel" id="phone" placeholder="Phone Number" class="form-control" required data-error="NEW ERROR MESSAGE">
                                        </div>
                                        <div id="msgSubmit" class="form-message hidden"></div>
                                        <span class="info"><i class="fa fa-info-circle main-color" aria-hidden="true"></i> We will never share your personal info</span>
                                        <button class="btn2 btn-default main-bg-color" type="submit" id="form-submit">Submit</button>
                                    </fieldset>
                                </form>
                            </section>
                          quote form end here -->

        <!--<div class="col-12">
           
        </div>
        <div class="col-4">
            <img class="text-center about-banner" src="assets/img/blog-sub5.png" width="100%" style="padding-right: 10px;">
        </div>
        <div class="col-4">
            <img class="text-center about-banner" src="assets/img/blog-sub6.png" width="100%" style="padding-right: 10px;">
        </div-->
    </div>
</div>



