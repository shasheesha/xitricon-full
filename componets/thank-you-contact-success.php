<!-- Modal -->
<div class="modal modal-background-fade" id="modalsuccess" tabindex="-1" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">

            <div class="col popup-padding px-5 text-center">

                <img src="assets/img/success.svg" alt="" class="mb-3 success-ico">

                <p> Thank you for taking the time to contact us! We will get back to you soon.
                </p>


            </div>
        </div>
    </div>
</div>