<div class="desktop-sec-view fade-in ">
    <div class="container">
        <div class="row align-items-md-stretch sec2-bg-2 sec-new">
            <div class="col">

                <img src="assets/img/pngegg.png" class="img-navu">

                <div class="justify-content-center">
                    <h3 class="py-4">Transform your Upstream
                        Procurement Organization</h3>
                    <p class="sec-new-txt">With Navulia eProcurement, you can realize
                        a scalable procurement solution that drives enterprise-wide growth through cost savings, risk reduction and better decision making.</p>
                </div>
            </div>

            <div class="col">
                <img src="assets/img/pc-icon.png" class="img-navu">
                <div class="justify-content-center">
                    <h3 class="py-4">Seamless Integration</h3>
                    <p class="sec-new-txt">Hassle-free integration with any ERP thus providing a faster turn around time in adapting and making the application your own.</p>
                </div>
            </div>

        </div>
    </div>
</div>
</div>

<!-- Mobile view -->
<div class="mobile-sec-view fade-in ">
    <div class="container">
        <div class="align-items-md-stretch sec2-bg-2 sec-new">
            <div class="col">
                <div style="
                    display: flex;
                    align-items: center;
                    justify-content: center;
                ">
                    <img src="assets/img/pngegg.png" class="img-navu">
                </div>
            </div>
            <div class="col img-section-new">
                <div class="justify-content-center padding-lr">
                    <h3>Transform your Upstream
                        Procurement Organization</h3>
                    <p class="sec-new-txt">With Navulia eProcurement, you can realize
                        a scalable procurement solution that drives enterprise-wide growth through cost savings, risk reduction and better decision making.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="container" style=" margin-bottom: 40px;">

        <div class="align-items-md-stretch sec2-bg-2 sec-new">
            <div class="col">
                <div style="
                    display: flex;
                    align-items: center;
                    justify-content: center;
                ">
                    <img src="assets/img/pc-icon.png" class="img-navu">
                </div>
            </div>
            <div class="col img-section-new">
                <div class="justify-content-center padding-lr">
                    <h3>Seamless Integration</h3>
                    <p class="sec-new-txt">Hassle-free integration with any ERP thus providing a faster turn around time in adapting and making the application your own.</p>
                </div>
            </div>

        </div>
    </div>
</div>