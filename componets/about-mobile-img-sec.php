<div class="desktop-sec-view fade-in ">
            <div class="container px-4 text-center d-flex justify-content-center">
                <div class="col icon-line"></div>
                <div class="col">
                    <div class="icon-1"></div>
                    <p>Aerospace &
                        Defence</p>
                </div>
                <div class="col icon-line"></div>
                <div class="col">
                    <div class="icon-2"></div>
                    <p>Energy, Utilities
                        & Resources</p>
                </div>
                <div class="col icon-line"></div>
                <div class="col">
                    <div class="icon-3"></div>
                    <p>Construction &
                        Engineering</p>
                </div>
                <div class="col icon-line"></div>
                <div class="col">
                    <div class="icon-4"></div>
                    <p>Service
                        Industries</p>
                </div>
                <div class="col icon-line"></div>
            </div>
            <div class="container px-4 py-5 text-center d-flex justify-content-center">
                <div class="col icon-line"></div>
                <div class="col">
                    <div class="icon-5"></div>
                    <p>Retail
                        Industry</p>
                </div>
                <div class="col icon-line"></div>
                <div class="col">
                    <div class="icon-6"></div>
                    <p>Manufacturing
                        Industry</p>
                </div>
                <div class="col icon-line"></div>
                <div class="col">
                    <div class="icon-7"></div>
                    <p>Telecommunications
                        Industry</p>
                </div>
                <div class="col icon-line"></div>
                <div class="col">
                    <div class="icon-8"></div>
                    <p>Banking,Finance
                        & Insurance</p>
                </div>
                <div class="col icon-line"></div>
            </div>

        </div>
        
<div class="mobile-sec-view fade-in ">
            <div class="container about-sec-icons px-4 text-center d-flex justify-content-center">
                <div class="col icon-line"></div>
                <div class="col about-sec-img-box">
                    <div class="icon-1"></div>
                    <p>Aerospace &
                        Defence</p>
                </div>
                <div class="col icon-line"></div>
                <div class="col about-sec-img-box">
                    <div class="icon-2"></div>
                    <p>Energy, Utilities
                        & Resources</p>
                </div>
                <div class="col icon-line"></div>
                <div class="col about-sec-img-box">
                    <div class="icon-3"></div>
                    <p>Construction &
                        Engineering</p>
                </div>
                <div class="col icon-line"></div>
                <div class="col about-sec-img-box">
                    <div class="icon-4"></div>
                    <p>Service
                        Industries</p>
                </div>
                <div class="col icon-line"></div>

                <div class="col about-sec-img-box">
                    <div class="icon-5"></div>
                    <p>Retail
                        Industry</p>
                </div>
                <div class="col icon-line"></div>
                <div class="col about-sec-img-box">
                    <div class="icon-6"></div>
                    <p>Manufacturing
                        Industry</p>
                </div>
                <div class="col icon-line"></div>
                <div class="col about-sec-img-box">
                    <div class="icon-7"></div>
                    <p>Communications
                        Industry</p>
                </div>
                <div class="col icon-line"></div>
                <div class="col about-sec-img-box">
                    <div class="icon-8"></div>
                    <p>Banking,Finance
                        & Insurance</p>
                </div>
                <div class="col icon-line"></div>

            </div>
        </div>