<div class="container py-5 px-4">
    <div class="col-12">
        <h1 class="blog-header" font="bold">Senior/Lead IFS Finance Consultant</h1>
    </div>

    <!--div style="padding-top:20px; padding-bottom:10px">
        <img class="text-center about-banner" src="assets/img/blog-cover-img3.png" width="100%">
    </div-->

    <div class="col-12 py-3">
        <p class="mt-2 lh-lg font-size-p">About The Role:</p>
        <p>Xitricon is a consulting, technology and digital transformation services company, headquartered in Dubai, UAE.
        <br>
        We are actively looking for a Senior/Lead IFS Finance Consultant with a strong background in Implementation, upgrades and Support for IFS Projects. In this role, you will be exposed to IFS latest versions like  Aurena ,Cloud and other emerging technologies. This role may require interaction with customers and within a team. Individual must have good communication, and execution skills.</p>

        <p class="mt-2 lh-lg font-size-p">Key Responsibilities:</p>

         <!--  a href="https://connect.ifs.com/uki/agenda/"--><!--button class="btn2 btn-default main-bg-color" type="submit">Click to Register</button></a-->
         <ul>
            <li>Engage in requirement gathering to fulfill customer and domain needs.</li>
            <li>Active communication with stakeholders to ensure the solution is fit for purpose.</li>
            <li>Applying the best engineering and architectural concepts to deliver high quality service.</li>
            <li>Adhering to IFS’ standards, guidelines, processes and tools.</li>
            <li>Ensure high quality deliverables through IFS' best practices.</li>
            </ul> 
               
             <p class="mt-2 lh-lg font-size-p">Requirements:</p>
            <ul>
            <li>Responsible for end to end Implementation , Upgrades Support Projects on IFS Applications</li>
            <li>Actively participate in all projects related activities.</li>
            <li>Interact with Technical team and help them in Reports generation and other technical related activities</li>
            <li>Responsible for skill development, internal and external team trainings</li>
            </ul>
             <p class="mt-2 lh-lg font-size-p">Skills Required:</p>
            <ul><li>5 to 8 years of total Experience with at least 2 years’ experience in IFS Finance and 2-3 end to end implementation experience.</li>
            <li>Must have proven IFS Applications Implementation, upgrades &Support Experience</li>
            <li>Expertise on Finance Module and Integration with Projects / Maintenance / Manufacturing / Supply Chain</li>
            <li>Good communication skills. Must be open to travelling.</li>
            <li>Preferred Qualification: Chartered Accountant / Cost Accountant or equivalent qualifications.</li>
            <li>Client Communication Skills Is a Must.</li>
            </ul>
            <p class="mt-2 lh-lg font-size-p">Benefits:</p>
            <ul>
              <li>A highly competitive compensation package </li>
              <li>Medical benefits (with cover for family) </li>
              <li>Opportunity to work remotely </li>
              <li>An international and diverse work atmosphere working with large clients in multiple geographies </li>
            </ul>
            <p class="mt-2 lh-lg font-size-p">Equal Opportunity Employer: </p>
            <p>At Xitricon we are building a company where a diverse mix of talented people want to come, stay, and do their best work. We know that our company runs on the hard work and the dedication of our passionate and creative employees.</p>

            <p>If you are interested in this opportunity, please submit your CV to <a href="mailto:shireen.anis@xitricon.com">shireen.anis@xitricon.com</a></p>

        <!-- quote form start here 
                            <section class="quote-form" style="background-image: none;" data-scroll-index="1">
                                <h2 class="form-heading text-center text-uppercase">Enquiry</h2>
                                <span class="form-title text-center"></span>
                                <form id="contactForm" method="post" action="submit_form.php"data-toggle="validator">
                                    <fieldset>
                                        <div class="form-group">
                                            <input type="text" id="name" placeholder="Your Name" class="form-control" required data-error="NEW ERROR MESSAGE">
                                        </div>
                                        <div class="form-group">
                                            <input type="email" id="email" placeholder="Your Email" class="form-control" required data-error="NEW ERROR MESSAGE">
                                        </div>
                                        <div class="form-group">
                                            <input type="tel" id="phone" placeholder="Phone Number" class="form-control" required data-error="NEW ERROR MESSAGE">
                                        </div>
                                        <div id="msgSubmit" class="form-message hidden"></div>
                                        <span class="info"><i class="fa fa-info-circle main-color" aria-hidden="true"></i> We will never share your personal info</span>
                                        <button class="btn2 btn-default main-bg-color" type="submit" id="form-submit">Submit</button>
                                    </fieldset>
                                </form>
                            </section>
                          quote form end here -->

        <!--div class="col-12">
            <img class="text-center about-banner" src="IFS/Social Image Xitricon_1200X630.jpg" width="50%" style="padding-right: 10px;">
        </div-->
        <!--div class="col-4">
            <img class="text-center about-banner" src="assets/img/blog-sub5.png" width="100%" style="padding-right: 10px;">
        </div>
        <div class="col-4">
            <img class="text-center about-banner" src="assets/img/blog-sub6.png" width="100%" style="padding-right: 10px;">
        </div-->
    </div>
</div>