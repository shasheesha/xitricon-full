<div class="container px-4 text-center d-flex justify-content-center">
                <div class="col-8">
                    <h1 class="pb-2 section-header-about">Accelerate Your Growth with Our Proven Services</h1>
                </div>
            </div>

<div class="container">
    <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3 text-center py-4 " style="align-items:center">
        <div class="col cursor-style">
            <!-- <h2 class="sec1-count-txt">160+</h2> -->
            <img src="assets/img/s1.svg">
            <p class="service-txt-main">Implementation</p>
        </div>
        <div class="col cursor-style">
            <img src="assets/img/s2.svg" style="padding:10px">
            <p class="service-txt-main mt-txt">Software Licensing</p>
            <p class="serive-txt-sub-w pNew">IFS, Boomi, Infor</p>
        </div>
        <div class="col cursor-style">

            <img src="assets/img/s3.svg" style="padding:10px">
            <p class="service-txt-main">Technical Services</p>
        </div>
    </div>

    <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3 text-center service-style-icon " style="align-items:center">
        <div class="col cursor-style" onclick="upgrade()">
            <img src="assets/img/s4.svg" style="padding:10px">
            <p class="service-txt-main">Upgrades & Customizations</p>
        </div>
        <div class="col cursor-style" onclick="ams()">
            <img src="assets/img/s5.svg" style="padding:10px">
            <p class="service-txt-main">AMS & Support Services</p>
        </div>
        <div class="col cursor-style" onclick="resourc()">
            <img src="assets/img/s6.svg" style="padding:10px">
            <p class="service-txt-main">Resource
                Augmentation</p>
        </div>
    </div>

    <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3 text-center justify-content-center service-style-icon" style="align-items:center">
        <div class="col cursor-style" onclick="training()">
            <img src="assets/img/s7.svg" style="padding:10px">
            <p class="service-txt-main">Training</p>
        </div>
        <!-- <div class="col cursor-style" onclick="eProcurement();">
            <img src="assets/img/s8.svg" style="padding:10px">
            <p class="service-txt-main">Navulia E-Procurement</p>
        </div> -->

    </div>
</div>




