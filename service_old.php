<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SERVICE - XITRICON </title>
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Plus Jakarta Sans' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <main>
        <div class="hero">
            <div class="row" id="navbar">
                <header class="d-flex flex-wrap align-items-center justify-content-center justify-content-md-between py-3">
                    <a href="index.php" class="d-flex align-items-center col-md-3 mb-2 mb-md-0 text-dark text-decoration-none">
                        <img class="d-block" src="assets/img/logo-b.svg" alt="" width="213" height="102">
                    </a>

                    <ul class="menu-mobile nav col-12 col-md-auto mb-2 justify-content-center mb-md-0 about-nav">
                        <li><a href="index.php" class="nav-link px-4">Home</a></li>
                        <li><a href="about-us.php" class="nav-link px-4">About</a></li>
                        <li><a href="#" class="nav-link px-4 active">Services</a></li>
                        <li><a href="navulia.php" class="nav-link px-4">Navulia</a></li>
                    </ul>

                    <div class="col-md-3 text-end">
                        <button type="button" class="btn btn-custom" onclick="toggleModal()">Schedule a call<img class="px-2" src="assets/img/arrow-right.svg" /></button>
                    </div>
                </header>
            </div>

            <div class="container px-4 text-center d-flex justify-content-center">
                <div class="col-8">
                    <h1 class="pb-2 section-1-header">Learn more about our solutions</h1>
                    <p class="mt-2 lh-lg font-size-p">We offer a comprehensive suite of services to companies in various
                        industries that hope to go digital. Be it implementation and customizing your solutions or
                        functional &amp; technical trainings, we are here to transform your business.</p>
                </div>
            </div>

            <!-- Section 1 -->
            <?php include 'componets/service-mobile-img-sec.php' ?>

            <!-- Section 3 -->

            <div class="container" id="section-3" style=" margin-bottom: 40px;">
                <div class="row align-items-md-stretch sec2-bg-2 service-col-bottom padding-img-sec-service mrg-zero">
                    <div class="col">
                        <div>
                            <img src="assets/img/section-img2.png" style="
                            border-radius: 42px;
                            padding: 20px !important;
                        ">
                        </div>
                    </div>
                    <div class="col">
                        <div style="
                        margin-left: 10px;
                        margin-right: 20px;
                    ">
                            <img class="img-custom-service-2" src="assets/img/i.svg" width="49px" height="49px" style="padding: 20px;">
                            <h5>Navulia E-Procurement</h5>
                            <p style="padding-bottom: 20px;font-size: 20px">A scalable procurement solution that drives enterprise-wide
                                growth through cost savings, risk reduction and more. </p>
                            <div class="d-flex align-items-center service-bottom-btn">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-sm service-arrow-btn"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>
                                </div>
                                <div>
                                    <p class="btn-next-txt" style="color:#FF7D00">Learn More</p>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <!-- Bottom section -->
            <section class="py-5 d-flex text-center d-flex justify-content-center">
                <div class="col">
                    <img src="assets/img/ellipse.svg">
                    <h2 class="blog-txt">We can transform your business!</h2>
                    <p class="blog-txt-sub">Reach out to us today.</p>
                    <button type="button" class="btn sec2-btn-custom-1" onclick="toggleModalSchedule()">Schedule a call<img class="px-2" src="assets/img/arrow-right.svg" /></button>
                </div>
            </section>

            <!-- Footer -->
               <!-- footer -->
        <?php include 'componets/footer.php' ?>
            <!-- Footer end -->
 <!-- PopUp -->
 <?php include 'componets/popup.php' ?>
            <?php include 'componets/popup-send-email.php' ?>
            <!-- PopUp -->


            <script>
                window.onscroll = function() {
                    myFunction()
                };

                var navbar = document.getElementById("navbar");
                var sticky = navbar.offsetTop;

                function myFunction() {
                    if (window.pageYOffset > sticky) {
                        navbar.classList.add("sticky-w")
                    } else {
                        navbar.classList.remove("sticky-w");
                    }
                }
            </script>

</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
<script src="assets/js/bootstrap.min.js" integrity="sha384-mQ93GR66B00ZXjt0YO5KlohRA5SY2XofN4zfuZxLkoj1gXtW8ANNCe9d5Y3eG5eD" crossorigin="anonymous"></script>

</html>