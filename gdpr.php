<!doctype html>
<html lang="en">

<head>
	<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PPJV89K');</script>
<!-- End Google Tag Manager -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>XITRICON</title>
	<link href="assets/css/style.css" rel="stylesheet">
	<link href="assets/css/bootstrap.min.css" rel="stylesheet">
	<link href='https://fonts.googleapis.com/css?family=Plus Jakarta Sans' rel='stylesheet'>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PPJV89K"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="py-3"></div>
<?php include 'componets/nav-blue.php' ?>
<?php include 'componets/mobile-nav.php' ?>
<div class="py-4 px-5">
	<div class="container">
		<h1>Our Company Privacy Policy</h1><br/>
		Our Company is part of the Our Company Group which includes Our Company International and Our Company Direct. This privacy policy will explain how our organization uses the personal data we collect from you when you use our website.
		<br/>
		Topics: <br/>

		• What data do we collect?<br/>
		• How do we collect your data?<br/>
		• How will we use your data?<br/>
		• How do we store your data?<br/>
		• Marketing<br/>
		• What are your data protection rights?<br/>
		• What are cookies?<br/>
		• How do we use cookies?<br/>
		• What types of cookies do we use?<br/>
		• How to manage your cookies<br/>
		• Privacy policies of other websites<br/>
		• Changes to our privacy policy<br/>
		• How to contact us<br/>
		• How to contact the appropriate authorities<br/>
		<br/>
		<h3>What data do we collect?</h3><br/>
		Our Company collects the following data:<br/>
		• Personal identification information (Name, email address, phone number, etc.)<br/>
		• [Add any other data your company collects]<br/>

		<br/><h3>How do we collect your data?</h3><br/>
		You directly provide Our Company with most of the data we collect. We collect data and process data when you:<br/>
		• Register online or place an order for any of our products or services.<br/>
		• Voluntarily complete a customer survey or provide feedback on any of our message boards or via email.<br/>
		• Use or view our website via your browser's cookies.<br/>
		• [Add any other ways your company collects data]<br/>
		<br/>
		Our Company may also receive your data indirectly from the following sources:<br/>
		• [Add any indirect source of data your company has)<br/>

		<br/><h3>How will we use your data?</h3><br/>

		Our Company collects your data so that we can:<br/>
		• Process your order, manage your account.<br/>
		• Email you with special offers on other products and services we think you might like.<br/>
		• [Add how else your company uses data]<br/>
		<br/>
		If you agree, Our Company will share your data with our partner companies so that they may offer you their products and services.<br/>
		• [List organizations that will receive data]<br/>

		When Our Company processes your order, it may send your data to, and also use the resulting information from, credit reference agencies to prevent fraudulent purchases.

		<br/><h3>How do we store your data?</h3><br/>
		Our Company securely stores your data at [enter the location and describe security precautions taken].<br/>
		<br/>
		Our Company will keep your [enter type of data] for [enter time period]. Once this time period has expired, we will delete your data by [enter how you delete users' data].
		<br/>
		<br/><h3>Marketing</h3><br/>
		Our Company would like to send you information about products and services of ours that we think you might like, as well as those of our partner companies.<br/>
		• [List partner companies here]<br/><br/>

		If you have agreed to receive marketing, you may always opt out at a later date.<br/>

		You have the right at any time to stop Our Company from contacting you for marketing purposes or giving your data to other members of the Our Company Group.<br/>

		If you no longer wish to be contacted for marketing purposes, please click here.<br/>

		<br/><h3>What are your data protection rights?</h3><br/>
		Our Company would like to make sure you are fully aware of all of your data protection rights. Every user is entitled to the following:<br/>

		The right to access - You have the right to request Our Company for copies of your personal data. We may charge you a small fee for this service.<br/>

		The right to rectification - You have the right to request that Our Company correct any information you believe is inaccurate. You also have the right to request Our Company to complete information you believe is incomplete.
		<br/>
		The right to erasure - You have the right to request that Our Company erase your personal data, under certain conditions.
		<br/>
		The right to restrict processing - You have the right to request that Our Company restrict the processing of your personal data, under certain conditions.
		<br/>
		The right to object to processing - You have the right to object to Our Company's processing of your personal data, under certain conditions.
		<br/>
		The right to data portability - You have the right to request that Our Company transfer the data that we have collected to another organization, or directly to you, under certain conditions.
		<br/>
		If you make a request, we have one month to respond to you. If you would like to exercise any of these rights, please contact us at our email:
		Call us at:<br/>
		Or write to us:<br/>

		<h3>What are cookies?</h3><br/>
		Cookies are text files placed on your computer to collect standard Internet log information and visitor behavior information. When you visit our websites, we may collect information from you automatically through cookies or similar technology.
		<br/>
		For further information, visit allaboutcookies.org.<br/>

		<br/><h3>How do we use cookies?</h3><br/>
		Our Company uses cookies in a range of ways to improve your experience on our website, including:<br/>
		• Keeping you signed in<br/>
		• Understanding how you use our website<br/>
		• [Add any uses your company has for cookies]<br/>

		<br/><h3>What types of cookies do we use?</h3><br/>
		There are a number of different types of cookies, however, our website uses:<br/>
		• Functionality - Our Company uses these cookies so that we recognize you on our website and remember your previously selected preferences. These could include what language you prefer and location you are in. A mix of first-party and third-party cookies are used.<br/>
		• Advertising - Our Company uses these cookies to collect information about your visit to our website, the content you viewed, the links you followed and information about your browser, device, and your IP address. Our Company sometimes shares some limited aspects of this data with third parties for advertising purposes. We may also share online data collected through cookies with our advertising partners. This means that when you
		<br/>
		visit another website, you may be shown advertising based on your browsing patterns on our website.<br/>
		• [Add any other types of cookies your company uses]<br/>

		<br/><h3>How to manage cookies</h3><br/>
		You can set your browser not to accept cookies, and the above website tells you how to remove cookies from your browser. However, in a few cases, some of our website features may not function as a result.
		<br/>
		<br/><h3>Privacy policies of other websites</h3><br/>
		The Our Company website contains links to other websites. Our privacy policy applies only to our website, so if you click on a link to another website, you should read their privacy policy.
		<br/>
		<br/><h3>Changes to our privacy policy</h3><br/>
		Our Company keeps its privacy policy under regular review and places any updates on this web page. This privacy policy was last updated on 9 January 2019.
		<br/>
		<br/><h3>How to contact us</h3><br/>
		If you have any questions about Our Company's privacy policy, the data we hold on you, or you would like to exercise one of your data protection rights, please do not hesitate to contact us.
		<br/>
		Email us at: info@xitricon.com<br/>
		Call us: +94-773788295<br/>
		Or write to us at: P.O.BOX 120430, ALLIANCE BUSINESS CENTER, DUBAI SILICON OASIS, DUBAI, UNITED ARAB EMIRATES<br/>
		<br/>
		How to contact the appropriate authority<br/>
		Should you wish to report a complaint or if you feel that Our Company has not addressed your concern in a satisfactory manner, you may contact the Information Commissioner's Office.
		<br/>
		Email: info@xitricon.com<br/>
		Address: P.O.BOX 120430, ALLIANCE BUSINESS CENTER, DUBAI SILICON OASIS, DUBAI, UNITED ARAB EMIRATES<br/>
	</div>

</div>

<!-- footer -->
<?php include 'componets/footer.php' ?>
<!-- Footer end -->
</body>

 <!-- PopUp -->
 <?php include 'componets/popup.php' ?>
            <?php include 'componets/popup-send-email.php' ?>
            <!-- PopUp -->


<?php include 'componets/script_includes.php' ?>
</html>