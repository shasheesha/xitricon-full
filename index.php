<?php
require "./api/functions.php";
getVisitorIP();
?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>XITRICON - A trusted leader in IFS, Boomi & Infor implementation, training, support and upgrade services.</title>
    <meta name="description" content="Xitricon is a leader in consulting, technology and digital transformation services across globe. With a comprehensive suite of services to aid in digital transformation, we help support and accelerate your organization’s journey to unlock productivity gains across all levels of your operations.">
    <meta name="keywords" content="ERP Software Companies, FSM Implementation,  IFS implementation, Boomi implementation, Infor implementation, training services, support services, upgrade services, consulting services, digital transformation services, productivity gains, operational efficiency, technology services, comprehensive suite of services">
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Plus Jakarta Sans' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="canonical" href="https://www.xitricon.com/">
    
</head>

<body>
    <main>
        <?php include 'componets/mobile-nav.php' ?>
        <div class="hero-bg">
            <video id="bg-video" controls preload="true" autoplay loop muted>
                <source src="assets/videos/xitricon-trial.mp4" type="video/mp4">
            </video>
            <?php include 'componets/nav-white.php' ?>
            <div class="px-4 text-center screen-1260">
                <p class="display-5 banner-text">Innovate.</p>
                <p class="display-5 banner-text">Differentiate.</p>
                <p class="display-5 banner-text">Grow.</p>
            </div>

            <!-- <button type="button" class="btn btn-custom-ban" onclick="location.href='about-us.php'">About Us <img class="px-2" src="assets/img/arrow-right.svg"></button> -->
            <?php include 'componets/index-banner-body-logo.php' ?>


            <div class="container my-5 fade-in">
                <div class="row">
                    <div class="col py-3 d-flex align-items-center justify-content-center"><img src="assets/img/ifs.svg" alt="IFS"> </div>
                    <div class="col py-3 d-flex align-items-center justify-content-center"><img src="assets/img/infor.svg" alt="Infor"></div>
                    <div class="col py-3 d-flex align-items-center justify-content-center"><img src="assets/img/boomi.svg" alt="Boomi"></div>

                </div>
            </div>
        </div>

        <!-- Section 1 -->

        <div class="container fade-in px-4 py-4 text-center d-flex justify-content-center" id="section-1">
            <div class="col-8">
                <h1 class="pb-2 section-1-header">We are a global enterprise service provider</h1>
                <p class="mt-2 lh-lg font-size-p">Xitricon is a leading consulting, technology, and digital transformation services firm that offers a comprehensive suite of solutions to help organizations successfully navigate their digital transformation journey.</p>
            </div>
        </div>

        <div class="container fade-in ">
            <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3 text-center">
                <div class="col ">
                    <img class="img-custom" src="assets/img/noun-employee.svg" alt="Employees">
                    <h2 class="sec1-count-txt">200+</h2>
                    <p class="sec1-count-txt-sub">Enthusiastic Employees</p>
                </div>
                <div class="col">
                    <img class="img-custom" src="assets/img/noun-handshake.svg" alt="Global Presence">
                    <h2 class="sec1-count-txt">3+</h2>
                    <p class="sec1-count-txt-sub">Global Alliances</p>
                </div>
                <div class="col">
                    <img class="img-custom" src="assets/img/noun-globe.svg" alt="Location">
                    <h2 class="sec1-count-txt">5</h2>
                    <p class="sec1-count-txt-sub">Global Locations</p>
                </div>
            </div>
        </div>


        <div class="container fade-in px-4 py-4 text-center d-flex justify-content-center" id="section-1">
            <div class="col-8">
                <h2 class="pb-2 section-1-header">Accelerate Your Growth with Our Proven Solutions</h2>
                <p class="mt-2 lh-lg font-size-p">Our extensive industry expertise, worldwide reach, and advanced solutions empower enterprises to maintain a competitive edge.</p>
            </div>
        </div>

        <!-- Section 2 -->
        <div class="container fade-in " id="section-2">
            <div class="row align-items-md-stretch">
                <div class="col-md-6">
                    <div class="h-100 p-5 sec2-bg-1">
                        <h5>OUR SERVICES </h5>
                        <!-- <h3>We are always available to accelerate your company’s growth with a range of services.</h3> -->
                        <!-- <img src="assets/img/section-img1.png"> -->
                        <!-- <p>Capabilities</p> -->
                        <div class="row tag-style">
                            <span>Implementation</span>
                            <span>Training</span>
                            <span>Upgrades</span>
                            <span>Customizations</span>
                            <span>AMS Services</span>
                            <span>Resource Augmentation</span>
                            <span>Software Licensing</span>

                            <!-- <span class="more-txt">+ More</span> -->
                        </div>
                        <button type="button" class="btn sec2-btn-custom" onclick="location.href='service.php'">Learn
                            more<img class="px-2" src="assets/img/arrow-right.svg" alt=">"/></button>
                    </div>
                </div>
                <div class="col-md-6 ">
                    <div class="h-100 p-5 sec2-bg-2">
                        <h5>Navulia E-PROCUREMENT</h5>
                        <!-- <h3>Discover an effective and scalable procurement solution to drive enterprise-wide growth.
                        </h3> -->
                        <!-- <img src="assets/img/section-img2.png" width= "100%"> -->
                        <!-- <p>Capabilities</p> -->
                        <div class="row tag-style tag-text-section">
                            <span>Supplier Management</span>
                            <span>Sourcing & Tendering</span>
                            <span>B2B Platform</span>
                            <!-- <span>BB Platform </span> -->

                            <!-- <span>B2B Portal</span>
                            <span>Seamless Integration</span>
                            <span>Scalable Procurement</span> -->

                            <!-- <span class="more-txt">+ More</span> -->
                        </div>
                        <button type="button" class="btn sec2-btn-custom-1" onclick="location.href='navulia.php'">Learn
                            more<img class="px-2" src="assets/img/arrow-right.svg" alt=">"/></button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Blog And Articles -->
        <div class="container px-4 py-4 text-center d-flex justify-content-center"></div>
        <div class="container fade-in px-4 py-4 text-center d-flex justify-content-center" id="section-1">
            <div class="col-8">
                <h2 class="pb-2 mt-5 section-1-header">Events & Updates</h2>
                <!--p class="mt-2 lh-lg font-size-p">Keep yourself up-to-date with the latest industry-related topics by
                    perusing our
                    collection of blogs and articles.</p-->
            </div>

        </div>

        <div class="container mb-5 fade-in ">
            <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3 d-flex justify-content-center ">
                 <div class="col">
                    <div class="card cursor-style" onclick="buttonClickBlog3()">
                        <img class="bd-placeholder-img card-img-top" width="100%" height="auto"
                            src="assets/img/IFS-Connect-Middle-East.png" alt="IFS Connect Event US">
                        <div class="card-body">
                            <!--p class="card-text">Event</p-->
                            <p class="card-text-p">Welcome to IFS Connect North America 2023</p>

                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card cursor-style" onclick="buttonClickBlog2()">
                        <img class="bd-placeholder-img card-img-top" width="100%" height="auto"
                            src="assets/img/IFS-Connect-US.png" alt="IFS Connect Event Middle East">
                        <div class="card-body">
                            <!--p class="card-text">Event</p-->
                            <p class="card-text-p">Welcome to IFS Connect Middle East 2023</p>

                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card cursor-style" onclick="buttonClickBlog1()">
                        <img class="bd-placeholder-img card-img-top" width="100%" height="auto"
                            src="assets/img/blog-img3.png" alt="IFS Connect Event UK & I">
                        <div class="card-body">
                            <!--p class="card-text">Event</p-->
                            <p class="card-text-p">Experience your Future at IFS Connect UK 2023</p>

                        </div>
                    </div>
                </div>
            </div>
        </div> 

        <!-- Bottom section -->
        <section class="py-4 fade-in d-flex text-center d-flex justify-content-center fade-in ">
            <div class="col">
                <img src="assets/img/ellipse.svg" alt=">">
                <h2 class="blog-txt">We can transform your business!</h2>
                <p class="blog-txt-sub">Reach out to us today.</p>
                <button type="button" class="btn sec2-btn-custom-1" data-bs-toggle="modal" data-bs-target="#modal1">Schedule a call<img class="px-2" src="assets/img/arrow-right.svg" alt=">" /></button>
            </div>
        </section>

        <!-- footer -->
        <?php include 'componets/footer.php' ?>
        <!-- footer end -->

         <!-- PopUp -->
         <?php include 'componets/popup.php' ?>
            <?php include 'componets/popup-send-email.php' ?>
            <!-- PopUp -->
    </main>

    <script>
        window.onscroll = function() {
            myFunction()
        };

        var navbar = document.getElementById("navbar");
        var sticky = navbar.offsetTop;

        function myFunction() {
            if (window.pageYOffset > sticky) {
                navbar.classList.add("sticky")
            } else {
                navbar.classList.remove("sticky");
            }
        }

        function buttonClickBlog1() {
            window.location.href = "IFS-connect-event.php";
        }

        function buttonClickBlog2() {
            window.location.href = "IFS-connect-event-MENA.php";
        }

         function buttonClickBlog3() {
            window.location.href = "IFS-connect-event-NA.php";
        }
    </script>

<script src="https://code.jquery.com/jquery-3.7.0.js" integrity="sha256-JlqSTELeR4TLqP0OG9dxM7yDPqX1ox/HfgiSLBj8+kM=" crossorigin="anonymous"></script>
<script src="./assets/js/main.js"></script>
</body>

<?php include 'componets/script_includes.php' ?>

</html>