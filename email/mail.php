<?php

//Import PHPMailer classes into the global namespace
//These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'src/Exception.php';
require 'src/PHPMailer.php';
require 'src/SMTP.php';

// //Load Composer's autoloader
// require 'vendor/autoload.php';

//Create an instance; passing `true` enables exceptions
$mail = new PHPMailer(true);

$name = '';
$email = '';
$country = '';
$subject = 'New Inquiry for Xitricon';
$company = '';
$designation = '';
$comment = '';
$d_url = '';
$d_message = '';
$phone = "";

// $name = $_POST['name'];
// $email = $_POST['email'];

$recaptcha = $_POST['g-recaptcha-response'];

$secret_key = '6LdBtcwkAAAAAKKLFFNRv1omExraAcOKTvP9G1rq';

$url = 'https://www.google.com/recaptcha/api/siteverify?secret='
          . $secret_key . '&response=' . $recaptcha;
  
    // Making request to verify captcha
    $response = file_get_contents($url);
  
    // Response return by google is in
    // JSON format, so we have to parse
    // that json
    $response = json_decode($response);
  
    // Checking, if response is true or not
    // if ($response->success == true) {

    
if(isset($_POST['name'])){
    $name = '<tr><td>Name:</td><td>'.$_POST['name'].'</td></tr>';
}

if(isset($_POST['country'])){
    $country = '<tr><td>Country:</td><td>'.$_POST['country'].'</td></tr>';
}

if(isset($_POST['comment'])){
    $comment = '<tr><td>Comment:</td><td>'.$_POST['comment'].'</td></tr>';
}
if(isset($_POST['company'])){
    $company = '<tr><td>Company:</td><td>'.$_POST['company'].'</td></tr>';
}
if(isset($_POST['designation'])){
    $designation = '<tr><td>Designation:</td><td>'.$_POST['designation'].'</td></tr>';
}
if(isset($_POST['email'])){
    $email = '<tr><td>Email:</td><td>'.$_POST['email'].'</td></tr>';
}
if(isset($_POST['phone'])){
    $phone = '<tr><td>Phone:</td><td>'.$_POST['phone'].'</td></tr>';
}
if(isset($_POST['navilu'])){
    $subject = 'New inquiry for Navulia E Procurement Brochure Download';
    $d_message = "Download the brochure here:";
    $d_url = 'https://xitricon.com/assets/pdf/Navulia-E-Procurement.pdf';
}


try {
    //Server settings
    $mail->SMTPDebug = 0;                      //Enable verbose debug output
    $mail->isSMTP();                                            //Send using SMTP
    $mail->Host       = 'smtp.gmail.com';                     //Set the SMTP server to send through
    $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
    $mail->Username   = 'xitricon.marketing@gmail.com';                     //SMTP username
    $mail->Password   = 'tmyrdkagkkcrwcsx';                               //SMTP password
    $mail->SMTPSecure = 'tls';            //Enable implicit TLS encryption
    $mail->Port       = 587;                                  //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`


    //Recipients
    $mail->setFrom('xitricon.marketing@gmail.com', 'Xitricon');
    $mail->addAddress($_POST['email'], $name);     //Add a recipient

    $mail->isHTML(true);                                  //Set email format to HTML
    $mail->Subject = 'Thank you for contacting Xitricon';

    //to client
    $html = file_get_contents('email_temp/to_client.html');
    $html = str_replace('{dlink}', $d_url, $html);
    $html = str_replace('{dmessage}', $d_message, $html);

    $mail->msgHTML($html);

    $mail->send();

    // Clear the recipient list
    $mail->ClearAllRecipients();

    $html = file_get_contents('email_temp/to_xitricon.html');

    $html = str_replace('{name}', $name, $html);
    $html = str_replace('{designation}', $designation, $html);
    $html = str_replace('{email}', $email, $html);
    $html = str_replace('{country}', $country, $html);
    $html = str_replace('{company}', $company, $html);
    $html = str_replace('{comment}', $comment, $html);
    $html = str_replace('{phone}', $phone, $html);


    $mail->setFrom($_POST['email'], 'Xitricon');
    $mail->addAddress('xitricon.marketing@gmail.com', $name); 
    $mail->Subject = $subject;
    
    $mail->msgHTML($html);

    $mail->send();

    echo '1';
} catch (Exception $e) {
    echo "0";
}

//recaptcha fail
// }else{
//     echo '<script>alert("Error in Google reCAPTACHA")</script>';
// }