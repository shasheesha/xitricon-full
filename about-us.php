<!doctype html>
<html lang="en">

<head>
    <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PPJV89K');</script>
<!-- End Google Tag Manager -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>About Us - Consulting, Technology, and Digital Transformation Services</title>
    <meta name="description" content="Xitricon provides global consulting, tech, and digital transformation services to boost productivity for organizations at all levels. Our experts offer tailored solutions to maximize value of on-premise, cloud, or hybrid software deployments. Our aim: Ensure your success in achieving business outcomes, adoption, ROI, insights, and self-reliance.">
    <meta name="keywords" content="Xitricon, consulting, technology, digital transformation, productivity gains, tailored solutions, project packages, software solutions, on premise, cloud, hybrid environments, business outcomes, adoption, risk mitigation, ROI, insights, self-reliance">
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Plus Jakarta Sans' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="canonical" href="https://www.xitricon.com/about-us.php">
</head>

<body>
    <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PPJV89K"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <main>
        <?php include 'componets/mobile-nav.php' ?>
        <div class="hero">
            <div class="py-3"></div>
            <?php include 'componets/nav-blue.php' ?>
            <div class="py-3"></div>
            <div class="container px-4 text-center d-flex justify-content-center">
                <div class="col-8">
                    <h1 class="pb-2 section-1-header">We are a global enterprise service provider</h1>
                </div>
            </div>
            <div class="container">
                <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3 text-center py-5">

                    <div class="col ">

                        <h2 class="sec1-count-txt">200+</h2>
                        <p class="sec1-count-txt-sub">Enthusiastic Employees</p>
                    </div>
                    <div class="col">

                        <h2 class="sec1-count-txt">3+</h2>
                        <p class="sec1-count-txt-sub">Global Alliances</p>
                    </div>
                    <div class="col">

                        <h2 class="sec1-count-txt">5</h2>
                        <p class="sec1-count-txt-sub">Global Locations</p>
                    </div>
                </div>

                <div class="container fade-in  px-4 py-5 text-center d-flex justify-content-center">
                    <div class="col-8">
                        <h1 class="pb-2 section-header-about">Industries we engage in</h1>
                        <!-- <p class="mt-2 lh-lg font-size-p">Xitricon operates in multiple industries, including aviation,
                    manufacturing, communication, energy, engineering & AMP, construction, and services. Our extensive
                    knowledge and experience in various businesses have enabled us offer tailor-made innovate solutions
                    to our clients from diverse sectors.</p> -->
                    </div>
                </div>

                <?php include 'componets/about-mobile-img-sec.php' ?>

            </div>
        </div>

        <div class="ccontainer px-4 text-center d-flex justify-content-center" style="margin-bottom: 5rem;">
            <div class="col-8">
                <h1 class="pb-2 section-1-header"> Extending our capabilities and sharing our expertise in new geographies</h1>
                <div class="container px-4 text-center d-flex justify-content-center">
                    <img class="text-center about-banner" src="assets/img/about-hero.png">
                </div>
            </div>
        </div>

        <!-- Bottom section -->
        <section class="py-5 fade-in d-flex text-center d-flex justify-content-center">
            <div class="col">
                <img src="assets/img/ellipse.svg">
                <h2 class="blog-txt">View our services & expertise</h2>
                <p class="blog-txt-sub">Let’s see what we can do for you!</p>
                <button type="button" class="btn sec2-btn-custom-1" onclick="buttonClickEvent()">Our Services<img class="px-2" src="assets/img/arrow-right.svg" /></button>
            </div>
        </section>

        <!-- Footer -->
        <!-- footer -->
        <?php include 'componets/footer.php' ?>
        <!-- Footer end -->

       <!-- PopUp -->
       <?php include 'componets/popup.php' ?>
            <?php include 'componets/popup-send-email.php' ?>
            <!-- PopUp -->
    </main>

    <script>
        function buttonClickEvent() {
            window.location.href = "service.php";
        }
    </script>

    <script>
        window.onscroll = function() {
            myFunction()
        };

        var navbar = document.getElementById("navbar");
        var sticky = navbar.offsetTop;

        function myFunction() {
            if (window.pageYOffset > sticky) {
                navbar.classList.add("sticky-w")
            } else {
                navbar.classList.remove("sticky-w");
            }
        }
    </script>
</body>

<?php include 'componets/script_includes.php' ?>

</html>