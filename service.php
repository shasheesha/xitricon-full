<!doctype html>
<html lang="en">

<head>
     <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PPJV89K');</script>
<!-- End Google Tag Manager -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Services - Accelerate Your Growth with Our Proven Services.</title>
    <meta name="description" content="Xitricon accelerates your organization's growth with proven services like implementation, licensing, upgrades, and customization, technical & support services, resource augmentation, and training for IFS, Boomi, Infor & more.">
    <meta name="keywords" content="Implementation, integration, software licensing, IFS, Boomi, Infor, technical services, upgrades, customizations, AMS, support services, resource augmentation, training">
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Plus Jakarta Sans' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PPJV89K"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <main>
        <?php include 'componets/mobile-nav.php' ?>
        <div class="hero-bg-service">
        <div class="py-3"></div>
            <?php include 'componets/nav-white.php' ?>
            <div class="py-3"></div>
            <?php include 'componets/service-icon-body.php' ?>
        </div>
      

        <!-- Bottom section -->
        <section class="py-5 d-flex text-center d-flex justify-content-center">
            <div class="col">
                <img src="assets/img/ellipse.svg">
                <h2 class="blog-txt">We can transform your business!</h2>
                <p class="blog-txt-sub">Get in touch with us today</p>
                <button type="button" class="btn sec2-btn-custom-1" data-bs-toggle="modal"
                    data-bs-target="#modal1">Schedule a call<img class="px-2" src="assets/img/arrow-right.svg" /></button>
            </div>
        </section>

        <!-- Footer -->
        <!-- footer -->
        <?php include 'componets/footer.php' ?>
        <!-- Footer end -->

 <!-- PopUp -->
 <?php include 'componets/popup.php' ?>
            <?php include 'componets/popup-send-email.php' ?>
            <!-- PopUp -->


        <script>
        window.onscroll = function() {
            myFunction()
        };

        var navbar = document.getElementById("navbar");
        var sticky = navbar.offsetTop;

        function myFunction() {
            if (window.pageYOffset > sticky) {
                navbar.classList.add("sticky")
            } else {
                navbar.classList.remove("sticky");
            }
        }

        function training() {
            window.open('assets/pdf/traning.pdf', '_blank');
        }

        function resourc() {
            window.open('assets/pdf/resourc.pdf', '_blank');
        }

        function eProcurement() {
            window.open('assets/pdf/eProcurement.pdf', '_blank');
        }

        function upgrade() {
            window.open('assets/pdf/upgrades.pdf', '_blank');
        }

        function ams() {
            window.open('assets/pdf/ams.pdf', '_blank');
        }
        </script>

</body>


<?php include 'componets/script_includes.php' ?>

</html>