const _50ms = 50
const _100ms = 50 * 2
const _1s = _100ms * 10
const _5s = _1s * 5
const _10s = _5s * 2
const GRADIENT_ANIMATION_DURATION = _5s
  
// Min size = 10 and max size = 40, must be > 0.
const STAR_SIZE = [5, 20]

// Hold all stars state that are appearing in the screen.
let stars = []

// How many stars should be rendered at once in the screen.
// Set to null to render based on screen space available.
const STARS_COUNT = null

const RED = `#F70000`
const ALMOST_GREEN = `#3DD2D0`
const MAIN_COLOR = ALMOST_GREEN

// Generate random stars that vary in both directions (x and y).
const BIDIRECTIONAL = `BIDIRECTIONAL`
// Generate stars that vary only in one direction (x or y).
const UNIDIRECTIONAL = `UNIDIRECTIONAL`

const STAR_GENERATION_MODE = UNIDIRECTIONAL

// How fast each star accelerate, min and max values respectively, must be >= 0 and 0 means a static position (loses the main motion effect).
const STAR_VELOCITY_MIN_AND_MAX = [1.5, 20]

// How fast each star reduces it's sizes to dust, min and max values respectively, must be >= 0 and 0 means constant size.
const SIZE_DECREASE_FACTOR = [0.1, 0.4]

function setup() {
  createCanvas(windowWidth, windowHeight)

  const [_, maxSize] = STAR_SIZE.map(Math.abs)  

  const starsCount = STARS_COUNT || max(windowWidth, windowHeight) / maxSize

  stars = Array.from({ length: starsCount }).map(_ => createStar())
}


function createStar() {
  const rand = random()

  const [minSize, maxSize] = STAR_SIZE.map(Math.abs)
  
  // Randomly decide the star size.
  const size = rand * (maxSize - minSize) + minSize
  
  const velocityX = random(...STAR_VELOCITY_MIN_AND_MAX)
  const velocityY = random(...STAR_VELOCITY_MIN_AND_MAX)
  
  return {
    velocity: {
      x: velocityX / 3,
      y: STAR_GENERATION_MODE === BIDIRECTIONAL ? velocityY : velocityX,
    },
    size,
    position: generateRandomInitialPosition(),
    sizeDecreaseFactor: random(...SIZE_DECREASE_FACTOR),
    initialSize: size,
  }
}

function updateStar({ position, velocity, size, sizeDecreaseFactor, initialSize }) {
  return {
    position: {
      x: position.x - velocity.x,
      y: position.y - velocity.y
    },
    size: size - sizeDecreaseFactor,
    initialSize,
    sizeDecreaseFactor,
    velocity
  }
}

function starIsVisible(star) {
  return star.position.x + star.size < width && 
          star.position.x + star.size > 0 &&
          star.position.y + star.size < height &&
          star.position.y + star.size > 0 &&
          star.size > 0
}

// Generates randomly an initial position on the right/bottom sides.
function generateRandomInitialPosition() {
  const rand = random() <= 0.9

  return {
    x: rand ? random() * width : width,
    y: rand ? height : random() * height,
  }
}

function drawStar(star) {
  const alpha = star.size / star.initialSize * 255
  const rgb = hex2rgb(MAIN_COLOR)

  fill(...rgb, alpha)
  rect(star.position.x, star.position.y, star.size, star.size)
}

function hex2rgb(hex) {
    const r = parseInt(hex.slice(1, 3), 16)
    const g = parseInt(hex.slice(3, 5), 16)
    const b = parseInt(hex.slice(5, 7), 16)
    return [r, g, b]
}

function setGradient(x, y, w, h, c1, c2) {
  noFill();

  // Top to bottom gradient
  for (let i = y; i <= y + h; i++) {
    let inter = map(i, y, y + h, 0, 1);
    let c = lerpColor(c1, c2, inter);
    stroke(c);
    line(x, i, x + w, i);
  }
}

function setLinearGradientBackground() {
  // Define the background color as an almost black color.
  background(0, 0, 20)

  // A value of the animation current state.
  // It will get the division modulus of the [GRADIENT_ANIMATION_DURATION].
  // And divide by the [GRADIENT_ANIMATION_DURATION] itself.
  // This computate a decimal number between 0 and 1 every [GRADIENT_ANIMATION_DURATION] seconds.
  const value = (millis() % GRADIENT_ANIMATION_DURATION) / GRADIENT_ANIMATION_DURATION

  // We want the effect of starting and then fading out.
  const curve = value > 0.5 ? 1 - value : value

  // Multiply by 255 to get the alpha value.
  const gradientAlpha = curve * 255

  // Create the color based on the constant [MAIN_COLOR].
  const gradientColor = color(...hex2rgb(MAIN_COLOR), gradientAlpha)

  // Then set the gradient through our function created earlier.
  setGradient(0, 0, width, height, color(0, 0, 0, 0), gradientColor)
}

function draw() {
  noStroke()
  clear(0, 0, width, height)
  
  setLinearGradientBackground()
  
  for(const star of stars) {
    drawStar(star)

    const updatedStar = updateStar(star)
    
    if (starIsVisible(updatedStar)) {
      stars[stars.indexOf(star)] = updatedStar
    } else {
      stars[stars.indexOf(star)] = createStar()
    }
  }
}
