   // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function () {
        'use strict'

        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.querySelectorAll('.needs-validation')

        // Loop over them and prevent submission
        Array.prototype.slice.call(forms)
            .forEach(function (form) {
                form.addEventListener('submit', function (event) {
                    if (!form.checkValidity()) {
                        event.preventDefault()
                        event.stopPropagation()
                    }

                    form.classList.add('was-validated')
                }, false)
            })
    })()

    function generateClientId() {
        // Check if the client ID is stored in local storage
        let clientId = localStorage.getItem('clientId');

        if (!clientId) {
            // If not present, generate a random ID and store it in local storage
            clientId = Math.random().toString(36).substring(2);
            localStorage.setItem('clientId', clientId);
        }

        return clientId;
    }

    $(window).on('load', function() {

        let ip = document.getElementById('ip');
    let url = document.getElementById('current_url');
    const clientId = generateClientId();
    const xhr = new XMLHttpRequest();
    xhr.open('POST', './api/main-api.php', true);
    xhr.setRequestHeader("application-auth", "xitricon-auth");

    const formData = new FormData();
    formData.append('ip', clientId);
    formData.append('url', url.value);
    formData.append('action', "add_visitor");

    xhr.onload = function () {
        if (xhr.readyState === xhr.DONE && xhr.status === 200) {
            if (xhr.responseText) {
                var responseObj = JSON.parse(xhr.responseText);
                if (responseObj) {
                    if (responseObj.status === "SUCCESS") {

                    } else {
                    alert(responseObj.msg);

                    }
                }
            }
        }
    };
    xhr.send(formData);
    });

    // $(window).on('load', function() {
    //     let ip = document.getElementById('ip');
    //     let url = document.getElementById('current_url');
    //     const clientId = generateClientId();
    //     const firstPath = './api/main-api.php';
    //     const secondPath = '../api/main-api.php';
        
    //     const formData = new FormData();
    //     formData.append('ip', clientId);
    //     formData.append('url', url.value);
    //     formData.append('action', "add_visitor");
    
    //     fetch(firstPath, {
    //         method: 'POST',
    //         headers: {
    //             "application-auth": "xitricon-auth",
    //         },
    //         body: formData
    //     })
    //     .then(response => {
    //         if (response.ok) {
    //             return response.json();
    //         } else {
    //             throw new Error('Network response was not ok.');
    //         }
    //     })
    //     .then(data => {
    //         if (data && data.status === "SUCCESS") {
    //             // Successful response
    //             // location.reload();
    //             // $('#subscribe-modal').modal('toggle');
    //         } else {
    //             // Handle unsuccessful response
    //             alert(data.msg);
    //         }
    //     })
    //     .catch(error => {
    //         console.error('Error:', error);
    //         // Try the second path
    //         fetch(secondPath, {
    //             method: 'POST',
    //             headers: {
    //                 "application-auth": "xitricon-auth",
    //             },
    //             body: formData
    //         })
    //         .then(response => {
    //             if (response.ok) {
    //                 return response.json();
    //             } else {
    //                 throw new Error('Network response was not ok.');
    //             }
    //         })
    //         .then(data => {
    //             if (data && data.status === "SUCCESS") {
    //                 // Successful response
    //                 // location.reload();
    //                 // $('#subscribe-modal').modal('toggle');
    //             } else {
    //                 // Handle unsuccessful response
    //                 alert(data.msg);
    //             }
    //         })
    //         .catch(error => {
    //             console.error('Error:', error);
    //             // Handle errors from the second path if necessary
    //         });
    //     });
    // });
    