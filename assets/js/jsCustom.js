//navilu form validate
var navilu_form = $('#mail_form');

navilu_form.validate({
  rules: {
    fullname: {
      required: true
    },
    email: {
      required: true,
      email: true
    },
    company: {
      required: true
    },
    country: {
      required: true
    }
  },
  messages: {
    name: "Please enter your full name",
    email: "Please enter a valid email",
    company: "Please enter your company name",
    country: "Please select a country",
  },

  highlight: function (element, errorClass, validClass) {
    $(element).addClass('is-invalid');
  },

  unhighlight: function (element, errorClass, validClass) {
    $(element).removeClass('is-invalid');
  }
});


//navilu form validate
var contact_form = $('#mail_form2');

contact_form.validate({
  rules: {
    fullname: {
      required: true
    },
    email: {
      required: true,
      email: true
    },
    country: {
      required: true
    }
  },
  messages: {
    name: "Please enter your full name",
    email: "Please enter a valid email",
    country: "Please select a country",
  },

  highlight: function (element, errorClass, validClass) {
    $(element).addClass('is-invalid');
  },

  unhighlight: function (element, errorClass, validClass) {
    $(element).removeClass('is-invalid');
  }
});


navilu_form.submit(function (e) {
  e.preventDefault();
  if ($(this).valid()) {
    $('#modal2').modal('hide');
    $('#modalsuccess').modal('show');
    $("#sendmail").prop('disabled', true);
    $.ajax({
      type: "POST",
      url: "email/mail.php",
      data: $('#mail_form').serialize(),
      success: function (result) {
        $('#modal2').modal('hide');
        if (result == '1') {
          // $('#modalsuccess').modal('show');
        }
      },
      error: function (jqXHR, exception) {
      }
    });
    $(this)[0].reset();
    $("#sendmail").prop('disabled', false);
  }
});

contact_form.submit(function (e) {
  e.preventDefault();
  if ($(this).valid()) {
    $('#modal1').modal('hide');
    $('#modalsuccess').modal('show');
    $("#sendmail2").prop('disabled', true);
    $.ajax({
      type: "POST",
      url: "email/mail.php",
      data: $('#mail_form2').serialize(),
      success: function (result) {
        $('#modal1').modal('hide');
        if (result == '1') {
          // $('#modalsuccess').modal('show');
        }
      },
      error: function (jqXHR, exception) {
      }
    });
    $(this)[0].reset();
    $("#sendmail2").prop('disabled', false);
  }
});


function onloadCallback() {
  grecaptcha.render('captcha', {
    'sitekey': '6LdBtcwkAAAAAFVqaG-yhkUgRPe6HqQwAjZo4l7S',
    'size': 'invisible'
  });
}
