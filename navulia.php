<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>NAVULIA - XITRICON</title>
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Plus Jakarta Sans' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
    .sec2-btn-custom-1 {
        width: 220px !important;
    }
    </style>
</head>

<body>
    <main>

        <?php include 'componets/mobile-nav.php' ?>

        <div class="hero-bg-navi ">
            <div class="py-3"></div>
            <?php include 'componets/nav-blue.php' ?>
            <div class="px-4 navulia-banner-txt text-center"
                style=" display: flex; flex-direction: column; justify-content: center; align-items: center;">
                <h1 class="display-5 banner-text-new"
                    style="font-weight: normal; letter-spacing: 10px; font-size: 35px !important;">EXPERIENCE</h1>
                <img class="mt-4 mb-4 mobile-style" src="assets/img/navulia-logo.svg" width="40%">
                <h1 class="display-5 banner-text-new">E-Procurement</h1>
            </div>

            <div class="col text-center">
                <!-- <button type="button" class="btn btn-custom-ban-learnmore" data-bs-toggle="modal" data-bs-target="#modal1">Learn
                    More<img class="px-2" src="assets/img/arrow-b.svg"></button> -->
                <button type="button" class="btn btn-custom-ban-learnmore"
                    onclick="document.getElementById('offerSection').scrollIntoView();">Learn
                    More<img class="px-2" src="assets/img/arrow-b.svg"></button>


            </div>
        </div>

        <!-- Section 1 -->
        <div>
            <div class="container px-4 py-5 text-center d-flex justify-content-center" id="section-1">
                <div class="col-8">
                    <h1 class="section-header-about">Why Navulia ? </h1>
                </div>
            </div>

            <?php include 'componets/navulia-mobile-view.php' ?>

        </div>
        <!-- Section 2 -->
        <div id="offerSection" class="offerSection">
            <div class="container px-4 py-5 text-center d-flex justify-content-center">
                <div class="col-8">
                    <h1 class="pb-2 section-header-about">The Offering</h1>
                </div>
            </div>

            <div class="container mb-5">
                <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
                    <div class="col">
                        <div class="card text-center">
                            <div class="card-body">
                                <img class="bd-placeholder-img card-img-top card-new-style" src="assets/img/na-1.svg">
                                <p class="card-text-new">Supplier Management</p>
                                <p class="card-text-p-new">This system simplifies supplier registration, onboarding, and
                                    management with automated processes, compliance checks, and performance evaluations.
                                </p>

                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card text-center">
                            <div class="card-body">
                                <img class="bd-placeholder-img card-img-top card-new-style" src="assets/img/na-2.svg">
                                <p class="card-text-new">Sourcing & Tendering</p>
                                <p class="card-text-p-new">Our solution streamlines tendering events, automating
                                    processes, and providing a comprehensive feature set for efficient sourcing,
                                    compliance, and vendor recommendations. Integration with IFS enhances efficiency.
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col">
                        <div class="card text-center">
                            <div class="card-body">
                                <img class="bd-placeholder-img card-img-top card-new-style" src="assets/img/na-3.svg">
                                <p class="card-text-new" style="width: 90%;">Business - to - Business Portal</p>
                                <p class="card-text-p-new">Enabling a platform that companies can use to work with their
                                    suppliers as well as away for suppliers to manage their information.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="d-flex align-items-center view-btn-new">
                <div class="btn-group">
                    <button type="button" class="btn btn-sm service-arrow-btn"><i
                            class="fa fa-chevron-right" aria-hidden="true"></i></button>
                </div>
                <div>
                    <p class="btn-next-txt">View Brochure</p>
                </div>

            </div> -->
            </div>
        </div>



        <!-- Bottom section -->
        <section class="py-5 d-flex text-center d-flex justify-content-center">
            <div class="col">
                <img src="assets/img/ellipse.svg">
                <h2 class="blog-txt">Experience Navulia Today!</h2>
                <p class="blog-txt-sub">Get in touch with us today</p>
                <button type="button" class="btn sec2-btn-custom-1" data-bs-toggle="modal"
                    data-bs-target="#modal2">Download Brochure<img class="px-2"
                        src="assets/img/arrow-right.svg" /></button>
            </div>
        </section>

        <!-- Footer -->
        <!-- footer -->
        <?php include 'componets/footer.php' ?>
        <!-- Footer end -->


        <!-- PopUp -->

        <?php include 'componets/popup.php' ?>
        <?php include 'componets/popup-navilu.php' ?>
        <?php include 'componets/popup-send-email.php' ?>
        <!-- PopUp -->
    </main>

    <script>
    window.onscroll = function() {
        myFunction()
    };

    var navbar = document.getElementById("navbar");
    var sticky = navbar.offsetTop;

    function myFunction() {
        if (window.pageYOffset > sticky) {
            navbar.classList.add("sticky-w")
        } else {
            navbar.classList.remove("sticky-w");
        }
    }
    </script>

</body>


<?php include 'componets/script_includes.php' ?>

</html>