<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Blog - XITRICON </title>
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Plus Jakarta Sans' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <main>
        <div class="hero">
            <div class="py-3"></div>
            <?php include 'componets/nav-blue.php' ?>
            <?php include 'componets/mobile-nav.php' ?>

            <div class="container">
                <div class="d-flex row">
                    <div class="col popup-padding px-5">
                        <h1 class="py-4" style="font-weight: bold;" id="demo">
                            Schedule a call
                        </h1>
                        <div class="popup-description">
                            <p>Do you have any questions or want to learn more about the services we offer? <br><br>Fill
                                out the
                                form
                                below
                                and our team will get back to you! </p>
                        </div>
                        <form method="post" id="mail_form2">
                            <input type="text" class="form-control my-2" id="fullname" name="name" placeholder="Name"
                                required />
                            <input type="email" class="form-control my-2" id="email" name="email" placeholder="Email" />
                            <input type="telephone" class="form-control my-2" id="phone" name="phone"
                                placeholder="Contact Number" />
                            <?php include 'componets/country-pickup.php' ?>
                            <textarea id="comment" class="form-control my-2" name="comment" rows="4"
                                placeholder="Comment" cols="50"></textarea>

                            <div id="captcha"></div>

                            <button type="submit" class="g-recaptcha btn btn-primary btn-style">
                                Send
                            </button>
                        </form>
                    </div>
                    <div class="col contact-icon">
                        <div class="my-4">
                            <div class="address-section cursor-style ">
                                <div class="pop-icons">
                                    <img src="assets/img/phone-new.svg" style="height: 28px; width: 28px;">
                                </div>
                                <div class="pop-txt-bottom">
                                    <label style="font-weight: bold;">PHONE</label>
                                    <label onclick="phoneBtn1()"
                                        style="color: #FF7D00; font-weight: normal !important;"><span
                                            style="color:black">UAE : </span>+971 558995713</label>
                                    <label onclick="phoneBtn2()"
                                        style="color: #FF7D00; font-weight: normal !important;"><span
                                            style="color:black">UK : </span>+44 7946054506</label>
                                    <label onclick="phoneBtn3()"
                                        style="color: #FF7D00; font-weight: normal !important;"><span
                                            style="color:black">AUS/AFRICA : </span>+94 779151537</label>
                                    <label onclick="phoneBtn3()"
                                        style="color: #FF7D00; font-weight: normal !important;"><span
                                            style="color:black">USA/CANADA : </span>+94 779151537</label>        
                                </div>
                            </div>
                            <div class="address-section cursor-style " onclick="whtsappBtn()">
                                <div class="pop-icons">
                                    <img src="assets/img/wtsapp.svg" style="height: 28px; width: 28px;">
                                </div>
                                <div class="pop-txt-bottom">
                                    <label style="font-weight: bold;">WHATSAPP</label>
                                    <label style="color: #FF7D00; font-weight: normal !important;">+94 77 378
                                        8295</label>
                                </div>
                            </div>

                            <div class="address-section cursor-style " onclick="emailBtn()">
                                <div class="pop-icons">
                                    <img src="assets/img/email-icon.svg">
                                </div>
                                <div class="pop-txt-bottom">
                                    <label style="font-weight: bold;">EMAIL</label>
                                    <label onclick="emailBtn3() "
                                        style="color: #FF7D00; font-weight: normal !important;"><span
                                            style="color:black"></span>info@xitricon.com</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-flex row px-5 py-5 contact-style">
                    <div class=" row">
                        <div class="col contact-col">
                            <div class="contact-locate-icon"> <img src="assets/img/location-pin.svg"></div>
                            <div class="px-3">
                                <p class="cotact-txt-clr">U.A.E</p>
                                <p class="contact-body-txt">P.O.BOX 120430 <br> ALLIANCE BUSINESS CENTER, <br> DUBAI
                                    SILICON OASIS, <br> DUBAI,<br> UNITED ARAB EMIRATES. </p>
                            </div>
                        </div>
                        <div class="col contact-col">
                            <div class="contact-locate-icon"> <img src="assets/img/location-pin.svg"></div>
                            <div class="px-3">
                                <p class="cotact-txt-clr">U.K</p>
                                <p class="contact-body-txt">71-75, SHELTON STREET,
                                    <br>COVENT GARDEN,
                                    <br>LONDON,<br>WC2H 9JQ<br>UNITED KINGDOM.
                                </p>
                            </div>
                        </div>
                        <div class="col contact-col">
                            <div class="contact-locate-icon"> <img src="assets/img/location-pin.svg"></div>
                            <div class="px-3">
                                <p class="cotact-txt-clr">SRI LANKA </p>
                                <p class="contact-body-txt">LEVEL 26 & 34, EAST TOWER,
                                    <br>WORLD TRADE CENTRE,
                                    <br>ECHELON SQUARE,
                                    <br>COLOMBO 01,
                                    <br>SRI LANKA.
                                </p>
                            </div>
                        </div>
                        <div class="col contact-col">
                            <div class="contact-locate-icon"> <img src="assets/img/location-pin.svg"></div>
                            <div class="px-3">
                                <p class="cotact-txt-clr">INDIA</p>
                                <p class="contact-body-txt">XITRICON INDIA PRIVATE LIMITED,
                                    <br>3/A MADHAV
                                    <br>CHATTERJEE ST,
                                    <br>FLAT 1A, KOLKATA, KOLKATA,
                                    <br>WEST BENGAL, INDIA
                                </p>
                            </div>
                        </div>
                        <div class="col contact-col">
                            <div class="contact-locate-icon"> <img src="assets/img/location-pin.svg"></div>
                            <div class="px-3">
                                <p class="cotact-txt-clr">CANADA</p>
                                <p class="contact-body-txt"># 2 FRONTIER DRIVE THOROLD,
                                    <br>ONTARIO, L2V-0K3
                                    <br>CANADA
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <!-- Footer -->
            <?php include 'componets/footer.php' ?>
            <!-- Footer end -->

            <!-- PopUp -->
            <?php include 'componets/popup.php' ?>
            <?php include 'componets/thank-you-contact-success.php' ?>
            <!-- PopUp -->
    </main>


    <script>
        function phoneBtn1() {
            window.location.href = "callto:+971558995713";
        }
        function phoneBtn2() {
            window.location.href = "callto:+447946054506";
        }
        function phoneBtn3() {
            window.location.href = "callto:+94779151537";
        }

        function whtsappBtn() {
            window.location.href = "//api.whatsapp.com/send?phone=+9477378829&text=Xitricon Web";
        }

        function emailBtn1() {
            window.location.href = "mailto:rifai.Wahid@xitricon.com";
        }

        function emailBtn2() {
            window.location.href = "mailto:cliff.goodwin@xitricon.com";
        }

        function emailBtn3() {
            window.location.href = "mailto:info@xitricon.com";
        }


        window.onscroll = function () {
            myFunction()
        };

        var navbar = document.getElementById("navbar");
        var sticky = navbar.offsetTop;

        function myFunction() {
            if (window.pageYOffset > sticky) {
                navbar.classList.add("sticky-w")
            } else {
                navbar.classList.remove("sticky-w");
            }
        }
    </script>
</body>

<?php include 'componets/script_includes.php' ?>
<!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script> -->

</html>